#include "stdafx.h"

#include <iostream>
#include <vector>
#include <windows.h>

#include <memory>
#include <math.h>

#include "Director.h"
#include "Context.h"
#include "Picker.h"
#include "Mouse.h"
#include "DrawPrimitives.h"
#include "Keyboard.h"
#include "World.h"
#include "Camera.h"
#include "Debug.h"
#include "Entity.h"
#include "Mesh.h"
#include "Transform.h"
#include "Editor.h"
#include "ShaderCache.h"
#include "Time.h"
#include "EditorGUI.h"
#include "Inspector.h"
#include "SceneLoader.h"
using namespace nanogui;
using namespace sip;

DrawPrimitives* prim;

void addQuad(glm::vec3 p, DrawPrimitives& prim)
{
	float scale = .3f;
	prim.addQuad(Director::Instance().getWorld()->getActiveCamera()->getViewProjectionMatrix(),
	glm::vec4(1.0f, 0.0f, 1.0f, .2f),

	glm::vec4(p.x + -1.0 * scale, p.y + 1.0 * scale, p.z, 1.0f),
	glm::vec4(p.x + 1.0f * scale, p.y + 1.0f * scale, p.z, 1.0f),
	glm::vec4(p.x + -1.0f * scale, p.y + -1.0f * scale, p.z, 1.0f),
	glm::vec4(p.x + 1.0f * scale, p.y + -1.0f * scale, p.z, 1.0f));

}

void renderScene()
{		
}		



void update()
{

	auto world = Director::Instance().getWorld();
	float dt = Time::deltaTime();

	world->update();
	Util::checkGLError("Error");

	if (Keyboard::isKeyDown('R'))
	{
		world->refreshMaterials();
	}

}




int _tmain(int argc, char** argv)
{
	Director::Instance().createContext("../Renderer/config/config.lua", 1600, 1024);
	prim = new DrawPrimitives();

	auto world = Director::Instance().getWorld();
	auto context = Director::Instance().getContext();
	context->setRenderFunc(renderScene);
	context->setUpdateFunc(update);

	int err = glGetError();

	Debug::logFail(err == GL_NO_ERROR,  "failed to init");
	Director::Instance().loadMaterialConfigFile("materials.lua");

	world->rebuildWorldTransforms();

	SceneLoader::Instance().loadScene("assets/testscene.scene");

	Director::Instance().getContext()->startMainLoop();

	return 0;
}

