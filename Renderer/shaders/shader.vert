#version 430

layout(location = 0) in vec4 VertexPosition;
layout(location = 1) in vec4 VertexColor;

out vec4 Color;

void main()
{
	Color = VertexColor;
	gl_Position = VertexPosition;
}