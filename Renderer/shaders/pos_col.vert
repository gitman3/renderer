#version 430

layout(location = 0) in vec4 a_vertexPos;
layout(location = 1) in vec4 a_vertexCol;

out vec4 Color;

void main()
{
	Color = a_vertexCol;
	gl_Position = a_vertexPos;
}