
#version 430

layout(location = 0) uniform sampler2D u_texture0;

in vec4 v_vertexPos;
in vec2 v_uv;

void main()
{
	vec4 pos = mod(v_vertexPos, 2.0);
	pos -= max(vec4(0.0), (pos - 1.) * 2.0);
	
	float width = 0.02;
	gl_FragColor = texture(u_texture0, pos.xz);
	gl_FragColor *= smoothstep(2.0, .0, gl_FragCoord.z);
}