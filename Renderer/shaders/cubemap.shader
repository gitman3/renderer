// @glDepthMask : GL_TRUE
@glEnable : GL_DEPTH_TEST
// @glEnable : GL_BLEND
  @glDisable : GL_CULL_FACE
// @glBlendFunc : GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
// @glPolygonMode : GL_FRONT_AND_BACK GL_LINE

[Vertex]

#version 430

layout(location = 13) uniform mat4 u_mvp;
layout(location = 16) uniform mat4 u_vp;
layout(location = 17) uniform mat4 u_p;
layout(location = 0) in vec4 a_vertexPos;
layout(location = 14) uniform mat4 u_v;


in vec2 a_texCoord;
out vec4 v_vertexPos;

void main()
{
	mat4 v = u_v;
	
	v[3] = vec4(0.0, 0.0, 0.0, 1.0);
	mat4 vp = u_p * v;
	gl_Position = vp * vec4(a_vertexPos.xyz * 15., 1.0);
	//gl_Position.w = 1.0;
	v_vertexPos = a_vertexPos;
	//v_vertexPos.x = 1.0 - v_vertexPos.x;
	v_vertexPos.y *= -1.;
//	v_vertexPos.z *= -1.;
}


[Fragment]

#version 430

layout(location = 0) uniform samplerCube u_textureCube0;
layout(location = 14) uniform mat4 u_v;
layout(location = 16) uniform mat4 u_vp;

in vec4 v_vertexPos;

void main()
{
	gl_FragColor = texture(u_textureCube0, v_vertexPos.xyz);
	//gl_FragColor = u_vp[0];
}
