#version 430

out vec4 FragColor;
in float D;
in vec4 Color;
in vec3 LightDir;
in vec3 Normal;
in vec2 TexCoord;
in vec3 AmbientColor;

varying vec4 MVPosition;
varying vec4 MPosition;

layout(binding = 0) uniform sampler2D u_texture0;
layout(binding = 1) uniform sampler2D u_texture1;
layout(binding = 2) uniform sampler2D u_texture2;


layout(binding = 3) uniform sampler2D u_normalmap0;
layout(binding = 4) uniform sampler2D u_normalmap1;
layout(binding = 5) uniform sampler2D u_normalmap2;
layout(binding = 10) uniform sampler2D u_depthTexture;

layout(location = 20) uniform vec3 u_viewDirection;
layout(location = 21) uniform vec3 u_cameraPosition;

layout (location = 30) uniform float u_specular;
layout (location = 31) uniform float u_rimPower;
layout (location = 32) uniform float u_rimAmount;
layout (location = 33) uniform float u_bumpPower;
layout (location = 34) uniform float u_bumpAmount;

void main()
{
	vec4 c = texture2D(u_texture0, TexCoord) * Color;
	FragColor = Color * 1. ;
}