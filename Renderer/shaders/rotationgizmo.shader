@glDepthMask : GL_TRUE
// @glEnable : GL_DEPTH_TEST
 @glEnable : GL_BLEND

// @glBlendFunc : GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
// @glPolygonMode : GL_FRONT_AND_BACK GL_LINE

[Vertex]

#version 430

layout(location = 0) in vec4 a_vertexPos;
layout(location = 10) uniform mat4 u_normalMatrix;
layout(location = 13) uniform mat4 u_mvp;

in vec2 a_texCoord;

out vec4 v_vertexPos;

void main()
{
	gl_Position = u_mvp * a_vertexPos;
	v_vertexPos = a_vertexPos;
}


[Fragment]

#version 430

layout(location = 0) uniform sampler2D u_texture0;
layout(location = 9) uniform float u_selectedHandle;
layout(location = 10) uniform mat4 u_normalMatrix;

in vec4 v_vertexPos;


void main()
{
	float r = smoothstep(0.05, 0.0, abs(dot(v_vertexPos.xyz, vec3(0.0, 1.0, 0.0))));
	float g = smoothstep(0.05, 0.0, abs(dot(v_vertexPos.xyz, vec3(1.0, 0.0, 0.0))));
	float b = smoothstep(0.05, 0.0, abs(dot(v_vertexPos.xyz, vec3(0.0, 0.0, 1.0))));
	
	float a = 0.2;
	
	if(u_selectedHandle == 1.) 
	{
		if(r > 0.0)
		{		
			a = r;
		}
		if(r > 0.0 && (g > 0.0 || b > 0.0))
		{
			a = 0.1 * r;
		}
	}
	
	if(u_selectedHandle == 2.) 
	{
		if(g > 0.0) a = g;
		if(g > 0.0 && (r > 0.0 || b > 0.0))
		{
			a = 0.1 * g;
		}
	}
	if(u_selectedHandle == 3.) 
	{
		if(b > 0.0) a = b;
		if(b > 0.0 && (r > 0.0 || g > 0.0))
		{
			a = 0.1 * b;
		}
	}
	
	gl_FragColor = vec4(r, g, b, a);
	vec3 normal = (u_normalMatrix * vec4(normalize(v_vertexPos.xyz), 0.0)).xyz;
	//gl_FragColor.a = 1.0;
	//if(normal.z <0.0) gl_FragColor = vec4(.0);
	//gl_FragColor = v_vertexPos;
}
