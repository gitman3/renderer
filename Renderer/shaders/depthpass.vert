#version 430

layout(location = 13) uniform mat4 u_mvp;
layout(location = 0)  in vec4 a_vertexPos;

void main()
{
	gl_Position = u_mvp * a_vertexPos;
}