@glDepthMask : GL_TRUE
@glEnable : GL_DEPTH_TEST
//@glEnable : GL_BLEND

[Vertex]
#include "pos_col_light_proj_textured.vert"

[Fragment]
#include "pos_col_light_proj_textured.frag"
