@glDepthMask : GL_TRUE
@glEnable : GL_DEPTH_TEST
@glPolygonMode : GL_FRONT_AND_BACK GL_LINE

[Vertex]
#version 430

layout(location = 10) uniform mat4 u_m;
layout(location = 11) uniform mat4 u_v;
layout(location = 12) uniform mat4 u_mv;
layout(location = 13) uniform mat4 u_mvp;
layout(location = 14) uniform mat4 u_normalmat;

layout(location = 0) in vec4 a_vertexPos;
layout(location = 1) in vec2 a_texCoord;
layout(location = 2) in vec3 a_vertexNormal;


layout(location = 5) uniform vec3 u_lightDir;
layout(location = 6) uniform vec3 u_ambientColor;


out vec4 Color;
out vec3 Normal;

out vec3 LightDir;
out vec3 AmbientColor;

varying vec4 MVPosition;
varying vec4 MPosition;


void main()
{
	vec3 ldir = u_lightDir;
	
	Normal = vec3(u_normalmat * vec4(a_vertexNormal, 0.0));
	float d = dot(-ldir, vec3(Normal));
	d = clamp(d, 0.0, 1.0);
	
	Color = vec4(d);
	AmbientColor = u_ambientColor;
	MVPosition = u_mv * a_vertexPos;
	MPosition = u_m * a_vertexPos;
	
	LightDir = ldir;
	gl_Position = u_mvp * (a_vertexPos * vec4(1.0, 1.0, 1.0, 1.0));
}

[Fragment]

#version 430

out vec4 FragColor;

in vec4 Color;
in vec3 Normal;
in vec3 LightDir;
in vec3 AmbientColor;

varying vec4 MPosition;

layout(location = 20) uniform vec3 u_viewDirection;
layout(location = 21) uniform vec3 u_cameraPosition;

layout (location = 30) uniform float u_specular;
layout (location = 31) uniform float u_rimPower;
layout (location = 32) uniform float u_rimAmount;


void main()
{


	FragColor = vec4(1.0, 0.0, 0.0, 1.0);
	
}