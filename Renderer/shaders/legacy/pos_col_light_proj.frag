#version 430

out vec4 FragColor;
in float D;
in vec4 Color;
in vec3 LightDir;
in vec2 TexCoord;
in mat3 TBM;
in vec3 AmbientColor;

varying vec4 MVPosition;
varying vec4 MPosition;

layout(binding = 0) uniform sampler2D u_texture0;
layout(binding = 1) uniform sampler2D u_texture1;
layout(binding = 2) uniform sampler2D u_texture2;


layout(binding = 3) uniform sampler2D u_normalmap0;
layout(binding = 4) uniform sampler2D u_normalmap1;
layout(binding = 5) uniform sampler2D u_normalmap2;
layout(binding = 10) uniform sampler2D u_depthTexture;

layout(location = 20) uniform vec3 u_viewDirection;
layout(location = 21) uniform vec3 u_cameraPosition;

layout (location = 30) uniform float u_specular;
layout (location = 31) uniform float u_rimPower;
layout (location = 32) uniform float u_rimAmount;
layout (location = 33) uniform float u_bumpPower;
layout (location = 34) uniform float u_bumpAmount;

void main()
{
	vec4 c = Color;
	vec3 vd = (vec3(MPosition) - u_cameraPosition);
	
	/// SSAO samples
	vec2 SSAOuv = vec2(gl_FragCoord.x / 1024.0, gl_FragCoord.y / 1024.0);	
	vec4 SSAOColor = texture2D(u_depthTexture, SSAOuv);
	
	///////////////////
	// Extract normals
	vec4 norm = texture2D(u_normalmap0, TexCoord);	
	
	norm.r = ( 2 * norm.r ) - 1.0;
	norm.g = ( 2 * norm.g ) - 1.0;
	norm.b = ( 2 * norm.b ) - 1.0;
	
	vec3 n = TBM * vec3(norm);
	////////////////////
	// Rim light	
	vec3 vdir = normalize(-vec3(vd));
	float f = 1.0 - dot(vdir, vec3(n));
	f = clamp(f, 0.0, 1.0);
	f = pow(f, u_rimPower);
	vec4 fres = vec4(0.0, 0.0, 0.5, 1.0) * f * u_rimAmount;
	
	// specular
	vec3 V = vdir;				
	vec3 L = -LightDir;
	vec3 H = normalize(V + L);

	float spec = dot(H, TBM[2]);         	
	spec = clamp(spec, 0.0, 1.0);
	spec = pow(spec, 15.0);
	spec *= u_specular;
	
	/////////////////////
	// Bump map
	float d = u_bumpAmount * dot(n, -LightDir) + (1.0 - u_bumpAmount) * D;
	d = clamp(d, 0.0, 1.0);	
	d = pow(d, u_bumpPower);	
	vec4 bump = vec4(d);
	
	
	////
	c = texture2D(u_texture0, TexCoord) * bump;
	c += vec4(AmbientColor, 1.0) + fres + spec;
	//c *= 1.0 - pow(gl_FragCoord.z, 1115.0) * 1.5;
	float fog = abs(pow(gl_FragCoord.z, 150.0)) * 2.0;
	
	float fogmax = 1.0;
	float fogmin = 0.01;
	fog = min(fogmax, fog);
	
	float ff = (fogmax - fog) / (fogmax - fogmin);
	ff = clamp(ff, 0.0, 1.0);
	
	// out of gamut correction
	float m = max(1.0, c.r);
	m = max(m, c.g);
	m = max(m, c.b);
	c /= m;
	
	
	FragColor = c;
	
}