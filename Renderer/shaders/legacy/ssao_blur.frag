#version 430

out vec4 FragColor;
in vec2 texcoord;
layout(binding = 0) uniform sampler2D u_texture0;
uniform vec2 u_Scale;

const int SAMPLES = 5;
// 
//0.261121	0.210789	0.110867	0.037976	0.008466	0.001227	0.000116
// 0.197413	0.174666	0.120978	0.065591	0.027835	0.009245	0.002403	0.000489	0.000078	0.00001	0.000001
// 	0.382928	0.241732	0.060598	0.005977	0.000229

// 0.000229	0.005977	0.060598	0.241732	0.382928	0.241732	0.060598	0.005977	0.000229

const float weights[SAMPLES] = 
{ 
	0.382928,
	0.241732,
	0.060598,
	0.005977,
	0.000229,
};
/*
const float weights[SAMPLES] = 
{ 
	0.197413,
	0.174666,
	0.120978,
	0.065591,
	0.027835,
	0.009245,
	0.002403,
	0.000489,
	0.000078,
	0.00001,
	0.00001	
};*/

const vec2 pixelSize = vec2(2.5, 2.5);
void main()
{	

    FragColor = texture2D(u_texture0, texcoord)*weights[0];
    
	for (int i = 1; i < SAMPLES; i++) {
        vec2 offset = vec2(float(i)*pixelSize.x*u_Scale.x, float(i)*pixelSize.y*u_Scale.y);
        FragColor += texture2D(u_texture0, texcoord + offset)*weights[i];
        FragColor += texture2D(u_texture0, texcoord - offset)*weights[i];
    }
}