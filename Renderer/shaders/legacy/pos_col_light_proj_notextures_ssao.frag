#version 430

out vec4 FragColor;

in vec4 Color;
in vec3 Normal;
in vec3 LightDir;
in vec3 AmbientColor;

varying vec4 MVPPosition;
varying vec4 MVPosition;
varying vec4 MPosition;

layout(location = 20) uniform vec3 u_viewDirection;
layout(location = 21) uniform vec3 u_cameraPosition;

layout (location = 30) uniform float u_specular;
layout (location = 31) uniform float u_rimPower;
layout (location = 32) uniform float u_rimAmount;

layout(binding = 10) uniform sampler2D u_depthTexture;
layout(location = 15) uniform mat4 u_p;

const int SAMPLES = 27;

vec3 samples1 [SAMPLES]  = 
{
vec3 (-0.462249, -0.466419, -0.172109 ),
vec3 (-0.276859, -0.334386, -0.101377 ),
vec3 (-0.317194, -0.279697, 0.418648 ),
vec3 (-0.218426, 0.147582, -0.300633 ),
vec3 (-0.447935, -0.0694143, 0.102842 ),
vec3 (-0.1904, 0.0778273, 0.430418 ),
vec3 (-0.234102, 0.484537, -0.394345 ),
vec3 (-0.445728, 0.176595, -0.140685 ),
vec3 (-0.200725, 0.270114, 0.38702 ),
vec3 (0.076749, -0.49999, -0.25513 ),
vec3 (-0.00972015, -0.424304, -0.096881 ),
vec3 (0.0238401, -0.209301, 0.289107 ),
vec3 (0.00551875, -0.104297, -0.416084 ),
vec3 (-0.154846, 0.0438703, 0.120024 ),
vec3 (-0.150828, 0.0322224, 0.474924 ),
vec3 (0.0279295, 0.322443, -0.378913 ),
vec3 (-0.116454, 0.339524, -0.0808893 ),
vec3 (0.138488, 0.370724, 0.208477 ),
vec3 (0.308456, -0.314508, -0.193442 ),
vec3 (0.337733, -0.391517, 0.0612456 ),
vec3 (0.167491, -0.490315, 0.170003 ),
vec3 (0.28578, 0.00948618, -0.291569 ),
vec3 (0.251925, 0.115711, -0.0373903 ),
vec3 (0.177277, -0.110024, 0.313105 ),
vec3 (0.299361, 0.255852, -0.239128 ),
vec3 (0.261478, 0.461842, 0.00664794 ),
vec3 (0.343898, 0.355759, 0.266737 ),
};
/*
vec3 samples2 [SAMPLES] =
{
vec3 (-0.460794, -0.322199, -0.17627 ),
vec3 (-0.172994, -0.384345, -0.0104729 ),
vec3 (-0.257419, -0.256584, 0.475758 ),
vec3 (-0.303583, 0.0813776, -0.280918 ),
vec3 (-0.205435, 0.00324005, 0.0358643 ),
vec3 (-0.430031, 0.0813267, 0.34404 ),
vec3 (-0.414691, 0.347296, -0.284865 ),
vec3 (-0.183259, 0.23043, -0.00101219 ),
vec3 (-0.331233, 0.219921, 0.257866 ),
vec3 (0.117786, -0.360561, -0.464293 ),
vec3 (0.000147507, -0.297011, 0.138529 ),
vec3 (-0.131184, -0.273471, 0.362972 ),
vec3 (-0.0743482, 0.0192928, -0.366553 ),
vec3 (-0.114419, 0.00600706, 0.0322835 ),
vec3 (-0.0967081, -0.063331, 0.242821 ),
vec3 (-0.0977762, 0.300338, -0.185893 ),
vec3 (0.137226, 0.441679, -0.0608387 ),
vec3 (0.0174312, 0.49411, 0.491974 ),
vec3 (0.360998, -0.439573, -0.473825 ),
vec3 (0.486104, -0.446491, 0.120258 ),
vec3 (0.448261, -0.459136, 0.382626 ),
vec3 (0.283542, 0.113849, -0.244591 ),
vec3 (0.40523, -0.0166478, 0.0794753 ),
vec3 (0.226564, -0.135202, 0.387143 ),
vec3 (0.315516, 0.458118, -0.494558 ),
vec3 (0.329829, 0.28224, 0.00755334 ),
vec3 (0.339666, 0.218752, 0.256493 ),
};*/
/*
vec3 samples3 [SAMPLES] =
{
vec3 (-0.460794, -0.322199, -0.17627 ),
vec3 (-0.172994, -0.384345, -0.0104729 ),
vec3 (-0.257419, -0.256584, 0.475758 ),
vec3 (-0.303583, 0.0813776, -0.280918 ),
vec3 (-0.205435, 0.00324005, 0.0358643 ),
vec3 (-0.430031, 0.0813267, 0.34404 ),
vec3 (-0.414691, 0.347296, -0.284865 ),
vec3 (-0.183259, 0.23043, -0.00101219 ),
vec3 (-0.331233, 0.219921, 0.257866 ),
vec3 (0.117786, -0.360561, -0.464293 ),
vec3 (0.000147507, -0.297011, 0.138529 ),
vec3 (-0.131184, -0.273471, 0.362972 ),
vec3 (-0.0743482, 0.0192928, -0.366553 ),
vec3 (-0.114419, 0.00600706, 0.0322835 ),
vec3 (-0.0967081, -0.063331, 0.242821 ),
vec3 (-0.0977762, 0.300338, -0.185893 ),
vec3 (0.137226, 0.441679, -0.0608387 ),
vec3 (0.0174312, 0.49411, 0.491974 ),
vec3 (0.360998, -0.439573, -0.473825 ),
vec3 (0.486104, -0.446491, 0.120258 ),
vec3 (0.448261, -0.459136, 0.382626 ),
vec3 (0.283542, 0.113849, -0.244591 ),
vec3 (0.40523, -0.0166478, 0.0794753 ),
vec3 (0.226564, -0.135202, 0.387143 ),
vec3 (0.315516, 0.458118, -0.494558 ),
vec3 (0.329829, 0.28224, 0.00755334 ),
vec3 (0.339666, 0.218752, 0.256493 ),
};*/


float snoise(vec3 uv, float res)
{
	const vec3 s = vec3(1e0, 1e2, 1e3);
	
	uv *= res;
	
	vec3 uv0 = floor(mod(uv, res))*s;
	vec3 uv1 = floor(mod(uv+vec3(1.), res))*s;
	
	vec3 f = fract(uv); f = f*f*(3.0-2.0*f);

	vec4 v = vec4(uv0.x+uv0.y+uv0.z, uv1.x+uv0.y+uv0.z,
		      	  uv0.x+uv1.y+uv0.z, uv1.x+uv1.y+uv0.z);

	vec4 r = fract(sin(v*1e-1)*1e3);
	float r0 = mix(mix(r.x, r.y, f.x), mix(r.z, r.w, f.x), f.y);
	
	r = fract(sin((v + uv1.z - uv0.z)*1e-1)*1e3);
	float r1 = mix(mix(r.x, r.y, f.x), mix(r.z, r.w, f.x), f.y);
	
	return mix(r0, r1, f.z)*2.-1.;
}

float getDepth(vec4 mvppos)
{
	float far = gl_DepthRange.far; 
	float near = gl_DepthRange.near;
	float ndcDepth = mvppos.z / mvppos.w;
	return (((far-near) * mvppos.z) + near + far) / 2.0;
}

vec3 getViewPosition(vec3 texanddepth)
{
	mat4 mvpinv = inverse(u_p);
    float x = texanddepth.x * 2 - 1;
    float y = (1 - texanddepth.y) * 2 - 1;
	texanddepth.x = x;
	texanddepth.y = y;
	vec4 unprojected = mvpinv * vec4(texanddepth, 1.0);	
	return unprojected.xyz / unprojected.w;	
}

void main()
{

	
	vec4 c = Color;
	vec3 vd = (vec3(MPosition) - u_cameraPosition);
	
	vec3 n = Normal;
	////////////////////
	// Rim light	
	vec3 vdir = normalize(-vec3(vd));
	float f = 1.0 - dot(vdir, vec3(n));
	f = clamp(f, 0.0, 1.0);
	f = pow(f, u_rimPower);
	vec4 fres = vec4(1.0, 1.0, 1.0, 1.0) * f * u_rimAmount;
	
	// specular
	vec3 V = vdir;				
	vec3 L = -LightDir;
	vec3 H = normalize(V + L);

	float spec = dot(H, n);         	
	spec = clamp(spec, 0.0, 1.0);
	spec = pow(spec, 15.0);
	spec *= u_specular;
	
	c += spec + fres;

	//////////	
	//// fog
	float fog = abs(pow(gl_FragCoord.z, 150.0)) * 2.0;	
	float fogmax = 1.0;
	float fogmin = 0.01;
	fog = min(fogmax, fog);
	
	float ff = (fogmax - fog) / (fogmax - fogmin);
	ff = clamp(ff, 0.0, 1.0);
	c = mix(vec4(0.0, 0.2, 0.3, 1.0), c, ff);
	
	
	// calculate fragment depth at normalized device coordinates	

	float far = gl_DepthRange.far; 
	float near = gl_DepthRange.near;
	float nf = near / far;
	vec3 depthMapCoord = vec3(gl_FragCoord.x / 1024.0, gl_FragCoord.y / 1024.0, 0.0);	
	
	// sample depth map for depth value
	float zSample = texture (u_depthTexture, vec2(depthMapCoord) ).z;
	depthMapCoord.z = (zSample);
	
	// view coordinates
	vec3 viewcoord = getViewPosition(depthMapCoord);
	
	//float refdepth = getDepth(zSample);
	
	float step = 0.545;
	int hits = 0;
	float tmp = 0.0;
	
	mat3 rot = mat3(1);
	float an = gl_FragCoord.x + gl_FragCoord.y + gl_FragCoord.z;
	an = snoise(depthMapCoord * 110.0, 11.0);

	rot[1] = vec3(0, cos(an), -sin(an));
	rot[2] = vec3(0, sin(an), cos(an));
	
	for (int i = 0; i < SAMPLES; ++i)
	{
		vec3 csample = rot * samples1[i];
		vec3 sampleAtview = viewcoord + csample * step;
		
		vec4 _sv = u_p * vec4(sampleAtview, 1.0);
		float _svdepth = _sv.z / _sv.w;
		vec2 svUV = _sv.xy / _sv.w;
		svUV.xy = svUV.xy * 0.5 + 0.5;		
		svUV.y = 1.0 - svUV.y;
		float depthInTexture = texture(u_depthTexture, svUV.xy).x;
		tmp = svUV.x;
		
		if(depthInTexture > _svdepth)
		{
			//hits = 120;
			++hits;
		}
		
		
		/*
		
		vec3 samplepos = vec3(gl_FragCoord.x, gl_FragCoord.y, 0.0) + s * step;
		vec3 depthMapTest = vec3(samplepos.x / 1024.0, samplepos.y / 1024.0, 0.0);		
		float smpl = texture (u_depthTexture, vec2(depthMapTest) ).z;
		float smplz = getDepth(smpl);
		
		if ( smplz + (s.z * (0.001)  ) > refdepth)
		{
			++hits;
		}*/
		
	}
	
	float shadow = hits / float(SAMPLES);
	c = vec4(shadow);
	
	// out of gamut correction
	float m = max(1.0, c.r);
	m = max(m, c.g);
	m = max(m, c.b);
	c /= m;
	FragColor = c;
	
}