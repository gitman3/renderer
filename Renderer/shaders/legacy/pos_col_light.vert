#version 430

layout(location = 0) in vec4 a_vertexPos;
layout(location = 1) in vec4 a_vertexCol;
layout(location = 2) in vec3 a_vertexNormal;

layout(location = 0) uniform vec3 u_lightDir;

out vec4 Color;

void main()
{
	float d = dot(vec3(0.0, 1.0, 0.0), a_vertexNormal);
	d = clamp(d, 0.05, 1.0);
	Color = a_vertexCol * d;
	//Color.r = abs(a_vertexNormal.z);
	gl_Position = a_vertexPos;
}