#version 430

out vec4 FragColor;
in float D;
in vec4 Color;
in vec3 LightDir;
in vec3 Normal;
in vec2 TexCoord;
in vec3 AmbientColor;

varying vec4 MVPosition;
varying vec4 MPosition;

layout(binding = 0) uniform sampler2D u_texture0;
layout(binding = 1) uniform sampler2D u_texture1;
layout(binding = 2) uniform sampler2D u_texture2;


layout(binding = 3) uniform sampler2D u_normalmap0;
layout(binding = 4) uniform sampler2D u_normalmap1;
layout(binding = 5) uniform sampler2D u_normalmap2;
layout(binding = 10) uniform sampler2D u_depthTexture;

layout(location = 20) uniform vec3 u_viewDirection;
layout(location = 21) uniform vec3 u_cameraPosition;

layout (location = 30) uniform float u_specular;
layout (location = 31) uniform float u_rimPower;
layout (location = 32) uniform float u_rimAmount;
layout (location = 33) uniform float u_bumpPower;
layout (location = 34) uniform float u_bumpAmount;

void main()
{

	/// SSAO samples
	vec2 SSAOuv = vec2(gl_FragCoord.x / 1024.0, gl_FragCoord.y / 1024.0);	
	vec4 SSAOColor = texture2D(u_depthTexture, SSAOuv);

	vec4 c = Color;
	vec3 vd = (vec3(MPosition) - u_cameraPosition);	
	vec3 n = Normal;
	
	////////////////////
	// Rim light	
	vec3 vdir = normalize(-vec3(vd));
	float f = 1.0 - dot(vdir, vec3(n));
	f = clamp(f, 0.0, 1.0);
	f = pow(f, u_rimPower);
	vec4 fres = vec4(1.0, 1.0, 1.0, 1.0) * f * u_rimAmount;
	
	// specular
	vec3 V = vdir;				
	vec3 L = -LightDir;
	vec3 H = normalize(V + L);

	float spec = dot(H, n);         	
	spec = clamp(spec, 0.0, 1.0);
	spec = pow(spec, 15.0);
	spec *= u_specular;
	
	////
	c = texture2D(u_texture0, TexCoord);
	
	c += vec4(AmbientColor, 1.0) + spec + fres;
	
	//c *= min(SSAOColor * 2.0, 1.0);
	// out of gamut correction
	//float m = max(1.0, c.r);
	//m = max(m, c.g);
	//m = max(m, c.b);
	//c /= m;
	FragColor = Color ;
	
	
}