#version 430

out vec4 FragColor;
in float D;
in vec4 Color;
in vec3 LightDir;
in vec3 Normal;
in vec2 TexCoord;
in vec3 AmbientColor;
in mat3 TBN;

varying vec4 MVPosition;
varying vec4 MPosition;

layout(binding = 0) uniform sampler2D u_texture0;
layout(binding = 1) uniform sampler2D u_texture1;
layout(binding = 2) uniform sampler2D u_texture2;

layout(binding = 3) uniform sampler2D u_normalmap0;
layout(binding = 4) uniform sampler2D u_normalmap1;
layout(binding = 5) uniform sampler2D u_normalmap2;
layout(binding = 10) uniform sampler2D u_depthTexture;

layout(location = 20) uniform vec3 u_viewDirection;
layout(location = 21) uniform vec3 u_cameraPosition;

layout (location = 30) uniform float u_specular;
layout (location = 31) uniform float u_rimPower;
layout (location = 32) uniform float u_rimAmount;
layout (location = 33) uniform float u_bumpPower;
layout (location = 34) uniform float u_bumpAmount;

void main()
{

	/// SSAO samples
	vec2 SSAOuv = vec2(gl_FragCoord.x / 1024.0, gl_FragCoord.y / 1024.0);	
	vec4 SSAOColor = texture2D(u_depthTexture, SSAOuv);

	vec4 c = Color;
	vec3 vd = normalize(MVPosition).xyz;
	vec3 n = Normal;
	
	////////////////////
	// Rim light	
	float f = 1.0 - dot(-vd, vec3(n));
	f = clamp(f, 0.0, 1.0);
	f = pow(f, u_rimPower);
	vec4 fres = vec4(1.0, 1.0, 1.0, 1.0) * f * u_rimAmount;
	
	// specular
	vec3 V = -vd;				
	vec3 L = -LightDir;
	vec3 H = normalize(V + L);

	float spec = dot(H, n);         	
	spec = clamp(spec, 0.0, 1.0);
	spec = pow(spec, 15.0);
	spec *= u_specular;
	
	////
	c = texture2D(u_texture0, TexCoord);
	
	c += vec4(AmbientColor, 1.0) + spec + fres;
	
	//c *= min(SSAOColor * 2.0, 1.0);
	FragColor = c ;
	
	mat3 _TBN = TBN;
	_TBN[0] = normalize(TBN[0]);
	_TBN[1] = normalize(TBN[1]);
	_TBN[2] = normalize(TBN[2]);
	
	//_TBN = transpose(_TBN);
	vec3 ts_vd = normalize(_TBN * vd);
	
	ts_vd *= 0.005;
	float z = 1.0;
	vec2 offset = vec2(0.0);
	
	for (int i = 0; i < 400; ++i)
	{
		float h = texture2D(u_texture0, TexCoord + offset).r;
		if(z < h)
		{
			break;
		}
		offset = float(i) * ts_vd.xy;
		z -= abs(ts_vd.z);
	}
	
	FragColor = texture2D(u_texture0, TexCoord + offset);
	
	//FragColor = vec4(vec3(0.0, 0.0, 1.0) * (_TBN), 1.0);
	//FragColor = vec4(_TBN[2], 1.0);
}