#version 430

layout(location = 10) uniform mat4 u_m;
layout(location = 11) uniform mat4 u_v;
layout(location = 12) uniform mat4 u_mv;
layout(location = 13) uniform mat4 u_mvp;
layout(location = 14) uniform mat4 u_normalmat;


layout(location = 0) in vec4 a_vertexPos;
layout(location = 1) in vec2 a_texCoord;
layout(location = 2) in vec3 a_vertexNormal;
layout(location = 3) in vec3 a_vertexBitangent;
layout(location = 4) in vec3 a_vertexTangent;

layout(location = 5) uniform vec3 u_lightDir;
layout(location = 6) uniform vec3 u_ambientColor;


out vec4 Color;
out vec2 TexCoord;
out mat3 TBM;

out vec3 LightDir;
out vec3 AmbientColor;
out float D;

varying vec4 MVPosition;
varying vec4 MPosition;


void main()
{
	vec3 ldir = u_lightDir;
	
	vec4 nrm = u_normalmat * vec4(a_vertexNormal, 0.0);
	D = clamp(dot(-ldir, vec3(nrm)), 0.0, 1.0);

	TexCoord = a_texCoord;
	Color = vec4(1.0);
	AmbientColor = u_ambientColor;
	MVPosition = u_mv * a_vertexPos;
	MPosition = u_m * a_vertexPos;
	
	mat3 _tbm;
	_tbm[0] = vec3(u_normalmat * vec4(a_vertexTangent,   0.0));
	_tbm[1] = vec3(u_normalmat * vec4(a_vertexBitangent, 0.0));
	_tbm[2] = vec3(nrm);
	
	TBM = _tbm;
	LightDir = ldir;
	gl_Position = u_mvp * a_vertexPos;
}