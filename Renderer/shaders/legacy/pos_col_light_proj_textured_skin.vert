#version 430

layout(location = 10) uniform mat4 u_m;
layout(location = 11) uniform mat4 u_v;
layout(location = 12) uniform mat4 u_mv;
layout(location = 13) uniform mat4 u_mvp;
layout(location = 14) uniform mat4 u_normalmat;

const int MAX_BONES = 100;

layout(location = 50) uniform mat4[MAX_BONES] u_bones;

layout(location = 0) in vec4 a_vertexPos;
layout(location = 1) in vec2 a_texCoord;
layout(location = 2) in vec3 a_vertexNormal;
layout(location = 6) in ivec4 a_boneIds;
layout(location = 7) in vec4 a_skinWeights;

layout(location = 45) uniform vec3 u_lightDir;
layout(location = 46) uniform vec3 u_ambientColor;


out vec4 Color;
out vec2 TexCoord;

out vec3 LightDir;
out vec3 AmbientColor;
out vec3 Normal;
out float D;

varying vec4 MVPosition;
varying vec4 MPosition;


void main()
{
	vec3 ldir = u_lightDir;

	Normal = vec3(u_normalmat * vec4(a_vertexNormal, 1.0));
	D = clamp(dot(-ldir, vec3(Normal)), 0.0, 1.0);

	TexCoord = a_texCoord;
	Color = vec4(1.0);
	AmbientColor = u_ambientColor;
	
	float w1 = a_skinWeights[0];
	float w2 = a_skinWeights[1];
	float w3 = a_skinWeights[2];
	float w4 = a_skinWeights[3];
	
	mat4 b1 = u_bones[a_boneIds[0]] * w1;
	mat4 b2 = u_bones[a_boneIds[1]] * w2;
	mat4 b3 = u_bones[a_boneIds[2]] * w3;
	mat4 b4 = u_bones[a_boneIds[3]] * w4;
	
	
	mat4 bTransform;
	bTransform[0] = vec4(1.0, 0.0, 0.0, 0.0);
	bTransform[1] = vec4(0.0, 1.0, 0.0, 0.0);
	bTransform[2] = vec4(0.0, 0.0, 1.0, 0.0);
	bTransform[3] = vec4(0.0, 0.0, 0.0, 1.0);
	
	bTransform += b1;
	bTransform += b2;
	bTransform += b3;
	bTransform += b4;
	
	vec4 vertexPos = a_vertexPos;
	vertexPos.w = 1.0;
	vertexPos =  bTransform * vertexPos;
	
	
	MVPosition = u_mv * vertexPos;
	MPosition = u_m * vertexPos;
	
	LightDir = ldir;
	gl_Position = u_mvp * vertexPos;
	Color = vec4(w1 * .1);
}