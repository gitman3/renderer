#version 430

layout(location = 10) uniform mat4 u_m;
layout(location = 11) uniform mat4 u_v;
layout(location = 12) uniform mat4 u_mv;
layout(location = 13) uniform mat4 u_mvp;
layout(location = 14) uniform mat4 u_normalmat;
layout(location = 15) uniform mat4 u_p;

layout(location = 0) in vec4 a_vertexPos;
layout(location = 1) in vec2 a_texCoord;
layout(location = 2) in vec3 a_vertexNormal;


layout(location = 5) uniform vec3 u_lightDir;
layout(location = 6) uniform vec3 u_ambientColor;

out vec4 MVPosition;
out vec4 MVPPosition;
out vec4 MPosition;
out vec4 v_vertexPos;
out vec3 v_vertexNormal;


void main()
{
	MVPPosition = u_mvp * a_vertexPos;
	MVPosition = u_mv * a_vertexPos;
	MPosition = u_m * a_vertexPos;
	v_vertexPos = a_vertexPos;
	v_vertexNormal = a_vertexNormal;
	gl_Position = u_mvp * a_vertexPos;
}