#version 430

layout(location = 10) uniform mat4 u_m;
layout(location = 11) uniform mat4 u_v;
layout(location = 12) uniform mat4 u_mv;
layout(location = 13) uniform mat4 u_mvp;
layout(location = 14) uniform mat4 u_normalmat;


layout(location = 0) in vec4 a_vertexPos;
layout(location = 1) in vec2 a_texCoord;

layout(location = 2) in vec3 a_vertexNormal;
layout(location = 4) in vec3 a_vertexBitangent;
layout(location = 5) in vec3 a_vertexTangent;

layout(location = 6) uniform vec3 u_ambientColor;
layout(location = 7) uniform vec3 u_lightDir;

out mat3 TBN;

out vec4 Color;
out vec2 TexCoord;

out vec3 LightDir;
out vec3 AmbientColor;
out vec3 Normal;
out float D;

varying vec4 MVPosition;
varying vec4 MPosition;


void main()
{
	vec3 ldir = u_lightDir;
	
	Normal = vec3(u_normalmat * vec4(a_vertexNormal, 0.0)).xyz;
	D = clamp(dot(-ldir, vec3(Normal)), 0.0, 1.0);

	TexCoord = a_texCoord;
	Color = vec4(1.0);
	AmbientColor = u_ambientColor;
	MVPosition = u_mv * a_vertexPos;
	MPosition = u_m * a_vertexPos;
	LightDir = ldir;
	
	mat3 _tbn;
	//_tbn[0] = vec3(u_normalmat * vec4(a_vertexTangent,   0.0)).xyz;
	//_tbn[1] = vec3(u_normalmat * vec4(a_vertexBitangent, 0.0)).xyz;
	
	_tbn[0] = normalize(cross(vec3(0.0, 1.0, 0.0), Normal));
	_tbn[1] = cross(_tbn[0], Normal);
	_tbn[2] = Normal;
	TBN = _tbn;
	
	gl_Position = u_mvp * a_vertexPos;
}