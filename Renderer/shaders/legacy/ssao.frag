#version 430

out vec4 FragColor;
in vec4 MPosition;
in vec4 v_vertexPos;
in vec3 v_vertexNormal;

layout(binding = 10) uniform sampler2D u_depthTexture;
layout(binding = 2) uniform sampler2D u_noiseTexture;
layout(location = 15) uniform mat4 u_p;
layout(location = 16) uniform mat4 u_pinv;
layout(location = 14) uniform mat4 u_normalmat;


const int SAMPLES = 13;
vec3 samples1 [SAMPLES]  = 
{
//vec3 (-0.462249, -0.466419, -0.172109 ),
vec3 (-0.276859, -0.334386, -0.101377 ),
//vec3 (-0.317194, -0.279697, 0.418648 ),
vec3 (-0.218426, 0.147582, -0.300633 ),
//vec3 (-0.447935, -0.0694143, 0.102842 ),
vec3 (-0.1904, 0.0778273, 0.430418 ),
//vec3 (-0.234102, 0.484537, -0.394345 ),
vec3 (-0.445728, 0.176595, -0.140685 ),
//vec3 (-0.200725, 0.270114, 0.38702 ),
vec3 (0.076749, -0.49999, -0.25513 ),
//vec3 (-0.00972015, -0.424304, -0.096881 ),
vec3 (0.0238401, -0.209301, 0.289107 ),
//vec3 (0.00551875, -0.104297, -0.416084 ),
vec3 (-0.154846, 0.0438703, 0.120024 ),
//vec3 (-0.150828, 0.0322224, 0.474924 ),
vec3 (0.0279295, 0.322443, -0.378913 ),
//vec3 (-0.116454, 0.339524, -0.0808893 ),
vec3 (0.138488, 0.370724, 0.208477 ),
//vec3 (0.308456, -0.314508, -0.193442 ),
vec3 (0.337733, -0.391517, 0.0612456 ),
//vec3 (0.167491, -0.490315, 0.170003 ),
vec3 (0.28578, 0.00948618, -0.291569 ),
//vec3 (0.251925, 0.115711, -0.0373903 ),
vec3 (0.177277, -0.110024, 0.313105 ),
//vec3 (0.299361, 0.255852, -0.239128 ),
vec3 (0.261478, 0.461842, 0.00664794 ),
//vec3 (0.343898, 0.355759, 0.266737 ),
};

float snoise(vec3 uv, float res)
{
	const vec3 s = vec3(1e0, 1e2, 1e3);
	
	uv *= res;
	
	vec3 uv0 = floor(mod(uv, res))*s;
	vec3 uv1 = floor(mod(uv+vec3(1.), res))*s;
	
	vec3 f = fract(uv); f = f*f*(3.0-2.0*f);

	vec4 v = vec4(uv0.x+uv0.y+uv0.z, uv1.x+uv0.y+uv0.z,
		      	  uv0.x+uv1.y+uv0.z, uv1.x+uv1.y+uv0.z);

	vec4 r = fract(sin(v*1e-1)*1e3);
	float r0 = mix(mix(r.x, r.y, f.x), mix(r.z, r.w, f.x), f.y);
	
	r = fract(sin((v + uv1.z - uv0.z)*1e-1)*1e3);
	float r1 = mix(mix(r.x, r.y, f.x), mix(r.z, r.w, f.x), f.y);
	
	return mix(r0, r1, f.z)*2.-1.;
}

float getViewSpaceDepth(float z_b)
{
	float zFar = gl_DepthRange.far; 
	float zNear = gl_DepthRange.near;
    float z_n = 2.0 * z_b - 1.0;
    float z_eye = 2.0 * zNear * zFar / (zFar + zNear - z_n * (zFar - zNear));
	return z_eye;
}

float getDepth(vec4 mvppos)
{
	float far = gl_DepthRange.far; 
	float near = gl_DepthRange.near;
	float ndcDepth = mvppos.z / mvppos.w;
	return (((far-near) * mvppos.z) + near + far) / 2.0;
}

vec3 getViewPosition(vec3 texanddepth)
{
    float x = texanddepth.x * 2 - 1;
    float y = texanddepth.y * 2 - 1;
    //float y = (1 - texanddepth.y) * 2 - 1;
	texanddepth.x = x;
	texanddepth.y = y;
	vec4 unprojected = u_pinv * vec4(texanddepth, 1.0);	
	return unprojected.xyz / unprojected.w;	
}

void main()
{

	
	vec4 c = vec4(1.0);
	vec3 depthMapCoord = vec3(gl_FragCoord.x / 1024.0, gl_FragCoord.y / 1024.0, 0.0);	
	
	float zSample = gl_FragCoord.z;
	depthMapCoord.z = (zSample);
	
	// view coordinates
	vec3 viewcoord = getViewPosition(depthMapCoord);
	
	float step = 2.5;
	float hits = 0.0;
	
	float an = texture2D(u_noiseTexture, vec2(depthMapCoord) * 1000.0).r;
	an *= 15.0;
	
	mat3 rot = mat3(1);
	rot[1] = vec3(0, cos(an), -sin(an));
	rot[2] = vec3(0, sin(an), cos(an));
	
	for (int i = 0; i < SAMPLES; ++i)
	{
		vec3 csample = rot * samples1[i];
		vec3 sampleAtview = viewcoord + csample * step;
		
		vec4 projectedSample = u_p * vec4(sampleAtview, 1.0);
		float sampleDepth = projectedSample.z / projectedSample.w;
		vec2 sampleUV = projectedSample.xy / projectedSample.w;
		sampleUV.xy = sampleUV.xy * 0.5 + 0.5;		
		float depthInTexture = texture(u_depthTexture, sampleUV.xy).x;
		
		vec3 depthPositionAtView = getViewPosition(vec3(sampleUV, depthInTexture));
		vec3 diff = depthPositionAtView - sampleAtview;
		
		//float textureDepthInView = getViewSpaceDepth(depthInTexture);
		
		if(depthInTexture < sampleDepth && (dot(diff, diff)) < 25.0)
		{
			hits += 1.0;
		}
	}
	
	float shadow = hits / float(SAMPLES);
	shadow = 1.0 - shadow;
//	shadow = pow(shadow, 2.0);
	c = vec4(shadow);
	c.a = 1.0;
	FragColor = (c);
	
}