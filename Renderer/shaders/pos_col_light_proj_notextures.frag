#version 430

out vec4 FragColor;

in vec4 Color;
in vec3 Normal;
in vec3 LightDir;
in vec3 AmbientColor;

varying vec4 MPosition;

layout(location = 20) uniform vec3 u_viewDirection;
layout(location = 21) uniform vec3 u_cameraPosition;

layout (location = 30) uniform float u_specular;
layout (location = 31) uniform float u_rimPower;
layout (location = 32) uniform float u_rimAmount;


void main()
{
	vec4 c = Color;
	c = clamp(c, 0.25, 1.0);
	vec3 vd = normalize((vec3(MPosition) - u_cameraPosition));
	
	vec3 n = Normal;

	
	// specular
	vec3 V = vd;				
	vec3 L = -LightDir;
	vec3 H = normalize(V + L);

	float spec = dot(H, n);         	
	spec = clamp(spec, 0.0, 1.0);
	spec = pow(spec, 15.0);
	spec *= u_specular;
	
	c += spec;
	FragColor = c * 1.;
	//FragColor.rgb = n;
}