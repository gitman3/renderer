@glDepthMask : GL_TRUE

@glEnable : GL_DEPTH_TEST
@glEnable : GL_BLEND
@glDisable : GL_CULL_FACE

// @glBlendFunc : GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
// @glPolygonMode : GL_FRONT_AND_BACK GL_LINE

[Vertex]

#version 430

layout(location = 13) uniform mat4 u_mvp;
layout(location = 0) in vec4 a_vertexPos;

in vec2 a_texCoord;

out vec4 v_vertexPos;

void main()
{
	gl_Position = u_mvp * a_vertexPos;
	v_vertexPos = a_vertexPos;
}


[Fragment]

#version 430

layout(location = 0) uniform sampler2D u_texture0;
layout(location = 9) uniform float u_selectedHandle;

in vec4 v_vertexPos;

void main()
{
	float zx = max(0.0, v_vertexPos.x);
	float zy = max(0.0, v_vertexPos.y);

	float yx = max(0.0, v_vertexPos.x);
	float yz = max(0.0, v_vertexPos.z);
	
	float xy = max(0.0, v_vertexPos.y);
	float xz = max(0.0, v_vertexPos.z);

	float size = .3;
	float offset = .0;
	float r = 0.0;
	float g = 0.0;
	float b = 0.0;
	
	if(v_vertexPos.x >= offset && v_vertexPos.x < offset + size && v_vertexPos.z >= offset && v_vertexPos.z <= offset + size) r = 1.0;
	if(abs(v_vertexPos.y) > 0.001) r = 0.0;

	float a = .5;
	if(u_selectedHandle == 2.)
	{
		if(r> 0.) a = 1.0;
	}

	gl_FragColor = vec4(r, g, b, a * max(b, max(r, g)));
	gl_FragDepth *= 0.1;
	
	
}
