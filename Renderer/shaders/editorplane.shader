@glDepthMask : GL_TRUE
@glEnable : GL_BLEND
@glBlendFunc : GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
@glDisable : GL_CULL_FACE
@glEnable : GL_DEPTH_TEST

[Vertex]

#include "default.vert"

[Fragment]

#version 430

layout(location = 0) uniform sampler2D u_texture0;

in vec4 v_vertexPos;
in vec2 v_uv;

void main()
{
	vec4 pos = mod(v_vertexPos, 2.0);
	pos -= max(vec4(0.0), (pos - 1.) * 2.0);
	
	gl_FragColor = texture(u_texture0, pos.xz);
	gl_FragColor *= smoothstep(2.0, .0, gl_FragCoord.z);
	//gl_FragColor = pos;
}
