#version 430

layout(location = 13) uniform mat4 u_mvp;
layout(location = 0) in vec4 a_vertexPos;

in vec2 a_texCoord;

out vec4 v_vertexPos;
out vec2 v_uv;

void main()
{
	gl_Position = u_mvp * a_vertexPos;
	v_vertexPos = a_vertexPos;
	v_uv = a_texCoord;
}


