
g_materials =  {
	
	metallic = 
	{
		specular = 1.0,
		rim_amount = 0.5,
		rim_power = 5,
		
		bump_power = 1.5,
		bump_amount = 1.0,
		
	}
}