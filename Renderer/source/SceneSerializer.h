#pragma once

namespace sip
{

	struct Project;
	class Scene;

	class SceneSerializer
	{
	public:
		static SceneSerializer& Instance()
		{
			static SceneSerializer Instance;
			return Instance;
		}

		void serialize(Scene& scene);

	private:
		SceneSerializer();
	};


}