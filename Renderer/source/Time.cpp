#include "Time.h"
#include <chrono>

using namespace sip;


namespace 
{
	std::chrono::system_clock::time_point PrevTime;
	unsigned int Elapsed;
	unsigned int Delta;
}

float Time::deltaTime()
{
	return (float)Delta / 1000.f;
}

void Time::tick()
{
	auto time = std::chrono::system_clock::now();
	Delta = static_cast<unsigned int>(std::chrono::duration_cast<std::chrono::milliseconds>(time - PrevTime).count());
	Elapsed += Delta;
	PrevTime = time;
}

float Time::elapsed()
{
	return Elapsed / 1000.f;
}

Time::Time()
{
	PrevTime = std::chrono::system_clock::now();
	Elapsed = 0;
	Delta = 0;
}
