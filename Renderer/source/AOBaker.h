#pragma  once
#include "Handle.h"
#include <map>
#include "RayTracing.h"
namespace sip
{

	class Entity;
	class AOBaker
	{
		static const int VERTICES_PER_BATCH = 3000;
		static const float RAYS_PER_VERTEX;

		struct OccludeeData
		{
			glm::vec4 ROs[VERTICES_PER_BATCH]; // Occlusion receiver vertex position
			glm::vec4 RDs[VERTICES_PER_BATCH]; // Occlusion receiver vertex normal
			glm::ivec4 Occlusions[VERTICES_PER_BATCH]; 
			glm::vec4 Debug;
		};

		// even every field inside the struct needs to be power of 2, or data is not aligned properly
		struct OccluderData
		{
			glm::vec4 Verts[3]; 
		};

		GLuint OccludeeSSBO;
		GLuint OccluderSSBO;
		
		void initOccluders(std::vector<Handle<Entity>>& entities);
		bool initOccludee(Entity* entity, int currentVertex);
		bool bakeInternal(Entity* entity, std::vector<glm::vec4>& occlusions);
		AOBaker();
		bool m_baking;

		std::vector<OccluderData> m_occluderData;
		void initBake(Handle<Entity> target, std::vector<Handle<Entity>> sources);

	public:
		void bakeAO(Handle<Entity>);
		bool isBaking();
		void update();

		static AOBaker& Instance()
		{
			static AOBaker Instance;
			return Instance;
		}
		
	};
}

