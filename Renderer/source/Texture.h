#pragma once

namespace sip
{

	class Texture
	{
		unsigned int m_handle;
	public:
		inline Texture(GLuint handle) : m_handle(handle) {}
		inline unsigned int GetHandle() const { return m_handle; }
	};
}