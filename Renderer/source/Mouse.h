#pragma once


namespace sip
{
	class Context;

	class Mouse
	{

	public:

		enum Button
		{
			Left =   0x000000FF,
			Middle = 0x0000FF00,
			Right =  0x00FF0000,
		};

		enum MouseButtonState
		{
			NONE =    0,
			PRESS =   1,
			HOLD =    1 << 1,
			RELEASE = 1 << 2
		};

		static int getX() { return Instance().m_x; }
		static int getY() { return Instance().m_y; }
		static int getDeltaX() { return Instance().m_x - Instance().m_prevX; }
		static int getDeltaY() { return Instance().m_y - Instance().m_prevY; };
		static double getScrollX() { return Instance().m_scrollX; }
		static double getScrollY() { return Instance().m_scrollY; }

		static bool isButtonDown(Button button);
		static bool isButtonHold(Button button);
		static bool isButtonReleased(Button button);

	private:

		static void MouseButtonFunc(int, int, int);
		static void MouseMovedFunc(int, int);
		static void MouseScrollFunc(double, double);
		static void Update();

		Mouse() :
			m_x(-1),
			m_y(-1),
			m_prevX(-1),
			m_prevY(-1),
			m_scrollX(0.),
			m_scrollY(0.),
			m_mouseButtonStates(0)
		{}

		static Mouse& Instance()
		{
			static Mouse instance;
			return instance;
		}

		int m_x;
		int m_y;
		int m_prevX;
		int m_prevY;
		double m_scrollX;
		double m_scrollY;

		int m_mouseButtonStates;

		static void bind(Context* context);
		void updateState(Button button, MouseButtonState state);

		friend class Context;
	};
}