#pragma once
#include "LuaTable.h"

namespace sip
{

	class Scene
	{
	public:
		Scene();
		Scene(const std::string& path);

		void setTable(const LuaTable& table);
		void setMetaTable(const LuaTable& table);

		std::string getProjectFilePath();
		std::string& getPath();

		std::vector<LuaTable> getEntityTables();
		LuaTable getMetaTable();

	private:

		LuaTable m_table;
		LuaTable m_metaTable;
		std::string m_path;
	};


}