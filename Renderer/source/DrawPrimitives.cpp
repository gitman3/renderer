#include "DrawPrimitives.h"

#include "GLStateRestore.h"
#include "ShaderCache.h"

using namespace sip;

DrawPrimitives::DrawPrimitives() : m_posvboHandle(-1), m_colvboHandle(-1), m_vaoHandle(-1), m_posData(), m_colData()
{

	GLStateRestore sr(GLStateRestore::VERTEX_ARRAY | GLStateRestore::ARRAY_BUFFER);

	m_program = ShaderCache::Instance()->getShader(ShaderCache::SNAME_POS_COL)->getHandle();

	glGenBuffers(1, &m_posvboHandle);
	glGenBuffers(1, &m_colvboHandle);
	glGenBuffers(1, &m_normvboHandle);

	glGenVertexArrays(1, &m_vaoHandle);
	glBindVertexArray(m_vaoHandle);

	glEnableVertexAttribArray(0); // Position
	glEnableVertexAttribArray(1); // Color
	glEnableVertexAttribArray(2); // Normal

	glBindBuffer(GL_ARRAY_BUFFER, m_posvboHandle);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, NULL);

	glBindBuffer(GL_ARRAY_BUFFER, m_colvboHandle);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, NULL);

	glBindBuffer(GL_ARRAY_BUFFER, m_normvboHandle);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, NULL);

}

void DrawPrimitives::addQuad(const glm::vec4& color, const glm::vec4& tl, const glm::vec4& tr, const glm::vec4& bl, const glm::vec4& br)
{
	GLStateRestore sr(GLStateRestore::ARRAY_BUFFER);
	m_posData.push_back(tl);
	m_posData.push_back(bl);
	m_posData.push_back(br);
	m_posData.push_back(br);
	m_posData.push_back(tl);
	m_posData.push_back(tr);

	glBindBuffer(GL_ARRAY_BUFFER, m_posvboHandle);
	glBufferData(GL_ARRAY_BUFFER, m_posData.size() * sizeof(glm::vec4), &m_posData[0], GL_DYNAMIC_DRAW);

	m_colData.push_back(color);
	m_colData.push_back(color);
	m_colData.push_back(color);
	m_colData.push_back(color);
	m_colData.push_back(color);
	m_colData.push_back(color);

	glBindBuffer(GL_ARRAY_BUFFER, m_colvboHandle);
	glBufferData(GL_ARRAY_BUFFER, m_colData.size() * sizeof(glm::vec4), &m_colData[0], GL_DYNAMIC_DRAW);
}

void DrawPrimitives::addQuad(const glm::mat4x4& transform, const glm::vec4& color, const glm::vec4& tl, const glm::vec4& tr, const glm::vec4& bl, const glm::vec4& br)
{
	GLStateRestore sr(GLStateRestore::ARRAY_BUFFER);

	glm::vec4 v1 = transform * tl;
	glm::vec4 v2 = transform * bl;
	glm::vec4 v3 = transform * br;
	glm::vec4 v4 = transform * br;
	glm::vec4 v5 = transform * tl;
	glm::vec4 v6 = transform * tr;

	m_posData.push_back(v1);
	m_posData.push_back(v2);
	m_posData.push_back(v3);
	m_posData.push_back(v4);
	m_posData.push_back(v5);
	m_posData.push_back(v6);

	glBindBuffer(GL_ARRAY_BUFFER, m_posvboHandle);
	glBufferData(GL_ARRAY_BUFFER, m_posData.size() * sizeof(glm::vec4), &m_posData[0], GL_DYNAMIC_DRAW);

	m_colData.push_back(color);
	m_colData.push_back(color);
	m_colData.push_back(color);
	m_colData.push_back(color);
	m_colData.push_back(color);
	m_colData.push_back(color);

	glBindBuffer(GL_ARRAY_BUFFER, m_colvboHandle);
	glBufferData(GL_ARRAY_BUFFER, m_colData.size() * sizeof(glm::vec4), &m_colData[0], GL_DYNAMIC_DRAW);

	///////////////// normals /////////////////
	glBindBuffer(GL_ARRAY_BUFFER, m_normvboHandle);

	auto ntl = glm::cross(glm::vec3(v2) - glm::vec3(v1), glm::vec3(v6) - glm::vec3(v1));
	auto nbl = glm::cross(glm::vec3(v3) - glm::vec3(v2), glm::vec3(v1) - glm::vec3(v2));
	auto ntr = glm::cross(glm::vec3(v1) - glm::vec3(v6), glm::vec3(v3) - glm::vec3(v6));
	auto nbr = glm::cross(glm::vec3(v6) - glm::vec3(v3), glm::vec3(v2) - glm::vec3(v3));

	ntl = glm::normalize(ntl);
	nbl = glm::normalize(nbl);
	ntr = glm::normalize(ntr);
	nbr = glm::normalize(nbr);

	m_normalData.push_back(ntl);
	m_normalData.push_back(nbl);
	m_normalData.push_back(nbr);
	m_normalData.push_back(nbr);
	m_normalData.push_back(ntl);
	m_normalData.push_back(ntr);

	glBufferData(GL_ARRAY_BUFFER, m_normalData.size() * sizeof(glm::vec3), &m_normalData[0], GL_DYNAMIC_DRAW);



}


void DrawPrimitives::clear()
{
	m_colData.clear();
	m_posData.clear();
//	glDeleteBuffers();
}

void DrawPrimitives::draw()
{
	GLStateRestore sr( GLStateRestore::SHADER | GLStateRestore::ARRAY_BUFFER | GLStateRestore::VERTEX_ARRAY | GLStateRestore::ELEM_ARR );
	glUseProgram(m_program);

	glBindVertexArray(m_vaoHandle);
	glDrawArrays(GL_TRIANGLES, 0, m_posData.size());

}
