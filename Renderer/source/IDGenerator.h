#pragma once
#include <unordered_set>
#include "Types.h"

namespace sip
{
	template<typename T>
	static ObjectID getNewID()
	{
		static std::unordered_set<ObjectID> usedIDs;

		static ObjectID runningId = 1;
		int runCount = 1;
		while (usedIDs.find(runningId) != usedIDs.end())
		{
			++runningId;
			if (runningId >= 65534) runningId = 1;
			++runCount;
			assert(runCount < 65536 * 2);
		};

		usedIDs.emplace(runningId);
		return runningId;
	}
}