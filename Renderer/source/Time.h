#pragma once

namespace sip
{
	class Time
	{
		static void tick();
		Time();
		friend class Context;

	public:
		static float deltaTime();
		static float elapsed();
	};
}