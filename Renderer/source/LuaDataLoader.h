#pragma once
#include "Prefab.h"

namespace sip
{

	class Material;
	class Scene;
	class CubeMap;

	class LuaDataLoader
	{
		LuaDataLoader();
	public:

		Prefab loadPrefab(const std::string& pathToFile);
		Material* loadMaterial(const std::string& pathToFile);
		void loadSceneMeta(const std::string& pathToFile, Scene& scene);

		static LuaDataLoader& Instance()
		{
			static LuaDataLoader Instance;
			return Instance;
		}
		std::shared_ptr<CubeMap> loadCubeMap(const std::string& pathToFile);
	};
}
