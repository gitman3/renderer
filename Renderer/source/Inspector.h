#pragma once


namespace nanogui
{
	class Screen;
}

namespace sip
{
	class Entity;

	namespace editor
	{
		class Inspector
		{
		public:
			Inspector();
			~Inspector();
			bool isVisible();
			void toggleVisible();
			void setVisible(bool visible);
			void onSelected(Handle<Entity> entity);
			Handle<Entity> getSelection();
		};
	}
}
