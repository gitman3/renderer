#pragma once

#include "glm/mat4x4.hpp"

struct aiNodeAnim;

namespace sip
{

	class Mesh;

	//Animation for a single bone
	class Animation
	{
		glm::mat4 m_matrix;
		glm::mat4 m_bindPoseMatrixInverse;
		aiNodeAnim* m_aiAnim;
		Mesh* m_mesh;
		float m_elapsed;
		bool m_running;

		void initBindPoseMatrix();

	public:

		Animation(aiNodeAnim* anim, Mesh* mesh);

		glm::mat4 getPose() { return m_matrix; }
		glm::mat4 getOffsetMatrix();

		void stop() { m_running = false; }
		void play() { m_running = true; }
		void update(float dt);
	};
}

