#pragma once

#include <istream>
#include "png.h"
#include <vector>
#include <gl/glew.h>

namespace sip
{

	class PNGLoader
	{
		static bool validate(std::istream& stream);
		static void userReadData(png_structp pngPtr, png_bytep data, png_size_t length);

	public:
		static std::vector<GLubyte> load(const char* pathToFile, GLint& width, GLint& height);
	};
}

