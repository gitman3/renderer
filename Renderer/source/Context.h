#pragma once

#include <functional>
#include "glm/vec2.hpp"

namespace nanogui
{
	class Screen;
}

struct GLFWwindow;

namespace sip
{

	class Context
	{

	public:

		typedef std::function < void() >					RenderFunc;
		typedef std::function < void() >					UpdateFunc;
		typedef std::function < void(int, int) >			MouseFunc;
		typedef std::function < void(int, int, int) >		MouseButtonFunc;
		typedef std::function < void(int, int, int, int) >	KeyFunc;
		typedef std::function < void(double, double) >			MouseScrollFunc;

		static Context& Instance();
		
		Context( int w, int h);
		~Context();

		void setRenderFunc(const RenderFunc& func);
		void setUpdateFunc(const UpdateFunc& func);
		void setKeyFunc(const KeyFunc& func);
		void startMainLoop();
		
		GLFWwindow* getWindow();
		nanogui::Screen* getScreen();

		glm::ivec2 getScreenSize();


	private:

		//inline DebugConsole* getConsole() { return m_debugConsole; }
		void setMouseFunc(const MouseFunc& func);
		void setMouseButtonFunc(const MouseButtonFunc& func);
		void setMouseScrollFunc(const MouseScrollFunc& func);

		int m_w;
		int m_h;

		nanogui::Screen* m_screen;
		GLFWwindow* m_window;

		static void internalUpdate();

		static RenderFunc sm_displayFunc;
		static UpdateFunc sm_updateFunc;
		static Context* sm_instance;
		static MouseFunc sm_mouseFunc;
		static MouseButtonFunc sm_mouseButtonFunc;
		static MouseScrollFunc sm_mouseScrollFunc;

		static KeyFunc sm_keyFunc; 
		friend class Mouse;
		friend class Director;
	};
}
