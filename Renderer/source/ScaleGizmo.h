#pragma once
#include "Handle.h"
#include <glm/vec3.hpp>
namespace sip
{
	class Entity;

	namespace editor
	{
		class ScaleGizmo
		{
			enum Plane
			{
				XY,
				YZ,
				XZ,
				NONE
			};
		public:
			ScaleGizmo();
			void onSelected(Handle<Entity> entity);

		private:
			Handle<Entity> m_entity;
			Plane m_axis;
			Handle<Entity> m_selection;

		};
	}
}