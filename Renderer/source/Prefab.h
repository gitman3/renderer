#pragma once
namespace sip
{
	class Prefab
	{
	public:

		std::string Name;
		std::string MeshFile;
		std::string MeshName;
		std::string filePath;
		//std::string PreviousPath;
		std::string fileName;

		std::map<std::string, std::string> MaterialMap;

		glm::vec3 Position;
		glm::vec3 Rotation;
		glm::vec3 Scale;


		bool IsAOTarget;
		bool isAOOccluder;

	

		Prefab() : 
			Name(), 
			MeshFile(), 
			MeshName(), 
			filePath(),
			MaterialMap(), 
			Position(), 
			Rotation(), 
			Scale(), 
			fileName(),
			isAOOccluder(false),
			IsAOTarget(false)
		{}

	};
}
