#pragma once
#include "nanogui/nanogui.h"

typedef nanogui::detail::FormWidget<float, std::integral_constant<bool, true>> floatWidget;
