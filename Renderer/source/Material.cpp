#include "Material.h"
#include "Util.h"
#include "Debug.h"

#include <iostream>
#include "GLStateRestore.h"
#include "ShaderCache.h"
#include "Director.h"
#include "GLProgram.h"
#include "TextureManager.h"
#include "Texture.h"
#include "World.h"
#include "Context.h"
#include "EditorGUI.h"
using namespace sip;


Material::~Material()
{
}


void Material::setMaterialConfig(const std::string& conf)
{
	Director::Instance().setMaterialConfig(this, conf);
}

void Material::use()
{
	m_program->use(this);

	for (unsigned int i = 0; i < m_textures.size(); ++i)
	{
		m_program->useTexture(i, &m_textures[i]);
	}

	for (unsigned int i = 0; i < m_normalMaps.size(); ++i)
	{
		m_program->useNormalMap(i, &m_normalMaps[i]);
	}

	if (m_cubemap)
	{
		m_program->useCubeMap(m_cubemap);
	}
}


void Material::setDefaultTexture(Texture texture)
{
	if (m_textures.size() > 0)
	{
		m_textures[0] = texture;
		return;
	}

	// first texture ever
	m_textures.push_back(texture);
}

void sip::Material::setCubeMap(CubeMap* cubeMap)
{
	m_cubemap = cubeMap;
}

void sip::Material::setTexture(int index, Texture texture)
{
	assert((int)m_textures.size() < index);
	m_textures[index] = texture;
}

void sip::Material::addTexture(Texture texture)
{
	m_textures.push_back(texture);
}

Texture* sip::Material::getTexture(int index)
{
	if (index >= (int)m_textures.size()) return nullptr;
	return &m_textures[index];
}

void sip::Material::setMaterialConfigName(PassKey<Director>, const std::string& name)
{
	m_materialConfName = name;
}

void Material::setProgram(GLProgram* prog)
{
	m_program = prog; m_programName = prog->getName();
}

