// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <vector>
#include <map>
#include <istream>
#include <iostream>
#include <glew.h>
#include <stdexcept>
#include <string>
#include <sstream>
#include <fstream>
#include <windows.h>
#include <tuple>
#include <assert.h>
#include <memory>
#include <math.h>
#include <functional>
#include <streambuf>

#include <glm/glm.hpp>

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/transform.hpp>

#include "assimp/mesh.h"
#include "assimp/material.h"
#include "assimp/anim.h"
#include "assimp/scene.h"
#include "assimp/quaternion.h"
#include "assimp/Importer.hpp"
#include "assimp/types.h"
#include "assimp/texture.h"
#include "assimp/vector2.h"
#include "assimp/vector3.h"
#include "assimp/postprocess.h"
#include "nanogui/nanogui.h"
#include "GLFW/glfw3.h"
