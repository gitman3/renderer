#pragma once

#include <string>
#include <GL/glew.h>
#include "Types.h"
#include "Shader.h"


namespace sip
{

	class ShaderLoader
	{
	public:

		static const char * const A_NAME_VCOL;
		static const char * const A_NAME_VPOS;
		static Shader createShader(std::string path);

	private:

		static GLuint link(std::vector<Shader>& shaders);
		static GLuint link(GLuint* shaders, int count);
		static GLuint createShader(const std::string& filename, const std::string& shaderStr, GLint type);
		static GLuint createFromPath(const std::string& path, GLint type);
		static GLuint programFrom(const std::string& path, GLint type);
	};
}
