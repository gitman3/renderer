#pragma once
#include <Map>
#include "RayTracing.h"
#include "Types.h"
#include "glew.h"

namespace sip
{
	class Entity;

	class Picker
	{
		// even every field inside the struct needs to be power of 2, or data is not aligned properly
		struct ComputeData
		{
			glm::vec4 p1; //16
			glm::vec4 p2; //32
			glm::vec4 p3; //48

			glm::vec3 _padding; // 60

			float t; //64

		};

		struct Entry
		{
			ID meshEntryId;
			GLuint bufferId;
			std::vector<ComputeData> m_computeData;
			ComputeData* m_bufferPtr;

			Entry() :
				meshEntryId(0),
				bufferId(0),
				m_computeData(),
				m_bufferPtr(nullptr)
			{}
		};

		static std::map<ID, Entry> sm_data;
		static void initEntries(Entity* entity);

	public:
		static RayTracing::RayCollisionInfo pick(Entity* entity, const RayTracing::Ray& ray);
	};
}
