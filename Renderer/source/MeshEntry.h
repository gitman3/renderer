#pragma once
#include <vector>
#include "glew.h"
#include "glm/vec3.hpp"
#include "Types.h"

struct aiMesh;

namespace sip
{
	class Material;

	class MeshEntry
	{
		MeshEntry();
	public:
		const enum BUFFERS
		{
			VERTEX_BUFFER,
			TEXCOORD_BUFFER,
			NORMAL_BUFFER,
			INDEX_BUFFER,
			VERTEX_COLORS_BUFFER,
			BINORMAL_BUFFER,
			TANGENT_BUFFER,
			VERTEX_WEIGHTS_BUFFER,
			VERTEX_BONE_INDICES_BUFFER,
			COUNT
		};

		static MeshEntry* createPlane(const glm::vec3& pos);
		static MeshEntry* createCube();

		aiMesh* getAIMesh() { return m_aiMesh; }

		void setVertexColor(int vertexIndex, const glm::vec4& color);
		void applyVertexColors();

		std::vector<glm::vec3>& getTriangleList() { return m_triangleList; }
		std::vector<int>& getIndiceList() { return m_indiceList; }

		MeshEntry(aiMesh *mesh);
		Material* getMaterial();
		void setMaterial(Material* mat);
		MaterialKey getMaterialKey() { return m_materialKey; }
		
		int getIndexToMaterial() const;
		float getBoundingSphereRadius() { return m_boundingSphereRadius; }
		const glm::vec3& getOrigin() { return m_origin; }
		void render();
		ID getId() { return m_id; }

		~MeshEntry();
		friend class Camera;
		friend class Animation;
		friend class Batcher;

	private:
		GLuint vao;
		GLuint vbo[COUNT];

		MaterialKey m_materialKey;
		std::vector<glm::vec3> m_triangleList;
		std::vector<int> m_indiceList;

		void buildTriangleList();
		Material* m_material;
		std::string m_materialName;

		unsigned int m_elementCount;
		float m_boundingSphereRadius;
		glm::vec3 m_origin;
		aiMesh* m_aiMesh;
		std::vector<float> m_vertexColors;

		ID m_id;
	};
}