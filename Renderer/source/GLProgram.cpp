#include "GLProgram.h"
#include "ShaderLoader.h"
#include "ShaderCache.h"
#include "Debug.h"
#include "Material.h"
#include "Texture.h"
#include "RenderTarget.h"
#include "DebugConsole.h"
#include "EditorGUI.h"
#include "Types.h"
using namespace sip;

namespace
{
	static unsigned int RunningId = 0;
}

void GLProgram::use(const Material * const material)
{
	glUseProgram(getHandle());
	auto s = glGetUniformLocation(getHandle(), "u_specular");
	auto rp = glGetUniformLocation(getHandle(), "u_rimPower");
	auto ra = glGetUniformLocation(getHandle(), "u_rimAmount");
	auto bp = glGetUniformLocation(getHandle(), "u_bumpPower");
	auto ba = glGetUniformLocation(getHandle(), "u_bumpAmount");

	glUniform1f(s, material->Specular);
	glUniform1f(rp, material->RimPower);
	glUniform1f(ra, material->RimAmount);
	glUniform1f(bp, material->BumpPower);
	glUniform1f(ba, material->BumpAmount);

	glPolygonMode(m_shader.PolygonDrawFaces, m_shader.PolygonDrawMode);

	if (m_shader.isset(BLEND_ENABLED))
	{
		glEnable(GL_BLEND);
		glBlendFunc(m_shader.BlendSrc, m_shader.BlendDst);
	}
	else
	{
		glDisable(GL_BLEND);
	}

	glDepthMask( m_shader.isset(DEPTH_WRITE_ENABLED)? GL_TRUE : GL_FALSE);

	if (m_shader.isset(CULLING_DISABLED))
	{
		glDisable(GL_CULL_FACE);
	}
	else
	{
		glEnable(GL_CULL_FACE);
	}

	if (m_shader.isset(DEPTH_TEST_ENABLED))
	{
		glEnable(GL_DEPTH_TEST);
	}
	else
	{
		glDisable(GL_DEPTH_TEST);
	}


}

void GLProgram::useDepthTexture(const RenderTarget* target)
{
	auto handle = target->m_texHandle;
	GLint uniformLocation = glGetUniformLocation(getHandle(), "u_depthTexture");
	if (uniformLocation < 0) return;
	glActiveTexture(GL_TEXTURE10);
	glBindTexture(GL_TEXTURE_2D, handle);
	glUniform1i(uniformLocation, 10);

	Util::checkGLError("failed to use depth texture");
}

void GLProgram::useCubeMap(const CubeMap* cubeMap)
{
	auto handle = cubeMap->getTexture();
	GLint uniformLocation = glGetUniformLocation(getHandle(), "u_textureCube0");
	if (uniformLocation < 0) return;
	Util::checkGLError("failed to use cubemap");

	glActiveTexture(GL_TEXTURE0 + Material::MAX_TEXTURES * 2);
	Util::checkGLError("failed to use cubemap");
	glBindTexture(GL_TEXTURE_CUBE_MAP, handle);
	Util::checkGLError("failed to use cubemap");
	glUniform1i(uniformLocation, Material::MAX_TEXTURES * 2);
	Util::checkGLError("failed to use cubemap");
}

void GLProgram::useNormalMap(int index, const Texture * const texture) 
{

	auto handle = texture->GetHandle();
	GLint uniformLocation = getNormalMapLocation(index);
	if (uniformLocation < 0) return;

	glActiveTexture(GL_TEXTURE0 + index + Material::MAX_TEXTURES);
	glBindTexture(GL_TEXTURE_2D, handle);
	glUniform1i(uniformLocation, index + Material::MAX_TEXTURES);
}

void GLProgram::useTexture(int index, const Texture * const texture) 
{
	auto handle = texture->GetHandle();
	GLint uniformLocation = getTextureLocation(index);
	if (uniformLocation < 0) return;

	glActiveTexture(GL_TEXTURE0 + index);
	glBindTexture(GL_TEXTURE_2D, handle);
	glUniform1i(uniformLocation, index);

}


GLint GLProgram::getTextureLocation(int index)
{
	switch (index)
	{
	case 0:
		return glGetUniformLocation(getHandle(), "u_texture0");
	case 1:
		return glGetUniformLocation(getHandle(), "u_texture1");
	case 2:
		return glGetUniformLocation(getHandle(), "u_texture2");
	default:
		Debug::logFail(false, "max 3 diffuse textures supported");
	}
	return -1;
}

GLint GLProgram::getNormalMapLocation(int index) 
{
	switch (index)
	{
	case 0:
		return glGetUniformLocation(getHandle(), "u_normalmap0");
	case 1:
		return glGetUniformLocation(getHandle(), "u_normalmap1");
	case 2:
		return glGetUniformLocation(getHandle(), "u_normalmap2");
	default:
		Debug::logFail(false, "max 3 normal maps supported");
	}
	return -1;
}


GLProgram::GLProgram(std::string pathToShader) : m_id(RunningId++), m_shader()
{
	m_shader = ShaderLoader::createShader(pathToShader);
	m_name = pathToShader;
	debug::checkGLError("Error creating shader");
	debug::log("Created shader ", pathToShader.c_str(), " with id ", m_id);
	debug::logFail(m_id < 255, " error creating GL Program");
}

GLProgram::GLProgram() : m_id(RunningId++), m_shader()
{
	debug::log("Created shader (default construct) with id ", m_id);
	debug::logFail(m_id < 255, " error creating GL Program");
}

void GLProgram::reloadShader()
{
	ShaderCache::Instance()->reloadShader(m_name.c_str());
	*this = *ShaderCache::Instance()->getShader(m_name.c_str());
}

void GLProgram::setUniform(const char* uniform, const glm::mat4& mat)
{
	auto pos = glGetUniformLocation(getHandle(), uniform);
	if (pos < 0) return;
	glUniformMatrix4fv(pos, 1, GL_FALSE, glm::value_ptr(mat));
}

void GLProgram::setUniform(const char* uniform, const glm::vec3& vec)
{
	auto pos = glGetUniformLocation(getHandle(), uniform);
	glUniform3f(pos, vec.x, vec.y, vec.z);
}

void GLProgram::setUniform(const char* uniform, const glm::vec2& vec)
{
	auto pos = glGetUniformLocation(getHandle(), uniform);
	glUniform2f(pos, vec.x, vec.y);
}

void GLProgram::setUniform(const char* uniform, float value)
{
	auto pos = glGetUniformLocation(getHandle(), uniform);
	glUniform1f(pos, value);
}

void GLProgram::setUniform(const char* uniform, std::vector<glm::mat4x4>& matrices)
{
	auto pos = glGetUniformLocation(getHandle(), uniform);
	glUniformMatrix4fv(pos, matrices.size(), GL_FALSE, glm::value_ptr(matrices[0]));

}

void GLProgram::setUniform(const char* uniform, int value)
{
	auto pos = glGetUniformLocation(getHandle(), uniform);
	glUniform1i(pos, value);
}

void GLProgram::deleteProgram()
{
	Debug::log("[GLProgram] Shader deleted");
	glDeleteProgram(getHandle());
	m_shader.Handle = 0;
}


GLProgram::~GLProgram()
{

}