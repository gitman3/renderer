#pragma once
#include "MemoryManager.h"
#include "Transform.h"
#include "Handle.h"

namespace sip
{


	template<typename T>
	class Storage
	{
		Memory<T> m_memory;
		MemoryManager<T> m_manager;

		Storage() : 
			m_memory(),
			m_manager(&m_memory)
		{
		}


	public:
		
		Memory<T>* getMemory() { return &m_memory; }
		static MemoryManager<T>& getManager() { return Instance().m_manager; }

		static Storage& Instance()
		{
			static Storage Instance;
			return Instance;
		}

		static T* convert(Handle<T> t) { assert(!t.isDeleted()); return Instance().m_manager.get(t); }
		static Handle<T> newObject() { return Instance().m_manager.newObject(); }

	};

	static Transform* transformPtr(Handle<Transform> h)
	{
		return Storage<Transform>::convert(h);
	}

	static Entity* entityPtr(Handle<Entity> h)
	{
		return Storage<Entity>::convert(h);
	}

}