#include "RenderTarget.h"

using namespace sip;

void RenderTarget::bind()
{
	glBindFramebuffer(GL_FRAMEBUFFER, m_fboHandle);
	if (m_hasTexHandle)
	{
		glBindTexture(GL_TEXTURE_2D, m_texHandle);
	}
	if (m_hasRBOandle)
	{
		glBindRenderbuffer(GL_RENDERBUFFER, m_rboHandle);
	}
}
