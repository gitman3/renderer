#include "Util.h"
#include <string>
#include "GL\glew.h"
#include "Debug.h"
#include <ostream>

using namespace sip;


void Util::checkGLError(const std::string& message)
{
	int err = glGetError();
	if (err != GL_NO_ERROR)
	{
		auto str = glewGetErrorString(err);
		Debug::logFail(false, message, " error code " , err, " " , str);
	}
}

bool Util::fileExists(const std::string& path)
{
	if (FILE *file = fopen(path.c_str(), "r")) 
	{
		fclose(file);
		return true;
	}

	return false;
}

std::string Util::trimPathFrom(const std::string& pathToFile)
{
	int index = pathToFile.find_last_of("/");
	int index2 = pathToFile.find_last_of("\\");
	assert(!(index == std::string::npos && index2 == std::string::npos));

	if (index == std::string::npos)
	{
		index = index2;
	}
	else if (index2 != std::string::npos)
	{
		index = std::max(index, index2);
	}


	std::string path = pathToFile.substr(0, index);
	return path;
}

bool Util::stringContains(const std::string& sourceString, const std::string& searchString)
{
	auto pos = sourceString.find(searchString);
	return pos != std::string::npos;
}

std::string Util::trim(const std::string& str)
{
	auto last = str.find_last_not_of(" \n\r\t");
	auto first = str.find_first_not_of(" \n\r\t");

	first = first == std::string::npos ? 0 : first;
	last = last == std::string::npos ? str.length() : last + 1;
	return str.substr(first, (last - first));
}

std::vector<std::string> Util::splitString(const std::string& str, const char delim)
{
	std::vector<std::string> lines;
	auto cursor = str.begin();
	std::stringstream currentLine;
	while (cursor != str.end())
	{
		if (*cursor == delim)
		{
			lines.push_back(currentLine.str());
			currentLine.str("");
		}
		else
		{
			currentLine << *cursor;
		}
		++cursor;
	}
	lines.push_back(currentLine.str());
	return lines;
}

std::string Util::fileAsString(const std::string& path)
{
	std::ifstream t(path);
	assert(t.good());
	std::stringstream buffer;
	buffer << t.rdbuf();
	return buffer.str();
}

std::string Util::parseFilenameSuffix(const std::string& filename)
{
	auto dotPos = filename.find_last_of(".");
	debug::logFail(dotPos != std::string::npos, filename, "is not a file");
	return std::string(filename.begin() + dotPos + 1, filename.end());
}

namespace 
{
	void listDir(const char * dirn, std::vector<std::string>& list)
	{
		char dirnPath[1024];
		sprintf(dirnPath, "%s\\*", dirn);

		WIN32_FIND_DATA f;
		HANDLE h = FindFirstFile(dirnPath, &f);

		if (h == INVALID_HANDLE_VALUE) { return; }

		do
		{
			const char * name = f.cFileName;

			if (strcmp(name, ".") == 0 || strcmp(name, "..") == 0) { continue; }

			char filePath[1024];
			sprintf(filePath, "%s%s%s", dirn, "\\", name);


			if (f.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				listDir(filePath, list);
			}
			else
			{
				list.push_back(filePath);
			}

		} while (FindNextFile(h, &f));
		FindClose(h);
	}
}



std::vector<std::string> sip::Util::getFileListRecursive(const std::string& path)
{
	std::vector<std::string> fileList;
	listDir(path.c_str(), fileList);
	return fileList;
}

std::string sip::Util::fileFromPath(const std::string& path)
{
	int index = path.find_last_of("/");
	int index2 = path.find_last_of("\\");
	assert( !(index == std::string::npos && index2 == std::string::npos));

	if (index == std::string::npos)
	{
		index = index2;
	}
	else if(index2 != std::string::npos)
	{
		index = std::max(index, index2);
	}

	return std::string(path.begin() + 1 + index, path.end());
}

std::string Util::fileFromPathNoSuffix(const std::string& path)
{
	return Util::splitString(fileFromPath(path), '.')[0];
}

void Util::fixSeparators(std::string& path)
{
	std::replace(path.begin(), path.end(), '\\', '/');
}

void Util::writeToFile(const std::string& path, const std::string& content)
{
	std::ofstream myfile;
	myfile.open(path);
	assert(myfile.good());

	if (myfile.is_open())
	{
		myfile << content;
		myfile.close();
		return;
	}
	assert(false);

}

