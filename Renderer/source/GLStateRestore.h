#pragma once

#include "gl/glew.h"

namespace sip
{
	class GLStateRestore
	{
		GLint m_fboHandle;
		GLint m_texHandle;
		GLint m_rboHandle;
		GLint m_arbufHandle;
		GLint m_progHandle;
		GLint m_vaoHandle;
		GLint m_elemArrHandle;

		int m_options;
	public:
		GLStateRestore(int options);
		~GLStateRestore();

		enum Options
		{
			FBO = 1 << 1,
			RBO = 1 << 2,
			TEX = 1 << 3,
			ARRAY_BUFFER = 1 << 4,
			SHADER = 1 << 5,
			VERTEX_ARRAY = 1 << 6,
			ELEM_ARR = 1 << 7
		};
	};
}
