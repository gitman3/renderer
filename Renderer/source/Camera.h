#pragma once

#include "glm/mat4x4.hpp"
#include "glm/vec3.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "RayTracing.h"

namespace sip
{

	class Entity;
	class MeshEntry;

	class Camera
	{
		glm::mat4 m_projectionMatrix;
		glm::mat4 m_cameraMatrix;
		glm::mat4 m_viewProjectionMatrix;

		// it seems weird that world position is stored separately but actually this MUST be done for the camera to work
		// as for the camera matrix construction the position values inside are negated 
		// so setting consecutively the position causes the values to oscillate back and forth, if getPosition() was used and it would pull the values out of the matrix directly.
		glm::vec3 m_worldPosition;
		glm::vec3 m_target;

		float m_lookaroundx;
		float m_lookaroundy;

		float m_fov;
		float m_near;
		float m_far;
		float m_aspect;

	public:


		Camera();

		inline void setProjection(float fov, float aspect, float n, float f)
		{
			m_fov = fov;
			m_aspect = aspect;
			m_near = n;
			m_far = f;
			m_projectionMatrix = glm::perspective(fov, aspect, n, f);
		}

		inline const glm::vec3 getPosition() { return glm::vec3(m_worldPosition); } 
		inline const glm::mat4& getProjectionMatrix() { return m_projectionMatrix; }
		inline const glm::mat4& getViewMatrix() { return m_cameraMatrix; }
		const glm::mat4& getViewProjectionMatrix();

		inline glm::vec3 getViewDirection() { return m_target; }
		bool cull(MeshEntry * entity, const glm::mat4& model, const glm::vec3& scale);

		RayTracing::Ray screenToWorldRay(const glm::vec2& screenPos);

		void lookAt(const glm::vec3& from, const glm::vec3& to);
		void lookAround(float x, float y);
		void translateLocal(const glm::vec3& by);
		void translateGlobal(const glm::vec3& by);
		friend class Entity;
	};
}

