#pragma once
#include "Types.h"

namespace sip
{


	struct Shader
	{
		GLint Handle;

		unsigned int Flags;
		GLint BlendSrc;
		GLint BlendDst;
		GLint PolygonDrawFaces;
		GLint PolygonDrawMode;
		bool LegacyShader;

		inline bool isset(ShaderFlags flag) const { return (Flags & flag) > 0; }

		Shader(GLint handle) :
			Handle(handle),
			Flags(0),
			BlendSrc(GL_SRC_ALPHA),
			BlendDst(GL_ONE_MINUS_SRC_ALPHA),
			PolygonDrawFaces(GL_FRONT_AND_BACK),
			PolygonDrawMode(GL_FILL),
			LegacyShader(true)
		{}

		Shader() :
			Handle(-1),
			Flags(),
			BlendSrc(GL_SRC_ALPHA),
			BlendDst(GL_ONE_MINUS_SRC_ALPHA),
			PolygonDrawFaces(GL_FRONT_AND_BACK),
			PolygonDrawMode(GL_FILL),
			LegacyShader(true)
		{}
	};


}
