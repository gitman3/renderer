#include "TranslateGizmo.h"
#include "RotationGizmo.h"
#include "MeshCache.h"
#include "Storage.h"
#include "Director.h"
#include "World.h"
#include "Mesh.h"
#include "MeshEntry.h"
#include "Entity.h"
#include "Material.h"
#include "ShaderCache.h"
#include "World.h"
#include "Mouse.h"
#include "Util.h"
#include "Debug.h"
#include "Command.h"
#include "Editor.h"
#include "EditorGUI.h"
#include "Time.h"
#include "Camera.h"
#include "Keyboard.h"
using namespace sip;
using namespace sip::editor;

TranslateGizmo::TranslateGizmo()
	: m_entity(),
	m_selection()
{
	m_entity = Director::Instance().getWorld()->addEntity();
	auto pGizmo = entityPtr(m_entity);
	pGizmo->setMesh(MeshCache::Instance().getMesh("../Renderer/resources/translation.dae"));
	auto mat = pGizmo->getMesh()->getDefaultMaterial();
	mat->setProgram(ShaderCache::Instance()->addShaderAtDefaultPath("translationgizmo"));
	onSelected(m_selection);
	pGizmo->setInspectorSelectable(false);
}

void TranslateGizmo::onSelected(Handle<Entity> selection)
{
	auto gizmoPtr = entityPtr(m_entity);

	if (!selection.isDeleted() && m_selection.isDeleted())
	{
		m_selection = selection;
	}

	if (m_selection.isDeleted())
	{
		m_axis = NONE;
		gizmoPtr->setVisible(false);
		return;
	}

	gizmoPtr->setVisible(true);
	auto selectionEPtr = entityPtr(m_selection);
	auto selectionTPtr = transformPtr(selectionEPtr->getTransform());

	//const auto& localT = selectionTPtr->getLocalTransform();
	//gizmoTptr->setLocalTransform(localT);
	auto gizmoTptr = transformPtr(gizmoPtr->getTransform());
	gizmoTptr->setLocalPosition(selectionTPtr->getLocalPosition());

	auto radius = selectionEPtr->getBoundingSphereRadius();

	gizmoTptr->setScale(radius);

	auto prog = gizmoPtr->getMesh()->getDefaultMaterial()->getProgram();
	prog->use();
	prog->setUniform("u_selectedHandle", 0.0f);

	if (!Mouse::isButtonHold(Mouse::Button::Left))
	{
		m_selection = Handle<Entity>();
		m_axis = NONE;
		return;
	}

	auto world = Director::Instance().getWorld();
	RayTracing::Ray ray = world->mouseToWorldRay();

	float xyNormal = 1.f;
	float xzNormal = 1.f;
	float yzNormal = 1.f;

	auto infoXY = RayTracing::tracePlane(ray, gizmoTptr->getLocalPosition(), glm::vec3(0.0f, 0.0f, 1.0f));
	if (!infoXY.collided())
	{
		infoXY = RayTracing::tracePlane(ray, gizmoTptr->getLocalPosition(), glm::vec3(0.0f, 0.0f, -1.0f));
		xyNormal = -1.f;
	}


	auto infoXZ = RayTracing::tracePlane(ray, gizmoTptr->getLocalPosition(), glm::vec3(0.0f, 1.0f, 0.0f));
	if (!infoXZ.collided())
	{
		infoXZ = RayTracing::tracePlane(ray, gizmoTptr->getLocalPosition(), glm::vec3(0.0f, -1.0f, 0.0f));
		xzNormal = -1.f;
	}

	auto infoYZ = RayTracing::tracePlane(ray, gizmoTptr->getLocalPosition(), glm::vec3(1.0f, 0.0f, 0.0f));
	if (!infoYZ.collided())
	{
		infoYZ = RayTracing::tracePlane(ray, gizmoTptr->getLocalPosition(), glm::vec3(-1.0f, 0.0f, 0.0f));
		yzNormal = -1.f;
	}

	if (!infoXY.collided() && !infoXZ.collided() && !infoYZ.collided()) return;

	glm::vec3 localPosXY = infoXY.pos - selectionTPtr->getLocalPosition();
	glm::vec3 localPosXZ = infoXZ.pos - selectionTPtr->getLocalPosition();
	glm::vec3 localPosYZ = infoYZ.pos - selectionTPtr->getLocalPosition();
	float size = .3f * radius;
	float offset = .5f * radius;
	auto target = localPosXY;

	float speed = Time::deltaTime();
	if (m_axis == Plane::NONE)
	{
		if (localPosXY.y >= offset && localPosXY.y < offset + size && localPosXY.x >= offset && localPosXY.x <= offset + size)
		{
			m_axis = Plane::XY;
		}
		else if (localPosXZ.x >= offset && localPosXZ.x < offset + size && localPosXZ.z >= offset && localPosXZ.z <= offset + size)
		{
			m_axis = Plane::XZ;
		}
		else if (localPosYZ.y >= offset && localPosYZ.y < offset + size && localPosYZ.z >= offset && localPosYZ.z <= offset + size)
		{
			m_axis = Plane::YZ;
		}
	}

	if (m_axis == Plane::NONE) return;

	RayTracing::RayCollisionInfo selectedInfo;

	glm::vec3 plane;
	glm::vec3 dirVec;
	switch (m_axis)
	{
	case Plane::XY:
		plane = glm::vec3(0.0f, 0.0f, xyNormal);
		prog->setUniform("u_selectedHandle", 1.0f);
		selectedInfo = infoXY;
		dirVec = { 1.0f, 0.0f, 0.0f };
		break;
	case Plane::XZ:
		plane = glm::vec3(0.0f, xzNormal, 0.0f);
		prog->setUniform("u_selectedHandle", 2.0f);
		selectedInfo = infoXZ;
		dirVec = { 0.0f, 0.0f, 1.0f };
		break;
	case Plane::YZ:
		plane = glm::vec3(yzNormal, 0.0f, 0.0f);
		prog->setUniform("u_selectedHandle", 3.0f);
		selectedInfo = infoYZ;
		dirVec = { 0.0f, 1.0f, 0.0f };
		break;
	default:
		assert(false);
	}

	auto prevRayXY = Director::Instance().getWorld()->getActiveCamera()->screenToWorldRay({ Mouse::getX() - Mouse::getDeltaX(), Mouse::getY() - Mouse::getDeltaY() });
	auto fromPos = RayTracing::tracePlane(prevRayXY, gizmoTptr->getLocalPosition(), plane);

	if (Keyboard::isKeyHold(Keyboard::CONTROL))
	{
		fromPos.pos *= dirVec;
		selectedInfo.pos *= dirVec;
	}

	Editor::Instance().translateLocal(m_selection, selectedInfo.pos - fromPos.pos);
}

