#pragma once
#include <memory>
#include "Types.h"

/** Caches materials for each scene to separate entry (supports multiple scenes).*/
struct aiMaterial;

namespace sip
{
	class Material;

	class MaterialCache
	{
	public:
		static std::string CUSTOM_MATERIALS_KEY;
		enum CustomKeys
		{
			POS_COL_LIGHT_PROJ_NOTEXTURES_KEY,
			EDITOR_WIREFRAME_KEY,
			SCALE_GIZMO_KEY,
			CUBEMAPPED
		};

		Material* createMaterial(const aiMaterial* mat, const std::string& filename, int indexInScene);
		void cacheMaterial(MaterialKey key, std::shared_ptr<Material> mat);
		Material* getMaterial(MaterialKey key);
		Material* getCustomMaterialWithKey(CustomKeys key);

		Material* getWithUniqueName(const std::string& key);

		void refreshMaterials();

		static MaterialKey getCustomMaterialKey(CustomKeys key);

		static MaterialCache& Instance()
		{
			static MaterialCache c;
			return c;
		}
	private:
		std::map<MaterialKey, std::shared_ptr<Material>> m_materials;

	};
}
