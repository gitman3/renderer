#include "Command.h"
#include "Entity.h"
#include "Transform.h"
#include "LuaDataLoader.h"
#include "Director.h"
#include "World.h"
#include "EditorGUI.h"
#include "Storage.h"

using namespace sip;
using namespace sip::editor;

/////// Move entity
MoveCommand::MoveCommand(Handle<Entity> entity, const glm::vec3& moveBy)
{
	m_moveFrom = glm::vec3(transformPtr(entityPtr(entity)->getTransform())->getLocalPosition());
	m_moveBy = moveBy;
	m_entity = entity;
}

void MoveCommand::execute()
{
	transformPtr(entityPtr(m_entity)->getTransform())->translateLocal(m_moveBy);
}

void MoveCommand::executeUndo()
{
	transformPtr(entityPtr(m_entity)->getTransform())->setLocalPosition(m_moveFrom);
}

bool MoveCommand::append(Command* other)
{
	auto move = dynamic_cast<MoveCommand*>(other);
	if (!move) return false;
	m_moveBy = move->m_moveBy;
	return true;
}

///////////////////////////
//////// Create entity //// 
///////////////////////////
void CreateEntityCommand::execute()
{
	auto prefab = LuaDataLoader::Instance().loadPrefab(m_pathToPrefab);
	m_entity = Director::Instance().getWorld()->addEntity(prefab);

	EditorGUI::log("Instantiating prefab : ", prefab.Name);
}

void CreateEntityCommand::executeUndo()
{
	Director::Instance().getWorld()->deleteEntity(m_entity);
	EditorGUI::Instance().getInspector()->onSelected(Handle<Entity>());
}

CreateEntityCommand::CreateEntityCommand(const std::string& pathToPrefab) : m_entity()
{
	m_pathToPrefab = pathToPrefab;
}


//////////////////////////////
///////// Delete entity  /////
//////////////////////////////
DeleteEntityCommand::DeleteEntityCommand(Handle<Entity> entity)
{
	m_entity = entity;
}

void DeleteEntityCommand::execute()
{
	Director::Instance().getWorld()->deleteEntity(m_entity);
	EditorGUI::Instance().getInspector()->onSelected(Handle<Entity>());
}

void DeleteEntityCommand::executeUndo()
{
	Storage<Entity>::Instance().getManager().rescurrectObject(m_entity);
	auto transform = entityPtr(m_entity)->getTransform();
	Storage<Transform>::Instance().getManager().rescurrectObject(transform);

}
//////////////////////////////
///////// RotateCommand  /////
//////////////////////////////

RotateCommand::RotateCommand(Handle<Entity> entity, const glm::quat& rotateBy) : 
	m_entity(entity), 
	m_rotateBy(rotateBy)
{
	m_rotateFrom = entityPtr(entity)->getTransformPtr()->getRotation();
}

void RotateCommand::execute()
{
	auto tpr = entityPtr(m_entity)->getTransformPtr();
	tpr->setRotation(tpr->getRotation() * m_rotateBy);
}

void RotateCommand::executeUndo()
{
	entityPtr(m_entity)->getTransformPtr()->setRotation(m_rotateFrom);
}

bool RotateCommand::append(Command* other)
{
	auto rot = dynamic_cast<RotateCommand*>(other);
	if (!rot) return false;
	m_rotateBy = rot->m_rotateBy;
	return true;
}
////////////////////////////////////
/////// ScaleCommand::ScaleCommand
////////////////////////////////////
ScaleCommand::ScaleCommand(Handle<Entity> entity, glm::vec3 scaleBy) :
	m_scaleBy(scaleBy),
	m_entity(entity),
	m_scaleFrom()
{
	m_scaleFrom = entityPtr(entity)->getTransformPtr()->getScale();
}

void ScaleCommand::execute()
{
	auto tptr = entityPtr(m_entity)->getTransformPtr();
	tptr->setScale(tptr->getScale().x + m_scaleBy.x);
}

void ScaleCommand::executeUndo()
{
	entityPtr(m_entity)->getTransformPtr()->setScale(m_scaleFrom.x);
}


bool ScaleCommand::append(Command* other)
{
	auto rot = dynamic_cast<ScaleCommand*>(other);
	if (!rot) return false;
	m_scaleBy = rot->m_scaleBy;
	return true;
}
