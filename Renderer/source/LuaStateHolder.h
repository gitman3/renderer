#pragma once

#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"

namespace sip
{
	class LuaStateHolder
	{
	private:
		lua_State* m_luaState;

		LuaStateHolder()
		{
			m_luaState = luaL_newstate();
			luaL_openlibs(m_luaState);
		}

	public:

		static LuaStateHolder& Instance() {
			static LuaStateHolder sm_instance;
			return sm_instance;
		}

		lua_State* getState()
		{
			return m_luaState;
		}

	};
}

