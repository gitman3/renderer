#pragma once

//#include "Transform.h"
#include <vector>
#include "glm/mat4x4.hpp"
#include <memory>
#include "Types.h"
#include "Handle.h"
#include "Prefab.h"

namespace sip
{

	class Material;
	class GLProgram;
	class World;
	class Mesh;
	class Transform;
	class Component;
	class Prefab;

	class Entity
	{
		Mesh* m_mesh; // owned by cache, raw pointer ok

		Handle<Transform> m_transform;
		std::shared_ptr<Prefab> m_prefab; // should be ok.
		Material* m_material; // owned by materialCache

		std::string m_name;
		ObjectID m_id;

		int m_tag;
		void* m_userData;
		bool m_inspectorSelectable;
		bool m_visible;

	public:

		Entity();
		Handle<Entity> this_handle;

		void setPrefab(const Prefab& prefab);
		Prefab* getPrefab() { return m_prefab.get(); }

		unsigned int _mem_id() { return m_id; }

		inline void  setUserData(void* data) { m_userData = data; }
		inline void* getUserData() { return m_userData; }

		inline void setTag(int tag) { m_tag = tag; }
		inline int  getTag() { return m_tag; }
		inline ObjectID getId() { return m_id; }
		inline Material* getMaterial() { return m_material; }

		void setMesh(Mesh* mesh);
		Mesh* getMesh();
		Handle<Transform> getTransform();

		void refreshShaders();
		void update();
		void setInspectorSelectable(bool selectable) { m_inspectorSelectable = selectable; }
		void setMaterialConf(const std::string& matName);

		/** This sets the material for the WHOLE object, if there are more mesh entries, this is not what you want*/
		void setMaterial(Material* material);
		bool getInspectorSelectable() { return m_inspectorSelectable; }
		void setVisible(bool visible);
		bool isVisible() const;

		float getBoundingSphereRadius() const;
		glm::vec4 getAABB() const;
		Transform* getTransformPtr();

		std::string& getName() { return m_name; }
		void setName(const std::string& name) { m_name = name; }

		friend class World;
		friend class Material;
		
	};
}
