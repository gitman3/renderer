#include "ShaderCache.h"
#include "ShaderLoader.h"
#include "Util.h"
using namespace sip;

const char * const ShaderCache::SHADERS_PATH = "../Renderer/shaders/";

const char * const ShaderCache::SNAME_DEPTH = "../Renderer/shaders/depthpass";
const char * const ShaderCache::SNAME_POS_COL = "../Renderer/shaders/pos_col";
//const char * const ShaderCache::SNAME_POS_COL_LIGHT_PROJ = "../Renderer/shaders/pos_col_light_proj";
const char * const ShaderCache::SNAME_POS_COL_LIGHT_PROJ_NOTEXTURES = "../Renderer/shaders/pos_col_light_proj_notextures";
const char * const ShaderCache::SNAME_POS_COL_LIGHT_PROJ_TEXTURED = "../Renderer/shaders/pos_col_light_proj_textured";
const char * const ShaderCache::SNAME_POS_COL_LIGHT_PROJ_TEXTURED_OPAQUE = "../Renderer/shaders/pos_col_light_proj_textured_opaque";
const char * const ShaderCache::SNAME_COMPUTE_RAY_MESH_COLLISION = "../Renderer/shaders/compute_ray_mesh_collision";
const char * const ShaderCache::SNAME_EDITOR_WIREFRAME = "../Renderer/shaders/editor_wireframe";
const char * const ShaderCache::SNAME_CUBEMAP = "../Renderer/shaders/cubemap";

void ShaderCache::reloadShader(const char* path)
{
	auto shader = m_shaderCache[path];
	shader.deleteProgram();
	addShader(path, GLProgram(path));
}

void ShaderCache::loadDefaultShaders()
{
	addShader(SNAME_DEPTH, GLProgram(SNAME_DEPTH));
	addShader(SNAME_POS_COL, GLProgram(SNAME_POS_COL));
	addShader(SNAME_POS_COL_LIGHT_PROJ_NOTEXTURES, GLProgram(SNAME_POS_COL_LIGHT_PROJ_NOTEXTURES));
	addShader(SNAME_POS_COL_LIGHT_PROJ_TEXTURED, GLProgram(SNAME_POS_COL_LIGHT_PROJ_TEXTURED));
	addShader(SNAME_POS_COL_LIGHT_PROJ_TEXTURED_OPAQUE, GLProgram(SNAME_POS_COL_LIGHT_PROJ_TEXTURED_OPAQUE));
	addShader(SNAME_COMPUTE_RAY_MESH_COLLISION, GLProgram(SNAME_COMPUTE_RAY_MESH_COLLISION));
	addShader(SNAME_EDITOR_WIREFRAME, GLProgram(SNAME_EDITOR_WIREFRAME));
	addShader(SNAME_CUBEMAP, GLProgram(SNAME_CUBEMAP));
	addShaderAtDefaultPath("compute_vertex_ao");

}

GLProgram* ShaderCache::getShader(const std::string& path)
{
	assert(m_shaderCache.find(path) != m_shaderCache.end());
	return &m_shaderCache.at(path);
}

void ShaderCache::addShader(const char* path, const GLProgram& shader)
{
	m_shaderCache.insert(std::make_pair(path, shader));
}

GLProgram* ShaderCache::addShaderAtDefaultPath(const char* path)
{
	std::stringstream ss;
	ss << "../Renderer/shaders/" << path;
	const std::string& str = ss.str();
	const char* pth = str.c_str();

	addShader(pth, GLProgram(pth));
	return &m_shaderCache[pth];
}

void ShaderCache::reloadAll(PassKey<World>)
{
	Util::checkGLError("");
	for (auto kp : m_shaderCache)
	{
		std::cout << "Reloading" << kp.first << "\n";
		kp.second.deleteProgram();
		m_shaderCache[kp.first] = GLProgram(kp.first);
	}
}

sip::GLProgram* sip::ShaderCache::getShaderAtDefaultPath(const std::string& path)
{
	return getShader("../Renderer/shaders/" + path);
}

