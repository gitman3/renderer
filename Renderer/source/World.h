#pragma once

#include <vector>
#include <memory>
#include "glm/vec3.hpp"
#include "Passkey.h"
#include "Types.h"
#include "Handle.h"
#include "RayTracing.h"

namespace sip
{
	class Entity;
	class Transform;
	class Camera;
	class Prefab;

	class World
	{

		std::string m_pathToConfig;
		glm::vec3 m_ambientLightColor;
		glm::vec3 m_globalLightDirection;
		glm::vec3 m_globalLightColor;
		glm::vec3 m_bgColor;
		std::shared_ptr<Camera> m_activeCamera;
		Handle<Entity> m_rootEntity;
		void refreshWorldConfig();

	public:
		static void recursiveGetChildren(Handle<Transform> from, std::vector<Handle<Transform>>& children);
		void recursiveGetChildrenAll(std::vector<Handle<Transform>>& children) const;

		World(const char* pathToConfig);
		~World();

		glm::vec3 getCameraPosition();
		Camera* getActiveCamera();

		void refreshMaterials();
		void render();
		void update();
		void rebuildWorldTransforms();
		glm::vec3 getAmbientLightColor() { return m_ambientLightColor; }
		glm::vec3 getSunDirection() { return m_globalLightDirection; }
		glm::vec3 getSunLightColor() { return m_globalLightColor; }
		glm::vec3 getBGColor() { return m_bgColor; }

		RayTracing::Ray mouseToWorldRay();

		// Camera function delegates
		glm::vec3 getViewDirection();
		void translateLocal(const glm::vec3& vec);
		void translateGlobal(const glm::vec3& vec);
		void lookAround(float x, float y);
		void lookAt(const glm::vec3& from, const glm::vec3& at);
		void deleteEntity(Handle<Entity> entity);

		Handle<Entity> getRawEntity();
		Handle<Entity> addEntity();
		Handle<Entity> addEntity(const Prefab& prefab);
		friend class Entity;
		friend class Director;
	};

}
