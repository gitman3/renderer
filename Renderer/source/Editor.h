#pragma once
#include <memory>
#include "Command.h"
#include "Handle.h"

namespace sip
{

	class Entity;

	namespace editor
	{
		class Inspector;

		class Editor
		{

			class CommandQueue
			{
				std::vector<std::unique_ptr<Command>> m_queue;
			public:
				CommandQueue();
				void add(std::unique_ptr<Command> command);
				void executeNextCommand();
				void undoCurrentCommand();
				void update();
				Command* getLastCommand();
			};

			CommandQueue m_commandQueue;
			Editor();

			void createEditorMaterials();

		public:

			static Editor& Instance()
			{
				static Editor instance;
				return instance;
			}

			void translateLocal(Handle<Entity> ent, const glm::vec3& pos);
			void rotateLocal(Handle<Entity> ent, const glm::fquat& rot);
			void scaleLocal(Handle<Entity> ent, const glm::vec3& amount);


			void update();
			
			Handle<Entity> createPlane(glm::vec3 position);
			Handle<Entity> pickEntity(int screenX, int screenY);
			Handle<Entity> entityFromPrefab(const std::string& path);


			void deleteEntity(Handle<Entity> entity);
			
			void init();
		};



	}
}