#pragma once

#include <vector>
#include <GL\glew.h>
#include "glm/vec4.hpp"
#include "glm/mat4x4.hpp"

namespace sip
{

	class DrawPrimitives
	{
		GLuint m_posvboHandle;
		GLuint m_colvboHandle;
		GLuint m_normvboHandle;

		GLuint m_vaoHandle;

		GLuint m_program;

		std::vector <glm::vec4> m_posData;
		std::vector <glm::vec4> m_colData;
		std::vector <glm::vec3> m_normalData;

	public:

		DrawPrimitives();

		inline void setProgram(GLuint prog)
		{
			m_program = prog;
		}

		inline GLint getProgram() { return m_program; }

		void addQuad(const glm::vec4& color, const glm::vec4& tl, const glm::vec4& tr, const glm::vec4& bl, const glm::vec4& br);
		void addQuad(const glm::mat4x4& transform, const glm::vec4& color, const glm::vec4& tl, const glm::vec4& tr, const glm::vec4& bl, const glm::vec4& br);
		void draw();
		void clear();

	};
}

