#include "nanogui/nanogui.h"
#include "DebugConsole.h"
#include "Director.h"
#include "Context.h"

using namespace sip;
using namespace nanogui;

namespace 
{
	Widget* ptrCanvas;
	Window* ptrWindow;
	const int MAX_MESSAGES = 20;
	const int CONSOLE_HEIGHT = 200;
}

editor::DebugConsole::DebugConsole() : 
	m_pendingText(), 
	m_messages()
{
	auto screen = Director::Instance().getContext()->getScreen();
	ptrWindow = new nanogui::Window(screen, "DebugConsole");
	ptrWindow->setLayout(new GroupLayout());
	nanogui::VScrollPanel* scrollPanel = new nanogui::VScrollPanel(ptrWindow);

	auto size = Director::Instance().getContext()->getScreenSize();
	size.x -= 30;
	size.y -= 50;
	scrollPanel->setFixedSize(Eigen::Vector2i(size.x, CONSOLE_HEIGHT));

	ptrCanvas = new Widget(scrollPanel);
	ptrCanvas->setLayout(new GroupLayout());
	auto layout = dynamic_cast<GroupLayout*>(ptrCanvas->layout());
	layout->setGroupIndent(0);
	layout->setGroupSpacing(0);
	layout->setSpacing(0);
	layout->setMargin(0);

	ptrCanvas->setFixedSize(Vector2i(size.x,  CONSOLE_HEIGHT));
	ptrWindow->setPosition(Vector2i(0, size.y - CONSOLE_HEIGHT));

	screen->setVisible(true);
	screen->performLayout();
	
}


void editor::DebugConsole::log(const std::string& message, Level level /*= NORMAL*/)
{
	m_messages.push_back( Message {  message, level });

	if (m_messages.size() > MAX_MESSAGES)
	{
		m_messages.erase(m_messages.begin());
	}

	nanogui::Label* label = new Label(ptrCanvas, message);
	label->setFontSize(20);
	auto size = Director::Instance().getContext()->getScreenSize();

	switch (level)
	{
	case DebugConsole::NORMAL:
		label->setColor(nanogui::Color(0,   255, 0, 255));
		break;
	case DebugConsole::Level::WARNING:
		label->setColor(nanogui::Color(255, 255, 0, 255));
		break;
	case DebugConsole::FAIL:
		label->setColor(nanogui::Color(255, 0, 0, 255));
		break;
	}
	m_pendingText = "";
	Context::Instance().getScreen()->performLayout();

}

void editor::DebugConsole::setVisible(bool visible)
{
	ptrWindow->setVisible(visible);
}

bool editor::DebugConsole::visible()
{
	return ptrWindow->visible();
}

void editor::DebugConsole::toggleVisible()
{
	ptrWindow->setVisible(!ptrWindow->visible());
}

