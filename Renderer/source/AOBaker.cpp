#include <math.h>

#include "AOBaker.h"
#include "Entity.h"
#include "Storage.h"
#include "Mesh.h"
#include "MeshEntry.h"
#include "Transform.h"
#include "Entity.h"
#include "EditorGUI.h"
#include "Debug.h"
#include <glm/gtc/matrix_access.hpp>
#include "ShaderCache.h"
#include "World.h"
#include "Director.h"

using namespace sip;

//#define DO_DEBUG
const float sip::AOBaker::RAYS_PER_VERTEX = 300.0f;

namespace 
{
	static const int WORK_GROUPS = 1024;

	std::vector<MeshEntry*>* EntriesToBake;
	int IndexToCurrentVertex;
	Transform* PtrTargetTransform;
	Entity* PtrTargetEntity;
	Handle<Entity> HandleToEntity;
	int IndexToCurrentOccludeeMeshEntry = 0;
	
}


void AOBaker::bakeAO(Handle<Entity> ent)
{
	if (ent.isDeleted()) return;

	auto world = Director::Instance().getWorld();
	std::vector<Handle<Transform>> children;
	world->recursiveGetChildrenAll(children);

	std::vector<Handle<Entity>> occluders;

	for (auto t : children)
	{
		if (t.isDeleted()) continue;
		auto transform = transformPtr(t);
		auto ent = transform->getEntity();
		if (ent.isDeleted()) continue;
		auto ePtr = entityPtr(ent);

		// custom created objects might not have prefabs
		if (!ePtr->getPrefab()) continue;

		// TODO: take distance to accout
		if (ePtr->getPrefab()->isAOOccluder)
		{
			occluders.push_back(ent);
		}
	}

	initBake(ent, occluders);

}

void AOBaker::initBake(Handle<Entity> entity, std::vector<Handle<Entity>> sources)
{
	IndexToCurrentOccludeeMeshEntry = 0;

	m_occluderData.clear();
	m_baking = true;
	IndexToCurrentVertex = 0;
	HandleToEntity = entity;
	PtrTargetEntity = entityPtr(entity);
	PtrTargetTransform = PtrTargetEntity->getTransformPtr();

	auto mesh = PtrTargetEntity->getMesh();
	EntriesToBake = &mesh->getMeshEntries();



	initOccluders(sources);
}

bool AOBaker::isBaking()
{
	return m_baking;
}

void AOBaker::update()
{
	if (!m_baking) return;

	if (HandleToEntity.isDeleted())
	{
		m_baking = false;
		return;
	}


	

	MeshEntry* entry = (*EntriesToBake)[IndexToCurrentOccludeeMeshEntry]; 
	int COUNT = entry->getAIMesh()->mNumVertices;

	std::cout << IndexToCurrentVertex << " / " << COUNT << "\n";


	std::vector<glm::vec4> occlusions;
	occlusions.reserve(VERTICES_PER_BATCH);

	bakeInternal(entityPtr(HandleToEntity), occlusions);

	for (uint i = 0; i < occlusions.size(); ++i)
	{
#ifdef DO_DEBUG
		entry->setVertexColor(IndexToCurrentVertex + i, glm::vec4(glm::vec3(occlusions[i]), 1.0f));
#else
		auto color = 1.0f - occlusions[i].x ;
		entry->setVertexColor(IndexToCurrentVertex + i, glm::vec4(color, color, color, 1.0f));
#endif

	}
	
	IndexToCurrentVertex += AOBaker::VERTICES_PER_BATCH;
	entry->applyVertexColors();

	if (IndexToCurrentVertex >= COUNT)
	{
		++IndexToCurrentOccludeeMeshEntry;
		editor::EditorGUI::log("AO Baked.");
		IndexToCurrentVertex = 0;

		if (IndexToCurrentOccludeeMeshEntry >= (int)EntriesToBake->size())
		{

			m_baking = false;
			return;
		}
	}
}




void AOBaker::initOccluders(std::vector<Handle<Entity>>& entities)
{
	for (auto hEntity : entities)
	{
		auto entity = entityPtr(hEntity);
		auto mesh = entity->getMesh();
		auto entriesCount = mesh->getMeshEntries().size();

		for (auto meshEntry : mesh->getMeshEntries())
		{

			auto& triangles = meshEntry->getTriangleList();
			if (triangles.size() == 0)
			{
				continue;
			}


			auto transform = entity->getTransformPtr();
			auto modelMat = transform->getLocalTransform();

			for (unsigned int i = 0; i < meshEntry->getTriangleList().size(); i = i + 3)
			{
				m_occluderData.push_back( { modelMat * glm::vec4(triangles[i], 1.f), modelMat * glm::vec4(triangles[i + 1], 1.0f), modelMat * glm::vec4(triangles[i + 2], 1.0f) });
			}

			auto structSize = sizeof(OccluderData);
			GLuint bufSize = m_occluderData.size() * structSize;
			glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, OccluderSSBO);
			glBufferData(GL_SHADER_STORAGE_BUFFER, bufSize, &m_occluderData[0], GL_DYNAMIC_COPY);
			glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

			int err = glGetError();
			debug::logFail(err == GL_NO_ERROR, "Error initializing compute shader");
		}
	}

}


bool sip::AOBaker::initOccludee(Entity* entity, int vertexIndex)
{
	auto mesh = entity->getMesh();
	auto entriesCount = mesh->getMeshEntries().size();
	auto transform = entity->getTransformPtr();
	auto modelMat = transform->getLocalTransform();

	for (auto meshEntry : mesh->getMeshEntries())
	{

		std::unique_ptr<OccludeeData> oData (new OccludeeData());

		auto aiMesh = meshEntry->getAIMesh();
		assert(aiMesh->HasNormals());
		assert(aiMesh->HasPositions());
		int count = 0;

		for (int i = vertexIndex; i < vertexIndex + VERTICES_PER_BATCH; ++i)
		{
			if (i >= (int)meshEntry->getIndiceList().size())
			{
				continue;
			}

			auto indice = meshEntry->getIndiceList()[i];

			auto vert = aiMesh->mVertices[indice];
			auto norm = aiMesh->mNormals[indice];
			
			oData->RDs[count] = modelMat * glm::vec4(norm.x, norm.y, norm.z, 0.0f);
			oData->ROs[count] = modelMat * glm::vec4(vert.x, vert.y, vert.z, 1.0f);
			oData->Occlusions[count] = glm::ivec4(0);
			++count;
		}

		auto structSize = sizeof(OccludeeData);

		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, OccludeeSSBO);
		auto ptr = oData.get();
		glBufferData(GL_SHADER_STORAGE_BUFFER, structSize, ptr, GL_DYNAMIC_COPY);
		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

		int err = glGetError();
		debug::logFail(err == GL_NO_ERROR, "Error initializing compute shader");
	}
	return true;
}

bool AOBaker::bakeInternal(Entity* entity, std::vector<glm::vec4>& occlusions)
{

	if (!initOccludee(entity, IndexToCurrentVertex))
	{
		return false;
	}

	auto shader = ShaderCache::Instance()->getShaderAtDefaultPath("compute_vertex_ao");
	shader->use();
	shader->setUniform("ModelMat", transformPtr(entity->getTransform())->getLocalTransform());
	shader->setUniform("RaysPerVertex", RAYS_PER_VERTEX);
	shader->setUniform("PositionsPerCall", VERTICES_PER_BATCH);
	// dispatch compute for tracing.
	glDispatchCompute(glm::max(1, (int)m_occluderData.size() / WORK_GROUPS), 1, 1);
	glMemoryBarrier(GL_ALL_BARRIER_BITS);

	// read back data for occludee.
	GLuint bufSize = sizeof(OccludeeData);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, OccludeeSSBO);
	auto ptr = glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, bufSize, GL_MAP_READ_BIT);
	auto bufferPtr = reinterpret_cast<OccludeeData*>(ptr);

	for (int i = 0; i < VERTICES_PER_BATCH; ++i)
	{

#ifdef DO_DEBUG
		occlusions.push_back(bufferPtr->Debug);
#else
		occlusions.push_back(glm::vec4(bufferPtr->Occlusions[i]) / RAYS_PER_VERTEX);
#endif
	}

	glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
	return true;
}


sip::AOBaker::AOBaker()
{
	ShaderCache::Instance()->addShaderAtDefaultPath("compute_vertex_ao");
	glGenBuffers(1, &OccludeeSSBO);
	glGenBuffers(1, &OccluderSSBO);
	m_occluderData.reserve(150000);
}

