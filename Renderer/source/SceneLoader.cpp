#include <sstream>

#include <stdio.h>
#include <string.h>
#include <windows.h>
#include <iostream>

#include "SceneLoader.h"
#include "LuaEngine.h"
#include "Util.h"
#include "LuaTable.h"
#include "Debug.h"
#include "EditorGUI.h"
#include "Editor.h"
#include "Storage.h"
#include "Entity.h"
#include "Transform.h"
#include "LuaDataLoader.h"
#include "CubeMap.h"
#include "Director.h"
#include "World.h"
#include "Mesh.h"
#include "ShaderCache.h"
#include "Material.h"
#include "TextureManager.h"

using namespace sip;

namespace
{
	Project* _project;

	int projectFileLoaded(lua_State* state)
	{
		LuaTable tbl(state, true);
		_project->pathToAssetsFolder = tbl.getString("ASSETS_PATH");
		return 0;
	}


}

SceneLoader::SceneLoader()
{
	_project = nullptr;
	lua_register(LuaEngine::instance().getState(), "projectFileLoaded", projectFileLoaded);
}


void SceneLoader::prepareProject( Project& project)
{
	 editor::EditorGUI::log("Preparing project...");

	 auto fileList = Util::getFileListRecursive(project.pathToAssetsFolder);
	 for (auto& str : fileList)
	 {
		 Util::fixSeparators(str);
		 std::cout << str << "\n";

		 if (Util::parseFilenameSuffix(str) == "prefab")
		 {
			 editor::EditorGUI::log("prefab : ", str);
			 project.prefabPaths[Util::fileFromPath(str)] = str;
		 }
		 else if (Util::parseFilenameSuffix(str) == "scene")
		 {
			 editor::EditorGUI::log("scene : ", str);
			 project.scenePaths[Util::fileFromPath(str)] = str;
		 }
		 else if (Util::parseFilenameSuffix(str) == "scenemeta")
		 {
			 editor::EditorGUI::log("meta : ", str);
			 project.sceneMetaFiles[Util::fileFromPath(str)] = str;
		 }
		 else if (Util::parseFilenameSuffix(str) == "mat")
		 {
			 editor::EditorGUI::log("material : ", str);
			 project.materialPaths[Util::fileFromPath(str)] = str;

		 }
		 else if (Util::parseFilenameSuffix(str) == "cubemap")
		 {
			 editor::EditorGUI::log("cubemap : ", str);
			 project.cubemapPaths[Util::fileFromPath(str)] = str;
		 }
		 else
		 {
			 std::cout << str << "\n";
		 }
	 }
}

void sip::SceneLoader::loadScene(const std::string& pathToFile)
{
	m_loadedScene =  Scene(pathToFile);
	m_loadedProject = loadProjectFile(m_loadedScene.getProjectFilePath());
	prepareProject(m_loadedProject);

	for (auto pair : m_loadedProject.prefabPaths)
	{
		std::cout << pair.first << " = " << pair.second << "\n";
	}

	for (auto pair : m_loadedProject.scenePaths)
	{
		std::cout << pair.first << " = " << pair.second << "\n";
	}

	for (auto pair : m_loadedProject.materialPaths)
	{
		std::cout << pair.first << " = " << pair.second << "\n";
		LuaDataLoader::Instance().loadMaterial(pair.second);
	}

	for (auto pair : m_loadedProject.sceneMetaFiles)
	{
		std::cout << pair.first << " = " << pair.second << "\n";
		auto sceneName = Util::fileFromPathNoSuffix(m_loadedScene.getPath());
		auto metaName = Util::fileFromPathNoSuffix(pair.second);

		if (sceneName == metaName)
		{
			LuaDataLoader::Instance().loadSceneMeta(pair.second, m_loadedScene);
		}
	}

	for (auto pair : m_loadedProject.cubemapPaths)
	{
		std::cout << pair.first << " = " << pair.second << "\n";
		TextureManager::Instance().addCubeMapIfMissing(pair.first, LuaDataLoader::Instance().loadCubeMap(pair.second));
	}

	if (m_loadedScene.getMetaTable().isString("cubemap"))
	{
		auto cubeMapName = m_loadedScene.getMetaTable().getString("cubemap");

		auto hEntity = Director::Instance().getWorld()->addEntity();
		auto pEntity = entityPtr(hEntity);
		auto cubeMesh = Mesh::createCubeMapMesh();
		cubeMesh->getDefaultMaterial()->setCubeMap(TextureManager::Instance().getCubeMap(cubeMapName));
		pEntity->setMesh(cubeMesh);
		pEntity->setInspectorSelectable(false);
	}

	auto tables = m_loadedScene.getEntityTables();
	for (auto& table : tables)
	{
		auto prefabName = table.getString("prefab");

		assert(m_loadedProject.prefabPaths.find(prefabName) != m_loadedProject.prefabPaths.end());

		auto entity = editor::Editor::Instance().entityFromPrefab(m_loadedProject.prefabPaths.at(prefabName));
		auto ep = entityPtr(entity);
		ep->setName(table.getString("name"));
		auto transformTable = table.getTable("transform");

		auto tr = transformPtr(ep->getTransform());
		{
			auto posTable = transformTable.getTable("position");
			tr->setLocalPosition(glm::vec3(posTable.getFloat("x"), posTable.getFloat("y"), posTable.getFloat("z")));
		}

		{
			auto scaleTable = transformTable.getTable("scale");
			tr->setScale(scaleTable.getFloat("x"));
		}

		{
			auto rotationTable = transformTable.getTable("rotation");
			glm::fquat quat;
			float x = rotationTable.getFloat("x");
			float y = rotationTable.getFloat("y");
			float z = rotationTable.getFloat("z");
			float w = rotationTable.getFloat("w");
			quat.x = x; quat.y = y; quat.z = z; quat.w = w;
			tr->setRotation(quat);
		}
	}
}

Project sip::SceneLoader::loadProjectFile(const std::string& pathToProject)
{
	Project project;
	_project = &project;

	auto src = Util::fileAsString(pathToProject);
	src += "\n";
	src += "projectFileLoaded(config)\n";

	std::string start = "local config = {}\n";
	start += src;
	LuaEngine::instance().executeScriptString(start.c_str());
	_project = nullptr;
	return project;
}
