#include "Context.h"

#include <iostream>
#include <chrono>
#include "ShaderCache.h"
#include "Debug.h"
#include "GLFW/glfw3.h"
#include "gl/glew.h"
#include "Mouse.h"
#include "Director.h"
#include "World.h"
#include "TextureManager.h"
#include <nanogui/nanogui.h>
#include "DebugConsole.h"
#include "Time.h"
#include "Keyboard.h"
using namespace sip;
using namespace nanogui;


Context::RenderFunc Context::sm_displayFunc;
Context::UpdateFunc Context::sm_updateFunc;
Context::MouseFunc Context::sm_mouseFunc;
Context::MouseButtonFunc Context::sm_mouseButtonFunc;
Context::MouseScrollFunc Context::sm_mouseScrollFunc;
Context::KeyFunc Context::sm_keyFunc;


Context* Context::sm_instance = nullptr;


Context::Context(int w, int h) :
	m_w(w),
	m_h(h),
	m_window(nullptr)
{
	sm_instance = this;

	if (!glfwInit())
		return;

	glfwWindowHint(GLFW_SAMPLES, 8);
	glfwWindowHint(GLFW_DEPTH_BITS, 24);
	//m_window.reset(glfwCreateWindow(m_w, m_h, "Renderer", NULL, NULL));
	m_window = glfwCreateWindow(m_w, m_h, "Renderer", NULL, NULL);

	if (!m_window)
	{
		glfwTerminate();
		return;
	}

	glfwMakeContextCurrent(m_window);

	GLint GlewInitResult = glewInit();
	if (GLEW_OK != GlewInitResult)
	{
		printf("ERROR: %s\n", glewGetErrorString(GlewInitResult));
		exit(EXIT_FAILURE);
	}

	m_screen = new Screen();
	m_screen->initialize(getWindow(), true);

	glfwSetCharCallback(m_window,
		[](GLFWwindow *, unsigned int codepoint)
	{
		sm_instance->m_screen->charCallbackEvent(codepoint);
	}
	);

	glfwSetDropCallback(m_window,
		[](GLFWwindow *, int count, const char **filenames)
	{
		sm_instance->m_screen->dropCallbackEvent(count, filenames);
	}
	);

	glfwSetScrollCallback(m_window,
		[](GLFWwindow *, double x, double y) {
		sm_instance->m_screen->scrollCallbackEvent(x, y);
	}
	);

	glfwSetFramebufferSizeCallback(m_window,
		[](GLFWwindow *, int width, int height)
	{
		sm_instance->m_screen->resizeCallbackEvent(width, height);
	}
	);

	Mouse::bind(sm_instance);
	ShaderCache::Instance()->loadDefaultShaders();

}


Context::~Context()
{
}

void Context::internalUpdate()
{
	Time::tick();
	Keyboard::update();
	Director::Instance().update();
	sm_updateFunc();
	Director::Instance().getWorld()->render();
	sm_displayFunc();

	auto screen = Instance().getScreen();
	if (screen != nullptr)
	{
		screen->drawContents();
		screen->drawWidgets();
	}
	glfwSwapBuffers(Context::Instance().getWindow());
	Mouse::Update();
}



void Context::startMainLoop()
{

	TextureManager::Instance().loadDefaultTextures();

	while (!glfwWindowShouldClose(m_window))
	{
		glfwPollEvents();
		internalUpdate();
		glfwSwapBuffers(m_window);
	}

	glfwTerminate();
}


void Context::setMouseFunc(const MouseFunc& func)
{

	sm_mouseFunc = func;
	glfwSetCursorPosCallback(m_window,
		[](GLFWwindow *, double x, double y)
	{
		sm_instance->m_screen->cursorPosCallbackEvent(x, y);
		sm_mouseFunc((int)x, (int)y);
	}
	);
}



void Context::setRenderFunc(const RenderFunc& func)
{
	sm_displayFunc = func;
}

void Context::setUpdateFunc(const UpdateFunc& func)
{
	sm_updateFunc = func;
}


void Context::setMouseButtonFunc(const MouseButtonFunc& func)
{
	sm_mouseButtonFunc = func;

	glfwSetMouseButtonCallback(sm_instance->getWindow(),
		[](GLFWwindow *, int button, int action, int modifiers) {
		sm_instance->getScreen()->mouseButtonCallbackEvent(button, action, modifiers);
		sm_mouseButtonFunc(button, action, modifiers);
	}
	);
}

void Context::setKeyFunc(const KeyFunc& func)
{
	sm_keyFunc = func;
	glfwSetKeyCallback(sm_instance->getWindow(),
		[](GLFWwindow *, int key, int scancode, int action, int mods) 
	{
		sm_instance->getScreen()->keyCallbackEvent(key, scancode, action, mods);
		sm_keyFunc(key, scancode, action, mods);
	}
	);

}

void Context::setMouseScrollFunc(const MouseScrollFunc& func)
{
	sm_mouseScrollFunc = func;
	glfwSetScrollCallback(sm_instance->getWindow(), [](GLFWwindow*, double xOffset, double yOffset)
	{
		sm_mouseScrollFunc(xOffset, yOffset);
	});
}

nanogui::Screen* Context::getScreen()
{
	 return m_screen; 
}

GLFWwindow* Context::getWindow()
{
	return m_window;
}

glm::ivec2 Context::getScreenSize()
{
	return glm::vec2(m_w, m_h);
}

Context& Context::Instance()
{
	Debug::logFail(sm_instance != nullptr, "Render context is not initialized!");
	return *sm_instance;
}

