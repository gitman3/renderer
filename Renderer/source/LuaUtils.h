#pragma once

#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"

namespace sip
{

	class LuaUtils 
	{
	public:
		static const char* getGlobalString(const char* id, lua_State* state = 0);
		static float getGlobalFloat(const char* id, lua_State* state = 0);

		static float getRowDataNumber(int row, const char* tableName, const char* key, lua_State* state = 0);
		static const char* getRowDataString(int row, const char* tableName, const char* key, lua_State* state = 0);
		static bool isRow(int row, const char* tableName, lua_State* state = 0);
		static void stackdump(lua_State* l);

	private:
		static int initStackFor(int row, const char* tableName, const char* key, lua_State* state);

	};
}
	