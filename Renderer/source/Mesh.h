#pragma once

#include "GL/glew.h"

#include <vector>
#include <map>
#include <memory>

#include "glm/vec3.hpp"
#include "glm/mat4x4.hpp"
#include "Component.h"

struct aiScene;
struct aiNodeAnim;

namespace Assimp
{
	class Importer;
}

namespace sip
{

	class GLProgram;
	class MeshEntry;
	class Entity;
	class Animation;
	class Material;

//	typedef unsigned int ID;

	class Mesh 
	{
		friend class Camera;
		friend class Entity;

		//std::vector <Material*> m_materials; // owned by materialCache

		// these need to be pointers, because they get copied around in memory, thus deleted and so forth.	
		// or implement a move for all of these.
		std::vector <MeshEntry*> m_meshEntries;
		std::vector <std::shared_ptr<Animation>> m_animations;

		std::vector<glm::vec3> m_triangleList;

		const aiScene* m_aiScene;
		std::shared_ptr<Assimp::Importer> m_importer;

		void addAnimation(aiNodeAnim* animation);
		std::vector<glm::vec3>& getTriangleList() { return m_triangleList; }
		ID m_id;
		//Entity* m_entity;
		Mesh();

	public:

		std::vector<MeshEntry*>& getMeshEntries() { return m_meshEntries; }
		//void setShadersForAllMaterials(const char* path);

		Mesh(const std::string& filename);
		static Mesh* createPlane(const glm::vec3& pos);
		static Mesh* createCubeMapMesh();

		Material* getDefaultMaterial();

		virtual ~Mesh();
		//std::vector<Material*>& getMaterials() { return m_materials; }
		//void render(const glm::mat4& m, const glm::vec3& scale, const GLProgram * const prog = nullptr);
		//void renderDepth(const glm::mat4& m, const glm::vec3& scale);

		ID getId() { return m_id; }

		//void setEntity(Entity* entity);
		//Entity* getEntity();
	};



}