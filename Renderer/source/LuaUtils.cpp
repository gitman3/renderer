#include "LuaUtils.h"
#include "LuaTable.h"
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "Debug.h"

using namespace sip;

void LuaUtils::stackdump(lua_State* l)
{
    int i;
    int top = lua_gettop(l);
	Debug::log("---------------------");
	Debug::log("--total in stack %d--", top);
	Debug::log("---------------------");
 
    for (i = 1; i <= top; i++)
    {  /* repeat for each level */
        int t = lua_type(l, i);
        switch (t) {
            case LUA_TSTRING:  /* strings */
				Debug::log("string: '%s'", lua_tostring(l, i));
                break;
            case LUA_TBOOLEAN:  /* booleans */
				Debug::log("boolean %s", lua_toboolean(l, i) ? "true" : "false");
                break;
            case LUA_TNUMBER:  /* numbers */
				Debug::log("number: %g", lua_tonumber(l, i));
                break;
            default:  /* other values */
				Debug::log("%s", lua_typename(l, t));
                break;
        }
		Debug::log("  ");  /* put a separator */
    }
}


const char* LuaUtils::getGlobalString(const char* id, lua_State* state){
	
	LuaStackRestore lsr(state);
	lua_getglobal(state, id);
	
	if (!lua_isstring(state, -1)) {
		Debug::log("ERROR: getGlobalString() id = %s", id);
		return 0;
	}
	
	const char* value = lua_tostring(state, -1);
	Debug::log("getGlobalString() value = %s", value);
	return value;
}


bool LuaUtils::isRow(int row,  const char* tableName, lua_State* state) {

	LuaStackRestore lsr(state);
	lua_getglobal(state, tableName); // 1
	
	if (!lua_istable(state, -1)){
		Debug::log("Could not load table %s", tableName);
		return false;
	}


	lua_pushnumber(state, row); // 2
	lua_gettable(state, -2); // 2
	
	if(!lua_istable(state, -1)) {
		return false;
	}
	return true;
}

const char* LuaUtils::getRowDataString(int row, const char* tableName, const char* key, lua_State* state){

	LuaStackRestore lsr(state);
	int retVal = LuaUtils::initStackFor(row, tableName, key, state);
	if (retVal == -1) {return 0;}
	assert(lua_istable(state, -2));
	assert(lua_isstring(state, -1));
	const char* value = lua_tostring(state, -1);
	return value;
}

float LuaUtils::getRowDataNumber(int row, const char* tableName, const char* key, lua_State* state) {

	LuaStackRestore lsr(state);
	int retVal = LuaUtils::initStackFor(row, tableName, key, state);
	if (retVal == -1) {return 0;}
	assert(lua_istable(state, -2));
	assert(lua_isnumber(state, -1));
	float value = static_cast<float>(lua_tonumber(state, -1));
	return value;
}

float LuaUtils::getGlobalFloat(const char* id, lua_State* state) {

	LuaStackRestore lsr(state);
	lua_getglobal(state, id);
	
	if (!lua_isnumber(state, -1)) {
		Debug::log("ERROR: getGlobalFloat() id = %s", id);
		return 0;
	}
	
	float value = static_cast<float>(lua_tonumber(state, -1));
	Debug::log("getGlobalFloat() value = %f", value);
	return value;	
}

////////////////
////////////////
int LuaUtils::initStackFor(int row, const char* tableName, const char* key, lua_State* state){
	
	lua_getglobal(state, tableName); // 1
	
	if (!lua_istable(state, -1)){
		Debug::log("Could not load table %s ", tableName);
		return -2;
	}


	lua_pushnumber(state, row); // 2
	lua_gettable(state, -2); // 2

	//assert(lua_istable(state, -1));
	
	if(!lua_istable(state, -1)) {
		return -1;
	}

	// push key
	lua_pushstring(state, key); // 3
	lua_gettable(state, -2);    // 3
	return 0;
}