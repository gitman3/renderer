#include "Editor.h"
#include "Picker.h"
#include "Director.h"
#include "Context.h"
#include "DebugConsole.h"
#include "World.h"
#include "Debug.h"
#include "RayTracing.h"
#include "Transform.h"
#include "Camera.h"
#include "Mesh.h"
#include "MeshEntry.h"
#include "Entity.h"
#include "TextureManager.h"
#include "Material.h"
#include "ShaderCache.h"
#include "Command.h"
#include "Keyboard.h"
#include "EditorGUI.h"
#include "Time.h"
#include "LuaDataLoader.h"
#include "Storage.h"
#include "MaterialCache.h"
#include "Batcher.h"
#include "Mouse.h"
using namespace sip;
using namespace sip::editor;

namespace 
{
	bool shouldCreateNewCommand = true;

}


Editor::Editor() : m_commandQueue()
{
}

void Editor::init()
{
	createEditorMaterials();
}


void Editor::createEditorMaterials()
{
	MaterialKey key = MaterialCache::Instance().getCustomMaterialKey(MaterialCache::EDITOR_WIREFRAME_KEY);
	auto mat = std::make_shared<Material>(key);
	auto prog = ShaderCache::Instance()->getShader(ShaderCache::SNAME_EDITOR_WIREFRAME);
	assert(prog != nullptr);
	mat->setProgram(prog);
	MaterialCache::Instance().cacheMaterial(key, mat);
}

Handle<Entity> Editor::pickEntity(int screenX, int screenY)
{
	auto world = Director::Instance().getWorld();
	debug::logFail(world != nullptr, "world is null");

	std::vector<Handle<Transform>> objects;
	world->recursiveGetChildrenAll(objects);

	auto ray = world->getActiveCamera()->screenToWorldRay(glm::vec2(screenX, screenY));
	float minT = FLT_MAX;
	Handle<Entity> picked;

	for (auto transform : objects)
	{
		if (transform.isDeleted()) continue;
		auto tr = transformPtr(transform);
		if (tr->getEntity().isDeleted()) continue;
		auto ent = entityPtr(tr->getEntity());
		if (!ent->getInspectorSelectable()) continue;

		auto info = RayTracing::traceSphere(ray, ent->getTransformPtr()->getLocalPosition(), ent->getBoundingSphereRadius());
		if (info.collided() && info.t < minT)
		{
			picked = tr->getEntity();
			minT = info.t;
		}
	}
	return picked;
}

void Editor::translateLocal(Handle<Entity> entity, const glm::vec3& pos)
{
	m_commandQueue.add(std::make_unique<MoveCommand>(entity, pos));
	m_commandQueue.executeNextCommand();
}



Handle<Entity> Editor::createPlane(glm::vec3 position)
{
	auto hEntity = Director::Instance().getWorld()->addEntity();
	auto entity = entityPtr(hEntity);
	entity->setMesh(Mesh::createPlane(position));
	return hEntity;
}


void Editor::update()
{
	m_commandQueue.update();
	float dt = Time::deltaTime();

	if (Keyboard::isKeyHold(Keyboard::CONTROL) && Keyboard::isKeyDown('Z'))
	{
		m_commandQueue.undoCurrentCommand();
	}
}

void sip::editor::Editor::rotateLocal(Handle<Entity> ent, const glm::fquat& rot)
{
	m_commandQueue.add(std::make_unique <RotateCommand> (ent, rot));
	m_commandQueue.executeNextCommand();
}

void sip::editor::Editor::scaleLocal(Handle<Entity> ent, const glm::vec3& amount)
{
	m_commandQueue.add(std::make_unique <ScaleCommand>(ent, amount));
	m_commandQueue.executeNextCommand();
}

void sip::editor::Editor::deleteEntity(Handle<Entity> entity)
{
	m_commandQueue.add(std::make_unique<DeleteEntityCommand>(entity));
	m_commandQueue.executeNextCommand();
}



Handle<Entity> Editor::entityFromPrefab(const std::string& path)
{
	m_commandQueue.add(std::make_unique<CreateEntityCommand>(path));
	m_commandQueue.executeNextCommand();
	return dynamic_cast<CreateEntityCommand*>(m_commandQueue.getLastCommand())->getCreatedEntity();

}

///////////////////////////////
///////// Command queue
//////////////////////////////
Editor::CommandQueue::CommandQueue() : 
	m_queue()
//, m_commandIndex(0)
{
}

void Editor::CommandQueue::add(std::unique_ptr<Command> command)
{

	if (m_queue.empty() || shouldCreateNewCommand)
	{
		m_queue.emplace_back(std::move(command));
		EditorGUI::log("new command added");
		shouldCreateNewCommand = false;
		return;
	}

	if (!m_queue.back()->append(command.get()))
	{
		m_queue.emplace_back(std::move(command));
		EditorGUI::log("new command added");
	}
}

void Editor::CommandQueue::executeNextCommand()
{
	if (m_queue.size() > 0)
	{
		m_queue.back()->execute();
	}
}

void Editor::CommandQueue::undoCurrentCommand()
{
	if (m_queue.size() > 0)
	{
		m_queue.back()->executeUndo();
		m_queue.erase(m_queue.end() -1);
	}
}


void sip::editor::Editor::CommandQueue::update()
{

	if(!Mouse::isButtonHold(Mouse::Middle) && !Mouse::isButtonHold(Mouse::Left) && !Mouse::isButtonHold(Mouse::Right) && !Keyboard::isAnyKeyHoldOrDown())
	{
		shouldCreateNewCommand = true;
	}
}

sip::editor::Command* sip::editor::Editor::CommandQueue::getLastCommand()
{
	return m_queue.back().get();
}
