#pragma once

namespace sip
{
	template<typename T>
	class PassKey
	{
		friend T;
	};
}
