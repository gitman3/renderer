#pragma once
#include <string>

namespace nanogui
{
	class Screen;
}

namespace sip
{
	namespace editor
	{
		class DebugConsole
		{
		public:
			enum Level
			{
				NORMAL,
				WARNING,
				FAIL
			};

			DebugConsole();
			void log(const std::string& message, Level level = NORMAL);


			void setVisible(bool visible);
			bool visible();
			void toggleVisible();

		private:
			
			friend class EditorGUI;

			std::string m_pendingText;

			struct Message
			{
				std::string message;
				Level level;

				Message(const std::string& m, Level l) :
					level(l),
					message(m)
				{}

			};
			std::vector<Message> m_messages;
		};
	}
}
