#pragma once
#include "glm/glm.hpp"

namespace sip
{

	namespace RayTracing
	{
		struct Ray
		{
			glm::vec3 origin;
			glm::vec3 direction;

			Ray() : origin(), direction()
			{}
		};

		struct RayCollisionInfo
		{
			Ray ray;
			float t;
			float t2;
			glm::vec3 pos;
			glm::vec3 pos2;

			RayCollisionInfo() :
				ray(),
				t(-1.0f),
				t2(-1.0f),
				pos(0.0f),
				pos2(0.0f)
			{}
			bool collided() { return t >= 0.0f; }

		};

		Ray getRay(int screenX, int screenY);
		RayCollisionInfo traceSphere(const Ray& ray, const glm::vec3& spherePos, float radius);
		RayCollisionInfo tracePlane(const Ray& ray, const glm::vec3& planePos, const glm::vec3 planeNormal);
	}
}