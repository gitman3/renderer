
#include "ShaderParser.h"
#include "Util.h"
#include "EditorGUI.h"
#include "Debug.h"
#include "ShaderCache.h"

using namespace sip;

namespace
{

	std::string parseValues(const std::string& keyAndValue)
	{
		auto kpValues = Util::splitString(keyAndValue, ':');
		return Util::trim(kpValues[1]);
	}
	std::vector<std::string> parseValueVector(const std::string& keyAndValues)
	{
		auto valuesStr = parseValues(keyAndValues);
		auto values = Util::splitString(valuesStr, ' ');
		for (auto& value : values)
		{
			value = Util::trim(value);
		}
		return values;
	}


	GLint parseFaceFill(const std::string& str)
	{
		if (str == "GL_FRONT_AND_BACK")
		{
			return GL_FRONT_AND_BACK;
		}
		else if (str == "GL_FRONT")
		{
			return GL_FRONT;
		}
		else if (str == "GL_BACK")
		{
			return GL_BACK;
		}
		assert(false);
		return GL_FRONT;
	}

	GLint parseFillMode(const std::string& str)
	{
		if (str == "GL_FILL")
		{
			return GL_FILL;
		}
		else if (str == "GL_LINE")
		{
			return GL_LINE;
		}
		assert(false);
		return GL_FILL;
	}

	GLint parseBlend(const std::string& str)
	{
		if (str == "GL_ONE")
		{
			return GL_ONE;
		}
		else if (str == "GL_ONE_MINUS_SRC_ALPHA")
		{
			return GL_ONE_MINUS_SRC_ALPHA;
		}
		else if (str == "GL_SRC_ALPHA")
		{
			return GL_SRC_ALPHA;
		}
		debug::logFail(false, "could not parse blend value", str.c_str());
		return 0;
	}
}

ShaderParserOutput ShaderParser::fromCustomSrc(const std::string& filename)
{

	auto src = Util::fileAsString(filename);

	// replace includes at source code
	replaceIncludes(src);

	auto lines = Util::splitString(src, '\n');

	std::vector<std::string> glFlagsLines;
	// moves the lines with gl flags from lines to glFlagsLines
	std::copy_if(std::make_move_iterator(lines.begin()), std::make_move_iterator(lines.end()), std::back_inserter(glFlagsLines), [=] (std::string& line) {
		auto lineTrimmed = Util::trim(line);
		return lineTrimmed.length() > 0 && *lineTrimmed.begin() == '@' ? true : false;
	});

	ShaderParserOutput output;

	for (auto line : glFlagsLines)
	{
		if (Util::stringContains(line, "glEnable"))
		{
			if (parseValues(line) == "GL_BLEND")
			{
				output.shader.Flags |= ShaderFlags::BLEND_ENABLED;
			}
			else if (parseValues(line) == "GL_DEPTH_TEST")
			{
				output.shader.Flags |= ShaderFlags::DEPTH_TEST_ENABLED;
			}
		}
		else if (Util::stringContains(line, "glDisable"))
		{
			if (parseValues(line) == "GL_CULL_FACE")
			{
				output.shader.Flags |= ShaderFlags::CULLING_DISABLED;
			}
		}

		else if (Util::stringContains(line, "glDepthMask"))
		{
			if (parseValues(line) == "GL_TRUE")
			{
				output.shader.Flags |= ShaderFlags::DEPTH_WRITE_ENABLED;
			}
		}
		else if (Util::stringContains(line, "glBlendFunc"))
		{
			const auto& values = parseValueVector(line);
			output.shader.BlendSrc = parseBlend(values[0]);
			output.shader.BlendDst = parseBlend(values[1]);
		}
		else if (Util::stringContains(line, "glPolygonMode"))
		{
			const auto& values = parseValueVector(line);
			output.shader.PolygonDrawFaces = parseFaceFill(values[0]);
			output.shader.PolygonDrawMode = parseFillMode(values[1]);
		}
	}

	// parse [Vertex] and [Fragment] flags
	int vertexShaderRow = -1;
	int fragmentShaderRow = -1;
	for (unsigned int i = 0; i < lines.size(); ++i)
	{
		if (lines[i].find("[Vertex]") != std::string::npos) vertexShaderRow = i;
		if (lines[i].find("[Fragment]") != std::string::npos) fragmentShaderRow = i;
	}

	debug::logFail(vertexShaderRow >= 0 && fragmentShaderRow >= 0, "Could not find fragment and vertex shader tags");
	
	std::stringstream vertexShaderStream;
	for (int i = vertexShaderRow + 1; i < fragmentShaderRow; ++i)
	{
		vertexShaderStream << lines[i] << "\n";
	}

	std::stringstream fragmentShaderStream;
	for (unsigned int i = fragmentShaderRow + 1; i < lines.size(); ++i)
	{
		fragmentShaderStream << lines[i] << "\n";
	}

	output.fragmentShaderSrc = fragmentShaderStream.str();
	output.vertexShaderSrc = vertexShaderStream.str();

	return output;
}


ShaderParserOutput ShaderParser::parseShader(const std::string& path)
{
	if (Util::fileExists(path + ".shader"))
	{
		return ShaderParser::fromCustomSrc(path + ".shader");
	}
	debug::logFail(false, "No such File : ", path);
	return ShaderParserOutput();
}

void ShaderParser::replaceIncludes(std::string& src)
{
	int offset = 0;
	auto cursor = src.begin();
	std::string searchSymbols = "\"";

	while ( (offset = src.find("#include", offset)) != std::string::npos)
	{
		cursor = src.begin() + offset + 7;
		auto fileNameStart = std::find_first_of(cursor, src.end(), searchSymbols.begin(), searchSymbols.end()) + 1;
		auto fileNameEnd = std::find_first_of(fileNameStart + 1, src.end(), searchSymbols.begin(), searchSymbols.end());
		std::string fileName(fileNameStart, fileNameEnd);

		std::string content = Util::fileAsString(ShaderCache::SHADERS_PATH + fileName);

		src.replace(src.begin() + offset, fileNameEnd + 1, content);
		++offset;
	}
}
