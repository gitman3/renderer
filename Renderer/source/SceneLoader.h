#pragma once
#include <map>
#include "LuaTable.h"
#include "Scene.h"

namespace sip
{

	struct Project
	{
		std::string pathToAssetsFolder;
		std::map<std::string, std::string> prefabPaths;
		std::map<std::string, std::string> scenePaths;
		std::map<std::string, std::string> materialPaths;
		std::map<std::string, std::string> cubemapPaths;
		std::map<std::string, std::string> sceneMetaFiles;
	};


	class SceneLoader
	{

		SceneLoader();
		Scene m_loadedScene;
		Project m_loadedProject;

	public:
		
		void loadScene(const std::string& pathToFile);
		void prepareProject(Project& project);
		Project loadProjectFile(const std::string& pathToProject);

		Scene& getLoadedScene() { return m_loadedScene; }
		Project& getLoadedProject() { return m_loadedProject; }

		static SceneLoader& Instance()
		{
			static SceneLoader Instance;
			return Instance;
		}

	};
}
