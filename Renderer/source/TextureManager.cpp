#include "TextureManager.h"
#include "Texture.h"
#include "3drParty\tgaio.h"
#include "Util.h"
#include "Debug.h"
#include "GLStateRestore.h"
#include "glew.h"

using namespace sip;

TextureManager::TextureManager() : m_noiseTexture(nullptr)
{
}

void TextureManager::loadDefaultTextures()
{
	m_noiseTexture = addTextureIfMissing("../Renderer/resources/noise.png");
	Util::checkGLError("Loading noise texture failed ");
}




Texture* TextureManager::addTextureIfMissing(const std::string& pathToTexture)
{
	Debug::loghline();
	Debug::log("[TextureManager] Adding texture [", pathToTexture, "]");

	if (m_textureCache.find(pathToTexture) != m_textureCache.end())
	{
		Debug::log("[TextureManager] Already exists in cache. ");
		Debug::loghline();
		return &m_textureCache.find(pathToTexture)->second;
	}
	
	std::ifstream ifs(pathToTexture);
	if (!ifs.good())
	{
		
		Debug::log("[TextureManager] ", pathToTexture, " does not exist");
		Debug::loghline();
		return nullptr;
	}
	try
	{
		GLuint id = TGAIO::loadTex(pathToTexture.c_str());
		Debug::log("[TextureManager] ", pathToTexture, " loaded with id ", id);

		m_textureCache.insert(std::pair<std::string, Texture>(pathToTexture, Texture(id)));
		Debug::log("[TextureManager] DONE");
		Debug::loghline();
		return &m_textureCache.find(pathToTexture)->second;


	} catch (std::exception& e)
	{
		Debug::log("[TextureManager] failed to load texture : ", e.what());
		return nullptr;
	}
}

RenderTarget TextureManager::getRenderTarget(int w, int h, unsigned int format)
{

	GLStateRestore sr(GLStateRestore::FBO | GLStateRestore::RBO | GLStateRestore::TEX);

	RenderTarget rt;

	GLuint fbohandle;
	GLuint texhandle;
	GLuint rbohandle;

	glGenFramebuffers(1, &fbohandle);
	glBindFramebuffer(GL_FRAMEBUFFER, fbohandle);

	glGenTextures(1, &texhandle);
	glBindTexture(GL_TEXTURE_2D, texhandle);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

	//Define texture properties for bound texture
	glBindTexture(GL_TEXTURE_2D, texhandle);
	glTexImage2D(GL_TEXTURE_2D, 0, format, w, h, 0, format, GL_UNSIGNED_BYTE, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texhandle, 0);

	// Attach texture to framebuffer color attachment slot
	//	glDrawBuffer(GL_NONE);
	//	glReadBuffer(GL_NONE);


	// Create depth buffer (optional)
	glGenRenderbuffers(1, &rbohandle);
	glBindRenderbuffer(GL_RENDERBUFFER, rbohandle);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, w, h);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rbohandle);


	glDrawBuffer(GL_COLOR_ATTACHMENT0);// is needed?
									   //
	rt.m_fboHandle = fbohandle;
	rt.m_texHandle = texhandle;
	rt.m_rboHandle = rbohandle;

	rt.m_hasRBOandle = true;
	rt.m_hasTexHandle = true;


	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

	if (status != GL_FRAMEBUFFER_COMPLETE)
	{
		Debug::logFail(false, "[TextureManager] failed to create render target");
	}
	else
	{
		Debug::log("[TextureManager] created render target");
	}

	return rt;
}


RenderTarget TextureManager::getDepthRenderTarget(int w, int h)
{

	GLStateRestore sr(GLStateRestore::FBO | GLStateRestore::RBO | GLStateRestore::TEX);

	RenderTarget rt;

	GLuint fbohandle;
	GLuint texhandle;

	glGenFramebuffers(1, &fbohandle);
	glBindFramebuffer(GL_FRAMEBUFFER, fbohandle);

	glGenTextures(1, &texhandle);
	glBindTexture(GL_TEXTURE_2D, texhandle);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	// Remove artefact on the edges of the shadowmap
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

	//Define texture properties for bound texture
	glBindTexture(GL_TEXTURE_2D, texhandle);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, w, h, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
	// Attach texture to framebuffer color attachment slot
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, texhandle, 0);

	/*
	// Create depth buffer (optional)
	glGenRenderbuffers(1, &rbohandle);
	glBindRenderbuffer(GL_RENDERBUFFER, rbohandle);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, w, h);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rbohandle);

	glDrawBuffer(GL_COLOR_ATTACHMENT0);// is needed?
	*/
	//
	rt.m_fboHandle = fbohandle;
	rt.m_texHandle = texhandle;
	rt.m_rboHandle = 0;
	rt.m_hasRBOandle = false;
	rt.m_hasTexHandle = true;
	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

	if (status != GL_FRAMEBUFFER_COMPLETE)
	{
		Debug::logFail(false, "[TextureManager] failed to create render target");
	}
	else
	{
		Debug::log("[TextureManager] created render target");
	}

	return rt;
}

sip::CubeMap* sip::TextureManager::addCubeMapIfMissing(std::string name, std::shared_ptr<CubeMap> cubeMap)
{
	if (m_cubeMapCache.find(name) == m_cubeMapCache.end())
	{
		m_cubeMapCache[name] = cubeMap;
	}
	return m_cubeMapCache[name].get();
}

sip::CubeMap* sip::TextureManager::getCubeMap(const std::string& name)
{
	assert(m_cubeMapCache.find(name) != m_cubeMapCache.end());
	return m_cubeMapCache.at(name).get();
}
