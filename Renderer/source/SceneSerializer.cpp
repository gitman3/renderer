#include <sstream>

#include "SceneSerializer.h"
#include "World.h"
#include "Transform.h"
#include "Entity.h"
#include "Storage.h"
#include "Director.h"
#include "Scene.h"
#include "Util.h"
#include "EditorGUI.h"

using namespace sip;

namespace
{
	void addEntityTable(std::stringstream& ss, int index, const char* name)
	{
		ss << "scene.instances[" << index << "]." << name << " = {}\n";
	}

	void addEntityProperty(std::stringstream& ss, int index, const char* name, std::string& value)
	{
		Util::fixSeparators(value);
		ss << "scene.instances[" << index << "]." << name << " = \"" << value << "\"\n";
	}

	void addEntityPropertyVec (std::stringstream& ss, int index, const char* name, glm::vec3 vec)
	{
		std::stringstream pos;
		pos << "{ x = "<< vec.x <<", y = " << vec.y << " , z = " << vec.z <<" }";
		ss << "scene.instances[" << index << "]." << name << " = " << pos.str() << "\n";
	}

	void addEntityPropertyVec(std::stringstream& ss, int index, const char* name, glm::vec4 vec)
	{
		std::stringstream pos;
		pos << "{ x = " << vec.x << ", y = " << vec.y << " , z = " << vec.z << ", w = "<< vec.q << " }";
		ss << "scene.instances[" << index << "]." << name << " = " << pos.str() << "\n";
	}

	void addEntityPropertyQuat(std::stringstream& ss, int index, const char* name, glm::fquat quat)
	{
		std::stringstream pos;
		addEntityPropertyVec(ss, index, name,  glm::vec4(quat.x, quat.y, quat.z, quat.w));
	}

}

SceneSerializer::SceneSerializer()
{

}


void SceneSerializer::serialize(Scene& scene)
{
	auto world = Director::Instance().getWorld();
	std::vector<Handle<Transform>> transforms;
	world->recursiveGetChildrenAll(transforms);

	std::stringstream header_ss;
	header_ss << " -- Auto generated scene file -- \n";
	header_ss << "scene.projectFilePath = \"" << scene.getProjectFilePath() << "\"\n";
	header_ss << "scene.instances = {}\n";


	std::stringstream entities_ss;
	int count = 0;
	for (auto hTransform : transforms)
	{
		if (hTransform.isDeleted()) continue;
		
		auto pTransform = transformPtr(hTransform);
		auto hEntity = pTransform->getEntity();
		if (hEntity.isDeleted()) continue;
		auto pEntity = entityPtr(hEntity);
		
		// TODO: should be able to serialize stuff without prefabs (custom meshes etc)
		if (pEntity->getPrefab() == nullptr) continue;

		++count;

		header_ss << "table.insert(scene.instances, {})\n";
		addEntityProperty(header_ss, count, "prefab", pEntity->getPrefab()->fileName);
		addEntityProperty(header_ss, count, "name", pEntity->getName());
		addEntityTable(header_ss, count, "transform");

		addEntityPropertyVec(header_ss, count, "transform.position", pTransform->getLocalPosition());
		addEntityPropertyQuat(header_ss, count, "transform.rotation", pTransform->getRotation());
		addEntityPropertyVec(header_ss, count, "transform.scale", glm::vec3(pTransform->getScale()));

	}

	std::stringstream res;
	res << header_ss.str();
	res << entities_ss.str();

	Util::writeToFile(scene.getPath(), res.str());

	editor::EditorGUI::log("Scene saved : ", scene.getPath());
}
