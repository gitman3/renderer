#include "MeshEntry.h"
#include "assimp/mesh.h"
#include "Debug.h"
#include "Util.h"
#include "Material.h"
#include "IDGenerator.h"
using namespace sip;

MeshEntry::MeshEntry(aiMesh *mesh) :
	m_boundingSphereRadius(0),
	m_origin(),
	m_aiMesh(mesh),
	m_id(getNewID<MeshEntry>()),
	m_material(nullptr),
	m_vertexColors()
{
	vbo[VERTEX_BUFFER] = NULL;
	vbo[TEXCOORD_BUFFER] = NULL;
	vbo[NORMAL_BUFFER] = NULL;
	vbo[INDEX_BUFFER] = NULL;

	// animation
	vbo[VERTEX_BONE_INDICES_BUFFER] = NULL;
	vbo[VERTEX_WEIGHTS_BUFFER] = NULL;

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);


	// what kind of mesh does not have positions??
	assert(m_aiMesh->HasPositions());

	// vertex colors buffer
	glGenBuffers(1, &vbo[VERTEX_COLORS_BUFFER]);
	m_vertexColors = std::vector<float>(m_aiMesh->mNumVertices * 4, 1.0f);
	applyVertexColors();

	// used at rendering
	m_elementCount = mesh->mNumFaces * 3;

	float xmin = FLT_MAX;
	float xmax = FLT_MIN;
	float ymin = FLT_MAX;
	float ymax = FLT_MIN;
	float zmin = FLT_MAX;
	float zmax = FLT_MIN;

	if (mesh->HasPositions()) 
	
	{
		//float *vertices = new float[mesh->mNumVertices * 3];
		for (unsigned int i = 0; i < mesh->mNumVertices; ++i)
		{
			/*
			vertices[i * 3] = mesh->mVertices[i].x;
			vertices[i * 3 + 1] = mesh->mVertices[i].y;
			vertices[i * 3 + 2] = mesh->mVertices[i].z;
			*/
			xmin = std::min(xmin, mesh->mVertices[i].x);
			xmax = std::max(xmax, mesh->mVertices[i].x);

			ymin = std::min(ymin, mesh->mVertices[i].y);
			ymax = std::max(ymax, mesh->mVertices[i].y);

			zmin = std::min(zmin, mesh->mVertices[i].z);
			zmax = std::max(zmax, mesh->mVertices[i].z);
		}

		float xc = xmin + (xmax - xmin) * 0.5f;
		float yc = ymin + (ymax - ymin) * 0.5f;
		float zc = zmin + (zmax - zmin) * 0.5f;

		m_origin = glm::vec3(xc, yc, zc);
		m_boundingSphereRadius = std::max(m_boundingSphereRadius, (xmax - xmin) * 0.5f);
		m_boundingSphereRadius = std::max(m_boundingSphereRadius, (ymax - ymin) * 0.5f);
		m_boundingSphereRadius = std::max(m_boundingSphereRadius, (zmax - zmin) * 0.5f);

		glGenBuffers(1, &vbo[VERTEX_BUFFER]);
		glBindBuffer(GL_ARRAY_BUFFER, vbo[VERTEX_BUFFER]);
		//glBufferData(GL_ARRAY_BUFFER, 3 * mesh->mNumVertices * sizeof(GLfloat), &vertices[0], GL_STATIC_DRAW);
		glBufferData(GL_ARRAY_BUFFER, 3 * mesh->mNumVertices * sizeof(GLfloat), &reinterpret_cast<float*>(mesh->mVertices)[0], GL_STATIC_DRAW);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(0);

		//delete[] vertices;
	}


	if (mesh->HasTextureCoords(0))
	{
		
		float *texCoords = new float[mesh->mNumVertices * 2];
		for (unsigned int i = 0; i < mesh->mNumVertices; ++i)
		{
			texCoords[i * 2] = mesh->mTextureCoords[0][i].x;
			texCoords[i * 2 + 1] = mesh->mTextureCoords[0][i].y;
		}
		
		glGenBuffers(1, &vbo[TEXCOORD_BUFFER]);
		glBindBuffer(GL_ARRAY_BUFFER, vbo[TEXCOORD_BUFFER]);
		glBufferData(GL_ARRAY_BUFFER, 2 * mesh->mNumVertices * sizeof(GLfloat), &texCoords[0], GL_STATIC_DRAW);

		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(1);

		delete[] texCoords;
	}



	if (mesh->HasNormals())
	{
		/*
		float *normals = new float[mesh->mNumVertices * 3];
		for (unsigned int i = 0; i < mesh->mNumVertices; ++i)
		{
			normals[i * 3] = mesh->mNormals[i].x;
			normals[i * 3 + 1] = mesh->mNormals[i].y;
			normals[i * 3 + 2] = mesh->mNormals[i].z;
		}
		*/
		glGenBuffers(1, &vbo[NORMAL_BUFFER]);
		glBindBuffer(GL_ARRAY_BUFFER, vbo[NORMAL_BUFFER]);
		glBufferData(GL_ARRAY_BUFFER, 3 * mesh->mNumVertices * sizeof(GLfloat), &reinterpret_cast<float*>(mesh->mNormals)[0], GL_STATIC_DRAW);

		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(2);

		//delete[] normals;
	}


	if (mesh->HasFaces())
	{
		
		unsigned int *indices = new unsigned int[mesh->mNumFaces * 3];
		for (unsigned int i = 0; i < mesh->mNumFaces; ++i)
		{
			indices[i * 3] = mesh->mFaces[i].mIndices[0];
			indices[i * 3 + 1] = mesh->mFaces[i].mIndices[1];
			indices[i * 3 + 2] = mesh->mFaces[i].mIndices[2];
		}
		
		glGenBuffers(1, &vbo[INDEX_BUFFER]);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[INDEX_BUFFER]);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3 * mesh->mNumFaces * sizeof(GLuint), indices, GL_STATIC_DRAW);

		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(3);

		delete[] indices;
	}


	if (mesh->HasTangentsAndBitangents())
	{
		//float *binormals = new float[mesh->mNumVertices * 3];
		//float *tangents = new float[mesh->mNumVertices * 3];
		/*
		for (unsigned int i = 0; i < mesh->mNumVertices; ++i)
		{

			auto bt = mesh->mBitangents[i].Normalize();
			auto tn = mesh->mTangents[i].Normalize();

			binormals[i * 3] = bt.x;
			binormals[i * 3 + 1] = bt.y;
			binormals[i * 3 + 2] = bt.z;

			tangents[i * 3] = tn.x;
			tangents[i * 3 + 1] = tn.y;
			tangents[i * 3 + 2] = tn.z;
		}
		*/

		glGenBuffers(1, &vbo[BINORMAL_BUFFER]);
		glBindBuffer(GL_ARRAY_BUFFER, vbo[BINORMAL_BUFFER]);
		glBufferData(GL_ARRAY_BUFFER, 3 * mesh->mNumVertices * sizeof(GLfloat), &reinterpret_cast<float*>(mesh->mBitangents)[0], GL_STATIC_DRAW);
		glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(4);
		//delete[] binormals;

		glGenBuffers(1, &vbo[TANGENT_BUFFER]);
		glBindBuffer(GL_ARRAY_BUFFER, vbo[TANGENT_BUFFER]);
		glBufferData(GL_ARRAY_BUFFER, 3 * mesh->mNumVertices * sizeof(GLfloat), &reinterpret_cast<float*>(mesh->mTangents)[0], GL_STATIC_DRAW);
		glVertexAttribPointer(5, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(5);
		//delete[] tangents;
	}

	int err = glGetError();
	assert(err == 0);

	if (mesh->HasBones())
	{


		const int MAX_BONES_PER_VERTEX = 4;
		std::vector<GLfloat> vertexWeights(mesh->mNumVertices * MAX_BONES_PER_VERTEX, 0.0f);
		std::vector<GLint> boneIds(mesh->mNumVertices * MAX_BONES_PER_VERTEX, 0);

		for (unsigned int i = 0; i < mesh->mNumBones; ++i)
		{
			auto bone = mesh->mBones[i];
			auto name = bone->mName;
			debug::log("Processing Bone : ", name.C_Str());

			auto weightCount = bone->mNumWeights;
			float weightTotal = 0.0f;
			int count = 0;
			for (unsigned int j = 0; j < weightCount; ++j) // weightCount = a big number
			{

				auto weight = bone->mWeights[j];

				weightTotal += weight.mWeight;
				int vertex = bone->mWeights[j].mVertexId;

				assert(weight.mWeight <= 1.00f);

				if (weight.mWeight > 0.f)
				{
					++count;
					for (int k = 0; k < MAX_BONES_PER_VERTEX; ++k)
					{
						if (vertexWeights[vertex * MAX_BONES_PER_VERTEX + k] == 0.0f)
						{
							vertexWeights[vertex * MAX_BONES_PER_VERTEX + k] = weight.mWeight;
							boneIds[vertex * MAX_BONES_PER_VERTEX + k] = weight.mVertexId;
							break;
						}
					}
				}

			}
			/*
			if (weightTotal <= 0.98 && weightTotal >= 1.1f)
			{
			debug::log(weightTotal);
			assert(weightTotal >= 0.98f && weightTotal <= 1.1f);
			}
			*/
		}

		for (unsigned int i = 0; i < vertexWeights.size(); i = i + 4)
		{
			float weight = vertexWeights[i] + vertexWeights[i + 1] + vertexWeights[i + 2] + vertexWeights[i + 3];
			if (weight < 0.99f || weight > 1.001f)
			{
				debug::log("weight is foobar : ", weight);
			}
			//	assert(weight >= 0.99f && weight <= 1.01f);
		}


		glGenBuffers(1, &vbo[VERTEX_BONE_INDICES_BUFFER]);
		glBindBuffer(GL_ARRAY_BUFFER, vbo[VERTEX_BONE_INDICES_BUFFER]);
		glBufferData(GL_ARRAY_BUFFER, boneIds.size() * sizeof(GLint), &boneIds[0], GL_STATIC_DRAW);
		glVertexAttribIPointer(6, 4, GL_INT, 0, 0);
		glEnableVertexAttribArray(6);

		glGenBuffers(1, &vbo[VERTEX_WEIGHTS_BUFFER]);
		glBindBuffer(GL_ARRAY_BUFFER, vbo[VERTEX_WEIGHTS_BUFFER]);
		glBufferData(GL_ARRAY_BUFFER, vertexWeights.size() * sizeof(GLfloat), &vertexWeights[0], GL_STATIC_DRAW);
		glVertexAttribPointer(7, 4, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(7);


		{
			int err = glGetError();
			assert(err == 0);
		}


	}

	buildTriangleList();

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	err = glGetError();
	assert(err == 0);
}

// raw mesh entry with no data
MeshEntry::MeshEntry() :
	m_boundingSphereRadius(0),
	m_origin(),
	m_aiMesh(nullptr),
	m_id(getNewID<MeshEntry>())

{
}

MeshEntry::~MeshEntry() 
{
	if (vbo[VERTEX_BUFFER])
	{
		glDeleteBuffers(1, &vbo[VERTEX_BUFFER]);
	}

	if (vbo[TEXCOORD_BUFFER]) 
	{
		glDeleteBuffers(1, &vbo[TEXCOORD_BUFFER]);
	}

	if (vbo[NORMAL_BUFFER]) 
	{
		glDeleteBuffers(1, &vbo[NORMAL_BUFFER]);
	}

	if (vbo[INDEX_BUFFER]) 
	{
		glDeleteBuffers(1, &vbo[INDEX_BUFFER]);
	}

	if (vbo[TANGENT_BUFFER]) 
	{
		glDeleteBuffers(1, &vbo[TANGENT_BUFFER]);
	}

	if (vbo[BINORMAL_BUFFER]) 
	{
		glDeleteBuffers(1, &vbo[BINORMAL_BUFFER]);
	}

	if (vbo[VERTEX_WEIGHTS_BUFFER])
	{
		glDeleteBuffers(1, &vbo[VERTEX_WEIGHTS_BUFFER]);
	}

	if (vbo[VERTEX_BONE_INDICES_BUFFER])
	{
		glDeleteBuffers(1, &vbo[VERTEX_BONE_INDICES_BUFFER]);
	}

	if (vbo[VERTEX_COLORS_BUFFER])
	{
		glDeleteBuffers(1, &vbo[VERTEX_COLORS_BUFFER]);
	}

	glDeleteVertexArrays(1, &vao);
}

void MeshEntry::buildTriangleList()
{
	m_triangleList.clear();
	if (m_aiMesh)
	{
		for (unsigned int i = 0; i < m_aiMesh->mNumFaces; ++i)
		{
			for (unsigned int ind = 0; ind < m_aiMesh->mFaces[i].mNumIndices; ++ind)
			{
				auto vert = m_aiMesh->mVertices[m_aiMesh->mFaces[i].mIndices[ind]];
				m_triangleList.push_back(glm::vec3(vert.x, vert.y, vert.z));
				m_indiceList.push_back(m_aiMesh->mFaces[i].mIndices[ind]);
			}
		}
	}
}

/**
*	Renders this MeshEntry
**/
void MeshEntry::render() 
{
	glBindVertexArray(vao);
	glDrawElements(GL_TRIANGLES, m_elementCount, GL_UNSIGNED_INT, NULL);
	glBindVertexArray(0);
}


sip::MeshEntry* sip::MeshEntry::createCube()
{
	MeshEntry* entry = new MeshEntry();
	Util::checkGLError("creating cube mesh entry");

	entry->m_elementCount = 2 * 3 * 6;
	entry->vbo[VERTEX_BUFFER] = NULL;
	entry->vbo[TEXCOORD_BUFFER] = NULL;
	entry->vbo[NORMAL_BUFFER] = NULL;
	entry->vbo[INDEX_BUFFER] = NULL;

	// animation
	entry->vbo[VERTEX_BONE_INDICES_BUFFER] = NULL;
	entry->vbo[VERTEX_WEIGHTS_BUFFER] = NULL;

	glGenVertexArrays(1, &entry->vao);
	glBindVertexArray(entry->vao);

	int indx = 0;
	std::vector<float> vertices =
	{    // x, y, z, x, y, z, x, y, z, x, y, z
		1.0f, -1.0f, -1.0f,  1.0f, -1.0f,  1.0f,  1.0f,  1.0f,  1.0f,  1.0f,  1.0f, -1.0f, // +X
		-1.0f, -1.0f,  1.0f, -1.0f, -1.0f, -1.0f, -1.0f,  1.0f, -1.0f, -1.0f,  1.0f,  1.0f, // -X
		-1.0f,  1.0f, -1.0f,  1.0f,  1.0f, -1.0f,  1.0f,  1.0f,  1.0f, -1.0f,  1.0f,  1.0f, // +Y
		-1.0f, -1.0f,  1.0f,  1.0f, -1.0f,  1.0f,  1.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f, // -Y
		1.0f, -1.0f,  1.0f, -1.0f, -1.0f,  1.0f, -1.0f,  1.0f,  1.0f,  1.0f,  1.0f,  1.0f, // +Z
		-1.0f, -1.0f, -1.0f,  1.0f, -1.0f, -1.0f,  1.0f,  1.0f, -1.0f, -1.0f,  1.0f, -1.0f  // -Z
	};

	const float span = 1.0f;
	glm::vec3 pos = glm::vec3(.0f);

	glGenBuffers(1, &entry->vbo[VERTEX_BUFFER]);
	glBindBuffer(GL_ARRAY_BUFFER, entry->vbo[VERTEX_BUFFER]);
	glBufferData(GL_ARRAY_BUFFER, 12 * 6 * sizeof(GLfloat), &vertices[0], GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);

	////////////////////// indices ////////////
	
	std::vector<unsigned int> indices;
	std::vector<unsigned int> ind = { 0, 2, 1, 0, 3, 2 };
	//std::vector<unsigned int> ind = { 2, 1, 0, 3, 2, 0 };

	for (int i = 0; i < 6; ++i)
	{
		for (int j = 0; j < 6; ++j)
		{
			indices.push_back( ind[j] + 4 * i );
		}
	}

	glGenBuffers(1, &entry->vbo[INDEX_BUFFER]);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, entry->vbo[INDEX_BUFFER]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * 3 * 2 * sizeof(GLuint), &indices[0], GL_STATIC_DRAW); // 2 triangles

	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(3);
	glBindVertexArray(0);
	
	Util::checkGLError("Error creating cube mesh entry");
	return entry;
}

void sip::MeshEntry::setVertexColor(int vertexIndex, const glm::vec4& color)
{
	vertexIndex *= 4;

	if (vertexIndex >= m_vertexColors.size()) return;
	//std::cout << vertexIndex << " / " << m_vertexColors.size() << "\n";

	m_vertexColors[vertexIndex] = color.r;
	m_vertexColors[++vertexIndex] = color.g;
	m_vertexColors[++vertexIndex] = color.b;
	m_vertexColors[++vertexIndex] = color.a;
}

void sip::MeshEntry::applyVertexColors()
{
	glBindBuffer(GL_ARRAY_BUFFER, vbo[VERTEX_COLORS_BUFFER]);
	glBufferData(GL_ARRAY_BUFFER, 4 * m_aiMesh->mNumVertices * sizeof(GLfloat), &m_vertexColors[0], GL_STATIC_DRAW);

	glVertexAttribPointer(8, 4, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(8);
}

MeshEntry* MeshEntry::createPlane(const glm::vec3& pos)
{
	MeshEntry* entry = new MeshEntry();
	entry->m_elementCount = 2 * 3;
	entry->vbo[VERTEX_BUFFER] = NULL;
	entry->vbo[TEXCOORD_BUFFER] = NULL;
	entry->vbo[NORMAL_BUFFER] = NULL;
	entry->vbo[INDEX_BUFFER] = NULL;

	// animation
	entry->vbo[VERTEX_BONE_INDICES_BUFFER] = NULL;
	entry->vbo[VERTEX_WEIGHTS_BUFFER] = NULL;

	glGenVertexArrays(1, &entry->vao);
	glBindVertexArray(entry->vao);

	float vertices[4 * 3];
	int indx = 0;
	const float span = 99.0f;
	vertices[indx++] = pos.x - span; // x
	vertices[indx++] = pos.y;  // y
	vertices[indx++] = pos.z - span;  // z

	vertices[indx++] = pos.x - span;  // x
	vertices[indx++] = pos.y;  // y
	vertices[indx++] = pos.z + span; // z

	vertices[indx++] = pos.x+span;  // x
	vertices[indx++] = pos.y;  // y
	vertices[indx++] = pos.z + span; // z

	vertices[indx++] = pos.x + span;  // x
	vertices[indx++] = pos.y;  // y
	vertices[indx++] = pos.z - span;  // z

	entry->m_origin = pos;
	entry->m_boundingSphereRadius = span;

	glGenBuffers(1, &entry->vbo[VERTEX_BUFFER]);
	glBindBuffer(GL_ARRAY_BUFFER, entry->vbo[VERTEX_BUFFER]);
	glBufferData(GL_ARRAY_BUFFER, 3 * 4 * sizeof(GLfloat), vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);
	////////////////////// indices
	unsigned int indices[6] = {0, 2, 1, 0, 3, 2};

	glGenBuffers(1, &entry->vbo[INDEX_BUFFER]);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, entry->vbo[INDEX_BUFFER]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3 * 2 * sizeof(GLuint), indices, GL_STATIC_DRAW); // 2 triangles

	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(3);

	///////////////// normals
	float normals[3 * 4] = {0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f , 0.0f, 1.0f, 0.0f , 0.0f, 1.0f, 0.0f };
	glGenBuffers(1, &entry->vbo[NORMAL_BUFFER]);
	glBindBuffer(GL_ARRAY_BUFFER, entry->vbo[NORMAL_BUFFER]);
	glBufferData(GL_ARRAY_BUFFER, 3 * 4 * sizeof(GLfloat), normals, GL_STATIC_DRAW);

	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(2);
	////////////////////// uvs
	float texCoords[4 * 2] = { 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f };
	glGenBuffers(1, &entry->vbo[TEXCOORD_BUFFER]);
	glBindBuffer(GL_ARRAY_BUFFER, entry->vbo[TEXCOORD_BUFFER]);
	glBufferData(GL_ARRAY_BUFFER, 4 * 2 * sizeof(GLfloat), texCoords, GL_STATIC_DRAW);

	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(1);

	//////////// Done
	glBindVertexArray(0);
	return entry;
}

Material* MeshEntry::getMaterial()
{
	return m_material;
}

int MeshEntry::getIndexToMaterial() const
{
	return m_aiMesh->mMaterialIndex;
}

void MeshEntry::setMaterial(Material* mat)
{
	m_materialKey = mat->Key;
	m_material = mat;
}
