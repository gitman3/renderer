#include <memory>
#include <sstream>
#include <iostream>

#include "assimp/Importer.hpp"
#include "assimp/postprocess.h"
#include "Debug.h"
#include "Mesh.h"
#include "ShaderCache.h"
#include "Material.h"
#include "Director.h"
#include "World.h"
#include "Picker.h"
#include "Animation.h"
#include "Util.h"
#include "MeshEntry.h"
#include "Camera.h"
#include "TextureManager.h"
#include "MaterialCache.h"
#include "EditorGUI.h"
#include "IDGenerator.h"
using namespace sip;


Mesh::Mesh(const std::string& filename) : 
	m_aiScene(nullptr), 
	m_meshEntries(),
	m_id(getNewID<Mesh>()),
	m_importer(std::make_shared<Assimp::Importer>())
{
	
	debug::log("[Mesh] Loading File : ", filename);
	m_aiScene = m_importer->ReadFile(filename,   aiProcess_GenSmoothNormals | aiProcess_CalcTangentSpace | aiProcess_LimitBoneWeights );

	if (!m_aiScene)
	{
		debug::log(m_importer->GetErrorString(), "[MESH] Failed to load mesh");
		editor::EditorGUI::logFail("cannot load file : ", filename);
		return;
	}

	if (m_aiScene->HasMaterials())
	{
		for (unsigned int i = 0; i < m_aiScene->mNumMaterials; ++i)
		{
			auto mat = m_aiScene->mMaterials[i];
			MaterialCache::Instance().createMaterial(mat, filename, i);
		}
	}

	for (unsigned int i = 0; i < m_aiScene->mNumMeshes; ++i)
	{
		const aiString& name = m_aiScene->mMeshes[i]->mName;
		debug::log("[Mesh] loading mesh ", name.C_Str());
		m_meshEntries.push_back(new MeshEntry(m_aiScene->mMeshes[i]));
	}

	for (auto entry : m_meshEntries)
	{
		entry->setMaterial(MaterialCache::Instance().getMaterial({ filename, entry->getIndexToMaterial() }));
	}

	if (m_aiScene->HasAnimations())
	{
		for (unsigned int i = 0; i < m_aiScene->mNumAnimations; ++i)
		{
			aiAnimation* anim = m_aiScene->mAnimations[i];
			//debug::log("[Mesh] loading animations ", anim->mName.C_Str());
			debug::log("bone animations : ", anim->mNumChannels);

			for (unsigned int j = 0; j < anim->mNumChannels; ++j)
			{
				aiNodeAnim* nodeAnim = anim->mChannels[j];
				auto nodeName = nodeAnim->mNodeName.C_Str(); // name of node in the scene
				//EngineDebug::log("[Mesh] node(bone)Name ", nodeName);
				addAnimation(nodeAnim);
			}
			

			for (unsigned int j = 0; j < anim->mNumMeshChannels; ++j)
			{
				aiMeshAnim* meshAnim = anim->mMeshChannels[j];
				//EngineDebug::log("[Mesh] meshName ", meshAnim->mName.C_Str());
			}
		}
	}

	debug::log("[Mesh] Done!");

}

Mesh::Mesh() :
	m_aiScene(nullptr),
	m_meshEntries(),
	m_id(getNewID<Mesh>()),
	m_importer(std::make_shared<Assimp::Importer>())
{

}

Mesh::~Mesh()
{
	for (unsigned int i = 0; i < m_meshEntries.size(); ++i)
	{
		delete m_meshEntries[i];
		m_meshEntries[i] = nullptr;
	}
}

void Mesh::addAnimation(aiNodeAnim* anim)
{
	Debug::log("Added Bone Animation for ", anim->mNodeName.C_Str());
	m_animations.push_back(std::make_shared<Animation>(anim, this));
}

Mesh* Mesh::createPlane(const glm::vec3& pos)
{
	auto plane = MeshEntry::createPlane(pos);
	Mesh* mesh = new Mesh();
	mesh->m_meshEntries.push_back(plane);
	
	MaterialKey key = MaterialCache::Instance().getCustomMaterialKey(MaterialCache::POS_COL_LIGHT_PROJ_NOTEXTURES_KEY);

	if (!MaterialCache::Instance().getMaterial(key))
	{
		auto mat = std::make_shared<Material>(key);
		mat->setProgram(ShaderCache::Instance()->getShader(ShaderCache::SNAME_POS_COL_LIGHT_PROJ_NOTEXTURES));
		MaterialCache::Instance().cacheMaterial(key, mat);
	}

	auto mat = MaterialCache::Instance().getMaterial(key);
	plane->setMaterial(mat);
	return mesh;
}

Material* Mesh::getDefaultMaterial()
{
	return m_meshEntries[0]->getMaterial();
}

Mesh* Mesh::createCubeMapMesh()
{
	Mesh* mesh = new Mesh();
	auto entry = MeshEntry::createCube();
	mesh->m_meshEntries.push_back(entry);

	MaterialKey key = MaterialCache::Instance().getCustomMaterialKey(MaterialCache::CUBEMAPPED);
	Util::checkGLError("Error creating cubemap");

	if (!MaterialCache::Instance().getMaterial(key))
	{
		auto mat = std::make_shared<Material>(key);
		mat->setProgram(ShaderCache::Instance()->getShader(ShaderCache::SNAME_CUBEMAP));
		MaterialCache::Instance().cacheMaterial(key, mat);
	}
	Util::checkGLError("Error creating cubemap");

	auto mat = MaterialCache::Instance().getMaterial(key);
	entry->setMaterial(mat);

	return mesh;
}
