#pragma  once

#include "glm/mat4x4.hpp"
#include "glm/trigonometric.hpp"
#include "glm/common.hpp"
#include <vector>
#include "Component.h"
#include "glm/gtc/quaternion.hpp"
#include "Passkey.h"
#include "Handle.h"
#include "Types.h"

namespace sip
{
	class World;
	class Entity;
	class Batcher;

	class Transform : public Component
	{
		


		// mostly placeholder for position
		//glm::mat4 m_localTransform;
		glm::mat4 m_worldTransform;
		glm::fquat m_rotation;
		glm::vec3 m_position;

		float m_scale;
		
		Handle<Transform> m_parent;
		Handle<Entity> m_entity;

		bool    m_worldTransformDirty;

		std::vector<Handle<Transform>> m_children;
		void markWorldTransformDirty();
		void rebuildWorldTransforms();

		int m_ID;
		

	public:
		bool m_localMatrixDirty;

		Handle<Transform> this_handle;

		Transform();
		inline Handle<Entity> getEntity() { return m_entity; }
		inline void setEntity(Handle<Entity> handle) { m_entity = handle; }

		void setRotation(const glm::fquat& rotation);
		//void setLocalTransform(const glm::mat4& mat);
		void setScale(float scale);

		inline std::vector<Handle<Transform>>& getChildren() { return m_children; }

		/** Calculates a matrix out of position, rotation and scale*/
		glm::mat4 getLocalTransform();
		inline glm::fquat& getRotation() { return m_rotation; }
		glm::mat4 getWorldTransform(PassKey<Batcher>);

		//inline glm::vec3  getLocalPosition() { return glm::vec3(m_localTransform[3]); }
		inline glm::vec3 getLocalPosition() { return m_position; }
		inline glm::vec3  getScale() { return glm::vec3(m_scale); }

		void translateLocal(const glm::vec3& by);
		void rotateLocal(const glm::vec3& axis, float radians);

		//void setWorldPosition(const glm::vec3& pos);
		void setLocalPosition(const glm::vec3& pos);
		

		void addChild(Handle<Transform> child);
		void detachChild(Handle<Transform> child);
		int _mem_id() { return m_ID; }
		
		//Transform(Handle<Entity> e);
		virtual ~Transform();

		friend class World;

	};
}

