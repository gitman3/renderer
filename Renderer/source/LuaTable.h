#pragma once

#include <assert.h>
#include <string>

#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"

namespace sip
{
	struct LuaStackRestore
	{
		int m_stackPointer;
		lua_State* m_state;

		LuaStackRestore();
		LuaStackRestore(lua_State* state);

		~LuaStackRestore();
		void init(lua_State* state);
	};


	class LuaTable 
	{
	private:
		lua_State* m_state;
		int m_ref;
		int* m_refCount;


		void pushNumeric(int index) const;
		void pushStr(const char* value) const;

	public:
		bool isTable(const char* value) const;
		bool isString(const char* value) const;
		bool isFloat(const char* value) const;
		bool isBoolean(const char* value) const;

		bool isTable(const int index) const;
		bool isString(const int index) const;
		bool isFloat(const int index) const;
		bool isBoolean(const int index) const;

		LuaTable getTable(const char* value) const;
		LuaTable getTable(int index) const;

		std::string getString(const char* value) const;
		std::string getString(int index) const;

		bool getBoolean(int index) const;
		bool getBoolean(const char* index) const;

		float getFloat(const char* value, float defaultValue = FLT_MAX) const;
		float getFloat(int index) const;

		void setFloat(const char* name, float value);
		void setFloat(const char* name, int value);
		void setString(const char* name, const char* value);
		void setBool(const char* name, const bool value);

		inline static void createGlobalTable(lua_State* state, const char* name)
		{
			lua_newtable(state);
			lua_setglobal(state, name);
		}

		/** set subtable to true, if you get table data through function called by lua to c++, through the stack */
		inline LuaTable(lua_State* state, bool subtable) : m_state(state),
			m_refCount(new int(1))
		{
			if (subtable)
			{
				m_ref = luaL_ref(state, LUA_REGISTRYINDEX);
			}
			else
			{
				lua_newtable(state);
				m_ref = luaL_ref(state, LUA_REGISTRYINDEX);
			}
		};

		inline LuaTable(lua_State* state, const char* name) : m_state(state), m_ref(-1), m_refCount(new int(1))
		{
			lua_getglobal(m_state, name);
			m_ref = luaL_ref(state, LUA_REGISTRYINDEX);
		}

		LuaTable(const LuaTable& other)
		{
			m_refCount = other.m_refCount;
			m_state = other.m_state;
			m_ref = other.m_ref;
			(*m_refCount)++;
		}


		LuaTable& operator=(const LuaTable& other)
		{
			if (&other == this)
			{
				return *this;
			}

			m_ref = other.m_ref;
			m_refCount = other.m_refCount;
			(*m_refCount)++;
			return *this;
		}

		~LuaTable()
		{
			if (--(*m_refCount) == 0)
			{
				delete m_refCount;
				m_refCount = nullptr;
				luaL_unref(m_state, LUA_REGISTRYINDEX, m_ref);
			}
		}

		void printTable() const;
	};
}

