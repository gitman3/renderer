#include <vector>
#include <map>
#include <tuple>

#include "glew.h"
#include "Batcher.h"
#include "Context.h"
#include "World.h"
#include "Material.h"
#include "Mesh.h"
#include "MeshEntry.h"
#include "Director.h"
#include "Debug.h"
#include "EditorGUI.h"
#include "Transform.h"
#include "Types.h"
#include "Entity.h"
#include "GLProgram.h"
#include "Camera.h"
#include "GLStateRestore.h"
#include "glm/mat4x4.hpp"
#include "glm/vec3.hpp"
#include "Storage.h"

using namespace sip;

namespace 
{

	struct BatchId
	{
		unsigned int glFlags;
		unsigned int programId;
		Material* material;
		unsigned long long ID;
		float distanceToCamera;
		BatchId() :
			glFlags(0),
			programId(0),
			material(nullptr),
			ID(0),
			distanceToCamera(0)
		{}
	};

	struct cmpBatchID 
	{
		bool operator()(const BatchId& a, const BatchId& b) const {
			if (a.glFlags & ShaderFlags::BLEND_ENABLED)
			{
				if (a.distanceToCamera == b.distanceToCamera)
				{
					return a.ID > b.ID;
				}
				return a.distanceToCamera > b.distanceToCamera;
			}
			else
			{
				return a.ID > b.ID;
			}

		}
	};

	typedef std::tuple<Handle<Entity>, Material*, Handle<Transform>> DrawEntityRequest;

	struct BatchData
	{
		glm::mat4x4 worldMatrix;
		BatchId id;
		GLuint vao;
		unsigned int elementCount;
	};

	std::vector<DrawEntityRequest> CustomDrawEntityRequests;
	typedef std::map<BatchId, std::vector<BatchData>, cmpBatchID> batchMap;

	batchMap OpaqueBatches;
	batchMap TransparentBatches;

	/** 
		Batch ID is made of : 
		GL flags, where transparent objects have high bits set to keep it at the top of the list 
		Shader ID, which is the running number of each shader.
	*/
	BatchId getBatchId(Material* mat, float distanceToCamera)
	{
		BatchId bid;
		bid.glFlags = mat->getProgram()->getShader().Flags;
		bid.material = mat;
		bid.programId = mat->getProgram()->getId();
		bid.ID = (bid.glFlags << 31) | bid.programId;
		bid.ID |= mat->getID() << 16;
		bid.distanceToCamera = distanceToCamera;
		return bid;
		/*
		auto glFlags = mat->getProgram()->getShader().Flags;
		auto id = mat->getProgram()->getId();
		return (glFlags << 31) | id;
		*/
	}

	void setupProgramCommons(Material* mat, World* world)
	{
		auto prog = mat->getProgram();
		debug::checkGLError("Error before render pass");
		prog->use();
		debug::checkGLError("Error before render pass");
		auto camera = world->getActiveCamera();
		debug::checkGLError("Error before render pass");

		//prog->useDepthTexture(world->m_ssaoBlurTexture);
		prog->setUniform("u_v", camera->getViewMatrix());
		prog->setUniform("u_p", world->getActiveCamera()->getProjectionMatrix());
		prog->setUniform("u_viewDirection", world->getViewDirection());
		prog->setUniform("u_ambientColor", world->getAmbientLightColor());
		prog->setUniform("u_lightDir", world->getSunDirection());
		prog->setUniform("u_cameraPosition", world->getCameraPosition());
	}

}

Batcher::Batcher()
{
}


void Batcher::buildBatches()
{
	OpaqueBatches.clear();
	TransparentBatches.clear();

	auto world = Director::Instance().getWorld();
	debug::logFail(world != nullptr, "World is null at batch building");
	
	auto camera = world->getActiveCamera();
	auto memory = Storage<Transform>::Instance().getMemory();

	for (auto iter = memory->begin(); iter != memory->end(); ++iter)
	{
		auto hEntity = iter.get().getEntity();
		if (hEntity.isDeleted()) continue;

		createRenderCommand(hEntity, iter.get(), camera, nullptr);
	}

	for (uint i = 0; i < CustomDrawEntityRequests.size(); ++i)
	{
		auto req = CustomDrawEntityRequests[i];
		auto hEntity = std::get<0>(req);
		auto pMaterial = std::get<1>(req);
		if (hEntity.isDeleted()) continue;
		auto pEntity = entityPtr(hEntity);
		auto pTransform = transformPtr(pEntity->getTransform());
		createRenderCommand(hEntity, *pTransform, camera, pMaterial);
	}

	CustomDrawEntityRequests.clear();

}

void Batcher::render()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	//GLStateRestore sr(GLStateRestore::FBO | GLStateRestore::TEX);

	// sort transparent batches


	auto world = Director::Instance().getWorld();
	auto camera = world->getActiveCamera();
	
	glDepthMask(GL_TRUE); // depth mask must be set or clear does nothing

	glDepthRange(0.0f, 1.0f);
	glClearDepth(1.0f);
	glClearColor(world->getBGColor().r, world->getBGColor().g, world->getBGColor().b, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	glDisable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


	for (int i = 0; i < 2; ++i)
	{
		batchMap& Batches = i == 0 ? OpaqueBatches : TransparentBatches;

		for (const auto& kp : Batches)
		{

			auto mat = kp.first.material;
			debug::checkGLError("Error before render pass");

			setupProgramCommons(mat, world);
			debug::checkGLError("Error after render pass");
			mat->use();
			debug::checkGLError("Error after render pass");

			auto prog = mat->getProgram();
			for (auto& data : kp.second)
			{

				prog->setUniform("u_m", data.worldMatrix);
				debug::checkGLError("Error after render pass");
				auto mvMatrix = camera->getViewMatrix() * data.worldMatrix;
				debug::checkGLError("Error after render pass");
				prog->setUniform("u_v", camera->getViewMatrix());
				debug::checkGLError("Error after render pass");

				prog->setUniform("u_mv", mvMatrix);
				prog->setUniform("u_vp", camera->getProjectionMatrix() * camera->getViewMatrix());
				prog->setUniform("u_mvp", camera->getViewProjectionMatrix() * data.worldMatrix);
				prog->setUniform("u_normalmat", glm::transpose(glm::inverse(mvMatrix)));
				debug::checkGLError("Error after render pass");

				glBindVertexArray(data.vao);
				glDrawElements(GL_TRIANGLES, data.elementCount, GL_UNSIGNED_INT, NULL);
				glBindVertexArray(0);
			}
			debug::checkGLError("Error after render pass");

		}

	}

	glEnable(GL_CULL_FACE);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

}


void Batcher::issueCustomRenderCommand(Handle<Entity> entity, Material& material)
{
	assert(!entity.isDeleted());
	CustomDrawEntityRequests.push_back(DrawEntityRequest(entity, &material, entityPtr(entity)->getTransform()));
}


void issueCustomRenderCommand(Handle<Entity> entity, Material& material, Handle<Transform> transform)
{
	CustomDrawEntityRequests.push_back(DrawEntityRequest(entity, &material, transform));
}

void sip::Batcher::createRenderCommand(Handle<Entity> entity, Transform& transform, Camera* camera, Material* material)
{
	if (transform.getEntity().isDeleted()) return; // TODO: should not need this really, unless in editor mode

	auto pEntity = entityPtr(entity);
	if (!pEntity->isVisible()) return;

	auto mesh = pEntity->getMesh();
	if (!mesh) return;
	auto entries = mesh->getMeshEntries();

	auto worldMat = transform.getWorldTransform({});
	for (auto entry : mesh->getMeshEntries())
	{
		// TODO: batch cull
//		if (!camera->cull(entry, transform.getLocalTransform(), transform.getScale()))
		{
			
			auto mat = material ? material : 
				pEntity->getMaterial() ? pEntity->getMaterial() : 
				entry->getMaterial();
			float distanceToCamera = glm::length(camera->getPosition() - pEntity->getTransformPtr()->getLocalPosition());
			auto batchId = getBatchId(mat, distanceToCamera);
			batchMap& batch = mat->getProgram()->getShader().isset(ShaderFlags::BLEND_ENABLED) ? TransparentBatches : OpaqueBatches;
			batch[batchId].push_back({ worldMat, batchId, entry->vao, entry->m_elementCount });
		}
	}
}

