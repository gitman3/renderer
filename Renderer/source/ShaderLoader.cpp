

#include <iostream>
#include <memory>

#include <string>
#include <fstream>
#include <streambuf>
#include <algorithm>
#include <sstream>

#include "ShaderLoader.h"
#include "Util.h"
#include "Debug.h"
#include "EditorGUI.h"
#include "ShaderParser.h"

using namespace sip;

const char * const ShaderLoader::A_NAME_VCOL = "a_vertexCol";
const char * const ShaderLoader::A_NAME_VPOS = "a_vertexPos";

GLuint ShaderLoader::programFrom(const std::string& path, GLint type)
{

	const char* suffix = type == GL_VERTEX_SHADER ? ".vert" :
		type == GL_FRAGMENT_SHADER ? ".frag" :
		type == GL_GEOMETRY_SHADER ? ".geom" : ".compute";
	
	const std::string fullPath = path + suffix;
	bool customType = suffix == ".shader"; 

	if (Util::fileExists(fullPath))
	{
		auto src = Util::fileAsString(fullPath);
		return (createShader(fullPath, src, type));
	}
	debug::checkGLError("Error creating shader");

	return 0;
}



GLuint ShaderLoader::createFromPath(const std::string& path, GLint type)
{
	const std::string& f = Util::fileAsString(path);
	return createShader(path, f, type);
}


GLuint sip::ShaderLoader::link(std::vector<Shader>& shaders)
{
	std::vector<GLuint> shaderHandles;
	for (auto& shader : shaders)
	{
		shaderHandles.push_back(shader.Handle);
	}
	return link(&shaderHandles[0], shaderHandles.size());
}


GLuint ShaderLoader::link(GLuint* shaders, int count)
{
	auto out = glCreateProgram();

	for (int i = 0; i < count; ++i)
	{
		glAttachShader(out, shaders[i]);
	}

	glLinkProgram(out);
	
	GLint status;
	glGetProgramiv(out, GL_LINK_STATUS, &status);

	if (status == GL_FALSE)
	{
		GLint logLength;
		glGetProgramiv(out, GL_INFO_LOG_LENGTH, &logLength);

		if (logLength > 0)
		{
			std::unique_ptr<char[]> log(new char[logLength]);
			GLsizei written;
			glGetProgramInfoLog(out, logLength, &written, log.get());
			Debug::log("Failed to link");
			Debug::logFail(false, std::string(log.get()));
		}
	}
	else
	{
		Debug::log("Link successful!");
			
	}
	return out;
}


Shader ShaderLoader::createShader(std::string path)
{

	std::vector <GLuint> shaders;
	if (Util::fileExists(path + ".shader"))
	{
		debug::checkGLError("Error creating shader");
		auto output = ShaderParser::parseShader(path);
		auto vShader = createShader(path, output.vertexShaderSrc, GL_VERTEX_SHADER);
		auto fShader = createShader(path, output.fragmentShaderSrc, GL_FRAGMENT_SHADER);
		shaders.push_back(vShader);
		shaders.push_back(fShader);
		
		output.shader.Handle = link(&shaders[0], shaders.size());
		/*
		std::cout << "------- Vertex ------------ \n";
		std::cout << output.vertexShaderSrc << "\n";
		std::cout << "------- Fragment ------------ \n";
		std::cout << output.fragmentShaderSrc << "\n";
		*/
		debug::checkGLError("Error creating shader");
		output.shader.LegacyShader = false;
		return output.shader;
	}

	std::vector<GLint> types{ GL_VERTEX_SHADER, GL_FRAGMENT_SHADER, GL_GEOMETRY_SHADER, GL_COMPUTE_SHADER };

	for (auto t : types)
	{
		auto prog = programFrom(path, t);
		if (prog > 0)
		{
			shaders.push_back(prog);
		}
	}

	debug::logFail(shaders.size() > 0, "Could not create shader ", path.c_str());
	Shader finalShader;
	finalShader.LegacyShader = true;
	finalShader.Handle = link(&shaders[0], shaders.size());
	debug::checkGLError("Error creating shader");

	return finalShader;
}

GLuint ShaderLoader::createShader(const std::string& filename, const std::string& shaderStr, GLint type)
{
	auto shader = glCreateShader(type);
	if (shader == 0)
	{
		exit(EXIT_FAILURE);
	}

	const GLchar* shaderCode = shaderStr.c_str();
	glShaderSource(shader, 1, &shaderCode, NULL);

	glCompileShader(shader);

	GLint result = 0;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &result);

	if (result == GL_FALSE)
	{
		Debug::log("failed to create shader");
		Debug::log(shaderCode);

		GLint logLength;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);

		if (logLength > 0)
		{
			std::unique_ptr<char[]> log(new char[logLength]);
			GLsizei written;
			glGetShaderInfoLog(shader, logLength, &written, log.get());
			Debug::logFail(false, log.get());
		}
	}
	else
	{
		Debug::log(filename + " compiled succesfully");
	}
	return shader;
}