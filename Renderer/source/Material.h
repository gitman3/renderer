#pragma once

#include <vector>
#include <string>
#include <memory>
#include <map>

#include "assimp\Importer.hpp"
#include "assimp\postprocess.h"
#include "Texture.h"
#include "Types.h"
#include "Passkey.h"
#include "IDGenerator.h"
#include "CubeMap.h"
namespace sip
{

	class GLProgram;
	class MaterialCache;
	class Director;

	class Material
	{

		std::string m_materialConfig;
		std::vector <Texture> m_textures;
		std::vector <Texture> m_normalMaps;

		std::string m_pathToFolder;
		std::string m_materialConfName;

		CubeMap* m_cubemap;

		GLProgram* m_program;

		// name is needed for reloading the shader when it's refreshed
		std::string m_programName;
		unsigned int m_ID;

	public:
		static const int MAX_TEXTURES = 3;


		Material(MaterialKey key) :
			SpecularColor(1.0f),
			DiffuseColor(1.0f),
			Specular(1.0f),
			m_program(nullptr),
			m_materialConfig(),
			m_materialConfName(),
			BumpPower(5.0f),
			BumpAmount(0.5f),
			RimPower(5.0f),
			RimAmount(0.5f),
			m_programName(),
			Key(key),
			m_ID(getNewID<Material>()),
			m_cubemap(nullptr)
		{}

		MaterialKey Key;
		glm::vec3 SpecularColor;
		glm::vec3 DiffuseColor;

		float Specular;
		float BumpPower;
		float BumpAmount;
		float RimPower;
		float RimAmount;

		unsigned int getID() { return m_ID; }
		void setPathToFolder(PassKey<MaterialCache>, const std::string& folder) { m_pathToFolder = folder; }
		
		const std::string& getPathToFolder() { return m_pathToFolder; }
		const std::string& getProgramName() { return m_programName; }
		const std::string getMaterialConfigName() { return m_materialConfName; }

		std::vector<Texture>& getTextures() { return m_textures; }
		std::vector<Texture>& getNormalMaps() { return m_normalMaps; }
		glm::vec3 getDiffuseColor() { return DiffuseColor; }

		CubeMap* getCubeMap() { return m_cubemap; }
		void setCubeMap(CubeMap* cubeMap);

		void setDefaultTexture(Texture texture);
		void setTexture(int index, Texture texture);
		void setProgram(GLProgram* prog);
		void addTexture(Texture texture);
		
		Texture* getTexture(int index);
		GLProgram* getProgram() { return m_program; }

		const GLProgram* getProgram() const { return m_program; }

		~Material();
		void setMaterialConfig(const std::string& conf);
		void use();

		void setMaterialConfigName(PassKey<Director>, const std::string& name);

	};


}
