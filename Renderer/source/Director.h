#pragma once

namespace sip
{
	class World;
	class Material;
	class Context;


	class Director
	{
		std::string m_pathToMatConf;
		static bool sm_initialized;

		void reloadMaterialConfig();
		void refreshMaterial(Material* mat);
		void onRenderStart();
		void onRenderEnd();
		int m_cullCount;
		int m_drawCount;


		inline void onRendered(bool rendered)
		{
			m_drawCount += rendered;
			m_cullCount += !rendered;
		}

		Director();
		~Director();

		World* m_world;
		Context* m_context;
	public:
		static Director& Instance()
		{
			static Director instance;
			return instance;
		}

		void refreshWorldConfig(World* world);
		void loadMaterialConfigFile(const char* pathTo);
		void setMaterialConfig(Material* material, const std::string& materialName);
		void createContext(const char* pathToConfig, int w, int h);
		void update();

		Context* getContext();
		World* getWorld();

		friend class Entity;
		friend class World;
		friend class Mesh;
	};
}
