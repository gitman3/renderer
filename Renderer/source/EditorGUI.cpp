#include "EditorGUI.h"
#include "Keyboard.h"
#include "Context.h"
#include "DebugConsole.h"
#include "Inspector.h"
#include "3drParty/tgaio.h"
#include "Time.h"
#include "Mouse.h"
#include "Director.h"
#include "Editor.h"
#include "World.h"
#include "Camera.h"
#include "TextureManager.h"
#include "ShaderCache.h"
#include "Mesh.h"
#include "Entity.h"
#include "Material.h"
#include "GLProgram.h"
#include "ShaderCache.h"
#include "LuaDataLoader.h"
#include "Debug.h"
#include "Storage.h"
#include "SceneLoader.h"
#include "SceneSerializer.h"
#include "MeshCache.h"
#include "Batcher.h"
#include "MaterialCache.h"
#include "RotationGizmo.h"
#include "TranslateGizmo.h"
#include "ScaleGizmo.h"
#include "AOBaker.h"
#include <thread>

using namespace sip;
using namespace sip::editor;


namespace
{
	GLFWcursor* RotateCursor;
	GLFWimage   RotateCursorImage;

	GLFWcursor* DefaultCursor;
	GLFWcursor* PanCursor;

	void windowFocused(GLFWwindow* win, int hasFocus)
	{
		EditorGUI::WindowHasFocus = hasFocus;
		EditorGUI::log(hasFocus ? "Editor gained focus" : "Editor lost focus");
	}

}

bool sip::editor::EditorGUI::WindowHasFocus;

EditorGUI::EditorGUI() : 
	m_debugConsole(nullptr),
	m_rotationGizmo(),
	m_translateGizmo(),
	m_selectionMode(SelectionMode::NONE)
{
	RotateCursorImage.pixels = TGAIO::read("../Renderer/resources/rotate.tga", RotateCursorImage.width, RotateCursorImage.height);
	RotateCursor =  glfwCreateCursor(&RotateCursorImage, RotateCursorImage.width / 2, RotateCursorImage.height / 2);
	DefaultCursor = glfwCreateStandardCursor(GLFW_ARROW_CURSOR);
	PanCursor =		glfwCreateStandardCursor(GLFW_HAND_CURSOR);
}


void EditorGUI::update()
{

	static Handle<Entity> currentSelection;
	auto world = Director::Instance().getWorld();

	float dt = Time::deltaTime();
	auto window = Director::Instance().getContext()->getWindow();
	glfwSetCursor(window, DefaultCursor);


	if (Keyboard::isKeyHold(Keyboard::CONTROL))
	{
		if (Keyboard::isKeyDown('S'))
		{
			if (!SceneLoader::Instance().getLoadedScene().getPath().empty())
			{
				SceneSerializer::Instance().serialize(SceneLoader::Instance().getLoadedScene());
			}
		}

		if (Keyboard::isKeyDown('C') && !currentSelection.isDeleted())
		{
			m_selectionsForCopy.clear();
			m_selectionsForCopy.push_back(currentSelection);
		}

		if (Keyboard::isKeyDown('V'))
		{
			for (auto& handle : m_selectionsForCopy)
			{
				if (!handle.isDeleted())
				{

					auto selectedEntityPtr = entityPtr(handle);
					auto selectedEntityTransform = selectedEntityPtr->getTransformPtr();
					auto ent = entityPtr(handle);
					auto pref = ent->getPrefab();
					auto newEntity = Editor::Instance().entityFromPrefab(pref->filePath);
					auto newEntityPtr = entityPtr(newEntity);
					newEntityPtr->getTransformPtr()->setLocalPosition(selectedEntityTransform->getLocalPosition());
					newEntityPtr->getTransformPtr()->setRotation(selectedEntityTransform->getRotation());
					newEntityPtr->getTransformPtr()->setScale(selectedEntityTransform->getScale().x);
					log("Copied ", newEntityPtr->getName());

				}
			}
		}

	}
	else
	{
		if (Keyboard::isKeyDown('C'))
		{
			m_debugConsole->toggleVisible();
		}
	}

	if (Keyboard::isKeyDown('W'))
	{
		m_selectionMode = SelectionMode::ROTATE;
	}
	else if (Keyboard::isKeyDown('Q'))
	{
		m_selectionMode = SelectionMode::TRANSLATE;
	}
	else if (Keyboard::isKeyDown('E'))
	{
		m_selectionMode = SelectionMode::SCALE;
	}

	auto selection = getInspector()->getSelection();

	if (!selection.isDeleted() && Keyboard::isKeyDown('B'))
	{
		//BakeThread = std::thread(BakeAO, selection);
		AOBaker::Instance().bakeAO(selection);
	}

	if (!selection.isDeleted())
	{
		AOBaker::Instance().update();
	}

	m_translateGizmo->onSelected(m_selectionMode == SelectionMode::TRANSLATE ? selection : Handle<Entity>());
	m_rotationGizmo->onSelected(m_selectionMode == SelectionMode::ROTATE ? selection : Handle<Entity>());
	m_scaleGizmo->onSelected(m_selectionMode == SelectionMode::SCALE ? selection : Handle<Entity>());
	

	if (world->getActiveCamera() != nullptr)
	{
		if (Mouse::isButtonDown(Mouse::Left))
		{
			auto ent = Editor::Instance().pickEntity(Mouse::getX(), Mouse::getY());

			// nothing selected
			if (ent.isDeleted())
			{
				currentSelection = ent;
				getInspector()->onSelected(ent);
			}

			if (currentSelection.isDeleted() && !ent.isDeleted())
			{
				currentSelection = ent;
				getInspector()->onSelected(ent);
			}

			// keep old selection 
			if (!currentSelection.isDeleted())
			{
				getInspector()->onSelected(currentSelection);
			}
		}


		if (!selection.isDeleted())
		{
			auto material = MaterialCache::Instance().getCustomMaterialWithKey(MaterialCache::EDITOR_WIREFRAME_KEY);
			Batcher::Instance().issueCustomRenderCommand(selection, *material);
		}

		if (Mouse::getScrollY() != 0.0)
		{
			world->getActiveCamera()->translateLocal(glm::vec3(0.0f, 0.0f, -(float)Mouse::getScrollY() * dt * 10.f));
		}

		if (Mouse::isButtonHold(Mouse::Middle))
		{
			glfwSetCursor(window, PanCursor);
			world->getActiveCamera()->translateLocal(glm::vec3(-Mouse::getDeltaX(), Mouse::getDeltaY(), 0.0f) * dt * .5f);
		}

		
		static float mxd = 0.0f;
		mxd = glm::mix(mxd, (float)Mouse::getDeltaX() * dt * .15f, .7f);

		static float mxy = 0.0f;
		mxy = glm::mix(mxy, (float)Mouse::getDeltaY() * dt * .15f, .7f);
		

		if (Mouse::isButtonHold(Mouse::Right))
		{
			glfwSetCursor(window, RotateCursor);
			world->getActiveCamera()->lookAround(mxd, mxy);
		}
	}
	
	
	// updates inspector UI
	m_inspector->onSelected(m_inspector->getSelection());


	if (Keyboard::isKeyDown(Keyboard::DEL))
	{
		auto selection = m_inspector->getSelection();
		if (!selection.isDeleted())
		{
			Editor::Instance().deleteEntity(selection);
		}
	}
	

}



void EditorGUI::InitGUI()
{
	m_debugConsole.reset(new DebugConsole());
	m_inspector.reset(new Inspector());
	WindowHasFocus = true;
	
	//////// create default plane

	{
		Entity* plane = Storage<Entity>::convert(Editor::Instance().createPlane(glm::vec3(0.0f, 0.0f, 0.0f)));
		plane->setName("EditorGrid");
		plane->setInspectorSelectable(false);
		Texture* texture = TextureManager::Instance().addTextureIfMissing("../Renderer/resources/grid.png");

		assert(texture != nullptr);
		auto mat = plane->getMesh()->getDefaultMaterial();
		mat->setDefaultTexture(*texture);
		mat->setProgram(ShaderCache::Instance()->addShaderAtDefaultPath("editorplane"));
	}

	m_rotationGizmo.reset(new RotationGizmo());
	m_translateGizmo.reset(new TranslateGizmo());
	m_scaleGizmo.reset(new ScaleGizmo());
	////////////
	glfwSetDropCallback(Context::Instance().getWindow(), dragAndDropCallback);
	glfwSetWindowFocusCallback(Context::Instance().getWindow(), windowFocused);

}

Inspector* EditorGUI::getInspector()
{
	return m_inspector.get();
}

void EditorGUI::dragAndDropCallback(GLFWwindow* window, int count, const char** files)
{
	for (int i = 0; i < count; ++i)
	{
		EditorGUI::log("Dropped file: ", files[i]);
		if (Util::parseFilenameSuffix(files[i]) == "prefab")
		{
			Editor::Instance().entityFromPrefab(files[i]);
		}
		else if (Util::parseFilenameSuffix(files[i]) == "scene")
		{
			log("Loading Scene : ", files[i]);

			SceneLoader::Instance().loadScene(files[i]);

		}
		else if (Util::parseFilenameSuffix(files[i]) == "onion")
		{
			log("Loading project file : ", files[i]);
			auto project = SceneLoader::Instance().loadProjectFile(files[i]);
			SceneLoader::Instance().prepareProject(project);
		}
	}
}


