#pragma once
#include "DebugConsole.h"
#include "Inspector.h"
#include <memory>
#include <string>
namespace sip
{

	namespace editor
	{
		class RotationGizmo;
		class TranslateGizmo;
		class ScaleGizmo;
		class DebugConsole;
		class Inspector;

		class EditorGUI
		{
		public:

			enum SelectionMode
			{
				NONE,
				TRANSLATE,
				ROTATE,
				SCALE
			};

			static EditorGUI& Instance()
			{
				static EditorGUI Instance;
				return Instance;
			}

			void update();
			void InitGUI();

			template<typename T, typename...Args> static void log(T t, Args...args)
			{
				std::stringstream ss;
				ss << t << " ";
				Instance().m_debugConsole->m_pendingText += ss.str();
				log(args...);
			}

			template <typename T>
			static void log(T t)
			{
				std::stringstream ss;
				ss << t << " ";
				Instance().m_debugConsole->m_pendingText += ss.str();
				Instance().m_debugConsole->log(Instance().m_debugConsole->m_pendingText);
			}

			template<typename T, typename...Args> static void logFail(T t, Args...args)
			{
				std::stringstream ss;
				ss << t << " ";
				Instance().m_debugConsole->m_pendingText += ss.str();
				logFail(args...);
			}

			template <typename T>
			static void logFail(T t)
			{
				std::stringstream ss;
				ss << t << " ";
				Instance().m_debugConsole->m_pendingText += ss.str();
				Instance().m_debugConsole->log(Instance().m_debugConsole->m_pendingText, DebugConsole::FAIL);
			}
			Inspector* getInspector();

			static bool WindowHasFocus;

		private:
			SelectionMode m_selectionMode;
			std::vector<Handle<Entity>> m_selectionsForCopy;

			static void dragAndDropCallback(GLFWwindow* window, int, const char**);
			std::shared_ptr<RotationGizmo> m_rotationGizmo;
			std::shared_ptr<TranslateGizmo> m_translateGizmo;
			std::shared_ptr<ScaleGizmo> m_scaleGizmo;

			EditorGUI();
			std::unique_ptr<DebugConsole> m_debugConsole;
			std::unique_ptr<Inspector> m_inspector;
		};
	}
}
