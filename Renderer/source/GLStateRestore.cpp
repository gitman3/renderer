#include "GLStateRestore.h"

using namespace sip;

GLStateRestore::GLStateRestore(int options) : m_fboHandle(-1), m_texHandle(-1), m_rboHandle(-1), m_options(options)
{

	if ((m_options & ELEM_ARR) > 0)
	{
		glGetIntegerv(GL_ELEMENT_ARRAY_BUFFER_BINDING, &m_elemArrHandle);
	}

	if ((m_options & VERTEX_ARRAY) > 0)
	{
		glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &m_vaoHandle);
	}

	if ((m_options & ARRAY_BUFFER) > 0)
	{
		glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &m_arbufHandle);
	}

	if ((m_options & SHADER) > 0)
	{
		glGetIntegerv(GL_CURRENT_PROGRAM, &m_progHandle);
	}


	if ((m_options & FBO) > 0)
	{
		glGetIntegerv(GL_FRAMEBUFFER_BINDING, &m_fboHandle);
	}

	if ((m_options & RBO) > 0)
	{
		glGetIntegerv(GL_RENDERBUFFER_BINDING, &m_rboHandle);
	}

	if ((m_options & TEX) > 0)
	{
		glGetIntegerv(GL_TEXTURE_BINDING_2D, &m_texHandle);
	}
}

GLStateRestore::~GLStateRestore()
{


	if ((m_options & ELEM_ARR) > 0)
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_elemArrHandle);
	}

	if ((m_options & SHADER) > 0)
	{
		glUseProgram(m_progHandle);
	}

	if ((m_options & VERTEX_ARRAY) > 0)
	{
		glBindVertexArray(m_vaoHandle);
	}

	if ((m_options & ARRAY_BUFFER) > 0)
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_arbufHandle);
	}

	if ((m_options & FBO) > 0)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, m_fboHandle);
	}

	if ((m_options & RBO) > 0)
	{
		glBindRenderbuffer(GL_RENDERBUFFER, m_rboHandle);
	}

	if ((m_options & TEX) > 0)
	{
		glBindTexture(GL_TEXTURE_2D, m_texHandle);
	}
}
