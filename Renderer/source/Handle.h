#pragma once

#include "Types.h"

namespace sip
{


	template<typename T>
	class Handle
	{
private:
		uint64* m_data;

#ifdef _DEBUG
		int* RefCount;
		bool* Deleted;
		int* ObjectID;
		int* PtrToMemory;
#endif
		template<typename T> friend class MemoryManager;
		template<typename T> friend class Memory;

		void setRefCount(int count)
		{
			*m_data &= ~REFCOUNT_MASK;
			*m_data = *m_data | count;
#ifdef _DEBUG
			*RefCount = getRefCount();
#endif
		}

		void increaseRefCount()
		{
			setRefCount(getRefCount() + 1);
		}

		void decreaseRefCount()
		{
			assert(getRefCount() >= 0);
			setRefCount(getRefCount() - 1);
		}




		void setDeleted(bool deleted)
		{
			if (deleted)
			{
				*m_data |= DELETED_MASK;
			}
			else
			{
				*m_data = *m_data & (~DELETED_MASK);
			}
#ifdef _DEBUG
			*Deleted = isDeleted();
#endif
		}

		void setMemoryLocation(int index)
		{
			assert(index < 65535);
			*m_data &= ~INDEX_MASK;
			*m_data |= static_cast<uint64>(index) << 48;
#ifdef _DEBUG
			*PtrToMemory = getMemoryLocation();
#endif
		}

		uint getMemoryLocation() const
		{
			return static_cast<uint>((*m_data & INDEX_MASK) >> 48);
		}


		uint getId() const
		{
			return (*m_data & ID_MASK) >> 32;
		}

		void setId(int id)
		{
			assert(id < 65535);
			*m_data &= ~ID_MASK;
			*m_data |= static_cast<uint64>(id) << 32;
			setDeleted(id == 0);

#ifdef _DEBUG
			*ObjectID = getId();
#endif
		}



public:

		int getRefCount() const
		{
			return static_cast<int>((*m_data) & REFCOUNT_MASK);
		}

		bool isDeleted() const
		{
			return ((*m_data) & DELETED_MASK) > 0;
		}

		Handle() : 
			m_data(new uint64(0))
#ifdef _DEBUG
			,Deleted(new bool(true))
			,ObjectID(new int(0))
			,RefCount(new int(0))
			,PtrToMemory(new int(0))
#endif
		{
			setDeleted(true);
			setRefCount(1);
		}

		Handle(const Handle& other) :
			m_data(other.m_data)
#ifdef _DEBUG
			,ObjectID(other.ObjectID)
			,Deleted(other.Deleted)
			,RefCount(other.RefCount)
			,PtrToMemory(other.PtrToMemory)
#endif
		{
			increaseRefCount();
		}
		/*
		Handle(Handle&& other) :
			m_data(std::move(other.m_data))
#ifdef _DEBUG
			,ObjectID(std::move(other.ObjectID))
			,Deleted(std::move(other.Deleted))
			,RefCount(std::move(other.RefCount))
			,PtrToMemory(std::move(other.PtrToMemory))
#endif
		{
			other.m_data = nullptr;
		}
		*/

		Handle& operator=(const Handle& other) 
		{
			if (&other == this) return *this;
			if (other == *this) return *this;
			m_data = other.m_data;

#ifdef _DEBUG
			auto rc = getRefCount();
			auto orc = other.getRefCount();
			ObjectID = other.ObjectID;
			Deleted = other.Deleted;
			RefCount = other.RefCount;
			PtrToMemory = other.PtrToMemory;
#endif

			increaseRefCount();
			return *this;
		}

		/*
		Handle& operator=(Handle&& other)
		{
			if (&other == this) return *this;
			m_data = std::move(other.m_data);

#ifdef _DEBUG
			ObjectID = std::move(other.ObjectID);
			Deleted = std::move(other.Deleted);
			RefCount = std::move(other.RefCount);
			PtrToMemory = std::move(other.PtrToMemory);
			other.ObjectID = nullptr;
			other.Deleted = nullptr;
			other.RefCount = nullptr;
			other.PtrToMemory = nullptr;
#endif
			other.m_data = nullptr;
			return *this;
		}
		*/
		bool operator==(const Handle& other) const
		{
			return *m_data == *other.m_data;
		}

		bool operator !=(const Handle& other) const
		{
			return *m_data != *other.m_data;
		}

		~Handle()
		{
			decreaseRefCount();
			if (m_data != nullptr)
			{
			}
		}
	};
}

