#include <sstream>
#include <iostream>

#include "World.h"
#include "Entity.h"
#include "Debug.h"
#include "LuaEngine.h"
#include "LuaTable.h"
#include "LuaUtils.h"
#include "GLProgram.h"
#include "Director.h"
#include "ShaderCache.h"
#include "Context.h"
#include "GLStateRestore.h"
#include "Util.h"
#include "Camera.h"
#include "TextureManager.h"
#include "Transform.h"
#include "Material.h"
#include "Mesh.h"
#include "Batcher.h"
#include "MaterialCache.h"
#include "MeshEntry.h"
#include "EditorGUI.h"
#include "Prefab.h"
#include "glm/gtx/quaternion.hpp"
#include "MeshCache.h"
#include "Storage.h"
#include "Mouse.h"

using namespace sip;

World::World(const char* pathToConfig) :
	m_ambientLightColor(0.1f),
	m_globalLightDirection(glm::vec3(0.0f, 1.0f, 0.0f)),
	m_globalLightColor(1.0f),
	m_rootEntity(Storage<Entity>::newObject()),
	m_bgColor(0.1f, 0.1f, 0.1f),
	m_activeCamera(new Camera()),
	m_pathToConfig(pathToConfig)
{
	refreshWorldConfig();
	auto size = Context::Instance().getScreenSize();
}

World::~World()
{
}


void World::refreshWorldConfig()
{
	Director::Instance().refreshWorldConfig(this);
}

void World::recursiveGetChildrenAll(std::vector<Handle<Transform>>& children) const
{
	auto entity = Storage<Entity>::convert(m_rootEntity);
	recursiveGetChildren(entity->getTransform(), children);
}

void World::recursiveGetChildren(Handle<Transform> from, std::vector<Handle<Transform>>& children)
{
	if (!from.isDeleted())
	{
		children.push_back(from);

		for (auto c : Storage<Transform>::convert(from)->getChildren())
		{
			recursiveGetChildren(c, children);
		}
	}

}

void World::refreshMaterials()
{
	ShaderCache::Instance()->reloadAll({});
	MaterialCache::Instance().refreshMaterials();

	std::vector<Handle<Transform>> children;
	recursiveGetChildrenAll(children);

	for (auto child : children)
	{
		if (child.isDeleted()) continue;
		auto t = Storage<Transform>::convert(child);
		if (t->getEntity().isDeleted()) continue;
		auto e = Storage<Entity>::convert(t->getEntity());
		if (e == nullptr) continue;

		if (e->getMesh() == nullptr) continue;

		auto entries = e->getMesh()->getMeshEntries();
		
		for (auto entry : entries)
		{
			entry->setMaterial(MaterialCache::Instance().getMaterial(entry->getMaterialKey()));
		}
	}

	editor::EditorGUI::log("Refreshed shaders and materials.");
}

void World::lookAt(const glm::vec3& from, const glm::vec3& to)
{
	m_activeCamera->lookAt(from, to);
}


void World::render()
{
	Director::Instance().onRenderStart();
	Batcher::Instance().buildBatches();
	Batcher::Instance().render();
	Director::Instance().onRenderEnd();
}

void World::lookAround(float x, float y)
{
	m_activeCamera->lookAround(x, y);

}

void World::translateLocal(const glm::vec3& vec)
{
	m_activeCamera->translateLocal(vec);
}

void World::translateGlobal(const glm::vec3& vec)
{
	m_activeCamera->translateGlobal(vec);
}

void World::update()
{
}

glm::vec3 World::getViewDirection()
{
	return m_activeCamera->getViewDirection();
}

Handle<Entity> World::addEntity()
{
	auto hEntity = getRawEntity();
	auto pEntity = entityPtr(hEntity);
	transformPtr(entityPtr(m_rootEntity)->getTransform())->addChild(pEntity->getTransform());
	return hEntity;
}

Handle<sip::Entity> World::getRawEntity()
{
	auto hEntity = Storage<Entity>::newObject();
	auto pEntity = Storage<Entity>::convert(hEntity);

	auto pEntityTransform = Storage<Transform>::convert(pEntity->getTransform());
	pEntityTransform->setEntity(hEntity);
	return hEntity;
}


Handle<Entity> World::addEntity(const Prefab& prefab)
{
	auto hEntity = Storage<Entity>::newObject();
	auto pEntity = Storage<Entity>::convert(hEntity);
	
	pEntity->setPrefab(prefab);
	auto pEntityTransform = Storage<Transform>::convert(pEntity->getTransform());
	pEntityTransform->setEntity(hEntity);

	auto pRootEntity = Storage<Entity>::convert(m_rootEntity);
	auto pRootTransform = Storage<Transform>::convert(pRootEntity->getTransform());
	pRootTransform->addChild(pEntity->getTransform());

	auto mesh = MeshCache::Instance().getMesh(prefab.MeshFile);
	pEntity->setMesh(mesh);

	for (auto kp : prefab.MaterialMap)
	{
		bool hasMat = false;
		for (auto entry : mesh->getMeshEntries())
		{
			if (entry->getAIMesh()->mName.C_Str() == kp.first)
			{
				hasMat = true;
				auto material = MaterialCache::Instance().getWithUniqueName(kp.second);
				entry->setMaterial(material);
			}
		}
		assert(hasMat);
	}


	glm::quat quat;
	quat = glm::rotate(quat, prefab.Rotation.x, glm::vec3(1.0f, 0.0f, 0.0f));
	quat = glm::rotate(quat, prefab.Rotation.y, glm::vec3(0.0f, 1.0f, 0.0f));
	quat = glm::rotate(quat, prefab.Rotation.z, glm::vec3(0.0f, 0.0f, 1.0f));
	pEntity->setMaterialConf("metallic");

	Storage<Transform>::convert(pEntity->getTransform())->setRotation(quat);
	Storage<Transform>::convert(pEntity->getTransform())->setScale(prefab.Scale.x);
	return hEntity;
}


glm::vec3 World::getCameraPosition()
{
	assert(m_activeCamera != nullptr);
	return m_activeCamera->getPosition();
}

Camera* World::getActiveCamera()
{
	return m_activeCamera.get();
}

void World::rebuildWorldTransforms()
{
	auto rootTransform = Storage<Entity>::convert(m_rootEntity)->getTransform();
	Storage<Transform>::convert(rootTransform)->rebuildWorldTransforms();
}


void World::deleteEntity(Handle<Entity> entity)
{
	auto pEntity = entityPtr(entity);
	auto parent = transformPtr(pEntity->getTransform())->m_parent;

	Storage<Transform>::getManager().deleteObject(pEntity->getTransform());
	Storage<Entity>::getManager().deleteObject(entity);

	/*
	if (!parent.isDeleted())
	{
		transformPtr(parent)->detachChild(pEntity->getTransform());
	}
	*/
}

sip::RayTracing::Ray sip::World::mouseToWorldRay()
{
	return getActiveCamera()->screenToWorldRay({ Mouse::getX(), Mouse::getY() });
}

