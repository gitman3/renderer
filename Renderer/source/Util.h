#pragma once

#include <string>
#include <random>
#include "Types.h"

namespace sip
{
	class Util
	{

	public:

		static std::vector<std::string> getFileListRecursive(const std::string& path);

		static bool fileExists(const std::string& pathToFile);

		static std::string parseFilenameSuffix(const std::string& filename);
		static std::string trimPathFrom(const std::string& pathToFile);
		static std::string fileFromPath(const std::string& path);
		static std::string fileFromPathNoSuffix(const std::string& path);

		static bool stringContains(const std::string& sourceString, const std::string& searchString);
		static std::string trim(const std::string& str);
		static std::vector<std::string> splitString(const std::string& str, const char delim);
		static std::string fileAsString(const std::string& path);
		static void checkGLError(const std::string& message);
		static void writeToFile(const std::string& path, const std::string& content);
		static void fixSeparators(std::string& path);

		template<int min, int max>
		static float random()
		{
			static bool initialized = false;
			static std::random_device rd;
			static std::mt19937 mt(rd());
			static auto dist = std::uniform_real_distribution<>(min, max);

			if (!initialized)
			{
				mt.seed(std::mt19937::default_seed);
				initialized = true;
			}
			return dist(mt);
		}

	};
}

