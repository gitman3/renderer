#include "Picker.h"
#include "Mesh.h"
#include "ShaderCache.h"
#include "Debug.h"
#include "Entity.h"
#include "MeshEntry.h"
#include "Transform.h"
#include "EditorGUI.h"
#include "Director.h"
#include "Context.h"
#include "Storage.h"
using namespace sip;

namespace 
{
	static const int WORK_GROUPS = 1024;
}

std::map<GLuint, Picker::Entry> Picker::sm_data;

void Picker::initEntries(Entity* entity)
{

	auto mesh = entity->getMesh();
	auto entriesCount = mesh->getMeshEntries().size();

	for (auto meshEntry : mesh->getMeshEntries())
	{
		if (sm_data.find(meshEntry->getId()) != sm_data.end())
		{
			// already initialized
			continue;
		}

		auto& triangles = meshEntry->getTriangleList();
		if (triangles.size() == 0)
		{
			continue;
		}

		auto& data = sm_data[meshEntry->getId()];

		for (unsigned int i = 0; i < triangles.size(); i = i + 3)
		{
			data.m_computeData.push_back( ComputeData { glm::vec4(triangles[i], 1.f), glm::vec4(triangles[i+1], 1.0f), glm::vec4(triangles[i+2], 1.0f), glm::vec3(0.0f), -1.f });
		}

		GLuint bufSize = data.m_computeData.size() * sizeof(ComputeData);

		glGenBuffers(1, &data.bufferId);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, data.bufferId);
		glBufferData(GL_SHADER_STORAGE_BUFFER, bufSize, &data.m_computeData[0], GL_DYNAMIC_COPY);
		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

		int err = glGetError();
		debug::logFail(err == GL_NO_ERROR, "Error initializing compute shader");
	}
}


RayTracing::RayCollisionInfo Picker::pick(Entity* entity, const RayTracing::Ray& ray)
{


	auto shader = ShaderCache::Instance()->getShader(ShaderCache::SNAME_COMPUTE_RAY_MESH_COLLISION);
	shader->use();
	shader->setUniform("ModelMat", transformPtr(entity->getTransform())->getLocalTransform());
	shader->setUniform("rd", ray.direction);
	shader->setUniform("ro", ray.origin);

	initEntries(entity);
	RayTracing::RayCollisionInfo collision;

	for (auto& kp : sm_data)
	{
		
		auto& data = kp.second;
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, data.bufferId);

		glDispatchCompute(data.m_computeData.size() / WORK_GROUPS, 1, 1);
		glMemoryBarrier(GL_ALL_BARRIER_BITS);

		GLuint bufSize = data.m_computeData.size() * sizeof(ComputeData);
		//if (data.m_bufferPtr == nullptr)
		{
			// grab the pointer to data, can only be done at first glMapBufferRange() call
			auto ptr = glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, bufSize, GL_MAP_READ_BIT);
			data.m_bufferPtr = reinterpret_cast<ComputeData*>(ptr);
		}



		debug::checkGLError("Error with compute");
		auto infoPtr = data.m_bufferPtr;
		if (infoPtr == nullptr) continue;

		for (unsigned int i = 0; i < data.m_computeData.size(); ++i)
		{
			if (infoPtr->t > 0.0f)
			{
				if (collision.t < 0.0f || (collision.t >= 0.0f && infoPtr->t < collision.t))
				{
					collision.t = infoPtr->t;
					collision.ray = ray;
					collision.pos = ray.origin + collision.t * ray.direction;
				}
			}
			//infoPtr->t = -1.0f;
			++infoPtr;
		}
		
		// fix to buffer data being late one frame for some reason
		glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
	}
	GLfloat params[3];
	glGetUniformfv(shader->getHandle(), glGetUniformLocation(shader->getHandle(), "rd"), &params[0]);

	return collision;
}
