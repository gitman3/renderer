#include "CubeMap.h"
#include "TextureManager.h"
#include "pngloader.h"
#include "3drParty/tgaio.h"
#include "Util.h"
using namespace sip;

namespace 
{
	void setupCubeMap(GLuint& texture)
	{
		glActiveTexture(GL_TEXTURE0);
		glEnable(GL_TEXTURE_CUBE_MAP);
		glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	}

	void makeCubeMapSide(GLuint side, const std::string& pathToImage)
	{
		int w, h;
		auto png = PNGLoader::load(pathToImage.c_str(), w, h);
		auto format = TGAIO::getImageFormat(png, w, h);

		glTexImage2D(side,
			0,                  //level 
			format.first,            //internal format 
			w,                 //width 
			h,                 //height 
			0,                  //border 
			format.second,             //format 
			GL_UNSIGNED_BYTE,   //type 
			&png[0]); // pixel data 
		Util::checkGLError("Error creating cubemap");
	}

}

CubeMap::CubeMap(const std::string& pathPosX, const std::string& pathNegX, const std::string& pathPosY, const std::string& pathNegY, const std::string& pathPosZ, const std::string& pathNegZ) :
	m_pathPosX(pathPosX),
	m_pathNegX(pathNegX),
	m_pathPosY(pathPosY),
	m_pathNegY(pathNegY),
	m_pathPosZ(pathPosZ),
	m_pathNegZ(pathNegZ)
{
	
	setupCubeMap(m_texture);
	Util::checkGLError("Error creating cubemap");

	makeCubeMapSide(GL_TEXTURE_CUBE_MAP_POSITIVE_X, pathPosX);
	makeCubeMapSide(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, pathNegX);

	makeCubeMapSide(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, pathPosY);
	makeCubeMapSide(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, pathNegY);

	makeCubeMapSide(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, pathPosZ);
	makeCubeMapSide(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, pathNegZ);


}

