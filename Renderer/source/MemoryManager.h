#pragma once
#include "Memory.h"
#include "Types.h"
#include "Handle.h"
#include <algorithm>  

namespace sip
{
	// manager for generic type.
	template<typename T>
	class MemoryManager
	{
		std::vector<Handle<T>> m_handles;
		Memory<T>* m_memory;

	public:
		MemoryManager(Memory<T>* memory) : m_memory(memory)
		{
			m_handles.reserve(1024);
		}

		T* get(const Handle<T>& h)
		{
			if (h.isDeleted()) return nullptr;
			return &m_memory->m_buffer[h.getMemoryLocation()].m_object;
		}

		// can rescurrect a single object. 
		// object needs to rescurrect it's own components individually for this to work.
		void rescurrectObject(Handle<T>& handle)
		{
			m_memory->rescurrectObject(handle);
		}

		void deleteObject(Handle<T>& handle)
		{
			assert(!handle.isDeleted());

			handle.setDeleted(true);
			
			// find an alive handle, starting from back going to front
			auto lastAliveHandle = std::find_if(m_handles.rbegin(), m_handles.rend(), [&](const Handle<T> handle)
			{
				return !handle.isDeleted();
			});
			
			// swap deleted handle with the last alive handle, if one was found.
			// this will speed up iterating the objects through the handles, as no need to skip the dead ones.
			if (lastAliveHandle != m_handles.rend())
			{
				iter_swap(std::find(m_handles.begin(), m_handles.end(), handle), lastAliveHandle);
			}
			
			m_memory->markObjectDeleted(handle);
			handle.setMemoryLocation(-1);
		}

		Handle<T> newObject()
		{

			auto index = m_memory->allocate();

			// find free handle or create new if needed
			// the dereferenced handles aren't really deleted ones as such, though
			// only thing referencing them is the memory manager, so no one should have
			// access to it's object, unless someone took a pointer to it. Undefined behaviour.
			auto freeHandle = std::find_if(m_handles.begin(), m_handles.end(), [&] (const Handle<T>& hnd)
			{
				// ref count is <=2, if only memory manager and the object itself is referencing (object has this pointer to the handle)
				return hnd.getRefCount() <= 2; 
			});

			if (freeHandle == m_handles.end())
			{
				m_handles.push_back(Handle<T>());
				freeHandle = m_handles.end() - 1;
			}
			else
			{
				*freeHandle = Handle<T>();
			}

			freeHandle->setMemoryLocation(index);
			freeHandle->setId(m_memory->getMemId(*freeHandle));
			assert(m_memory->getMemId(*freeHandle) != 0);
			m_memory->m_buffer[freeHandle->getMemoryLocation()].m_object.this_handle = *freeHandle;

			return *freeHandle;
		}
	};
}