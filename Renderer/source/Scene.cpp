#include "LuaEngine.h"
#include "Scene.h"
#include "lauxlib.h"
#include "lua.h"
#include "lualib.h"
#include "Util.h"
using namespace sip;

namespace 
{
	Scene* _Scene;
	int loadScene(lua_State* state)
	{
		LuaTable tbl(state, true);
		auto path = tbl.getString("projectFilePath");
		_Scene->setTable(tbl);
		return 0;
	}

}

std::string Scene::getProjectFilePath()
{
	return m_table.getString("projectFilePath");
}

Scene::Scene() : 
	m_table(LuaEngine::instance().getState(), "Scene"),
	m_metaTable(LuaEngine::instance().getState(), "SceneMetaTable")
{
	lua_register(LuaEngine::instance().getState(), "loadScene", loadScene);
}

Scene::Scene(const std::string& path):
	m_table(LuaEngine::instance().getState(), "Scene"),
	m_metaTable(LuaEngine::instance().getState(), "SceneMetaTable"),
	m_path(path)
{
	_Scene = this;
	lua_register(LuaEngine::instance().getState(), "loadScene", loadScene);

	std::string footer = "local scene = {}\n";
	auto src = Util::fileAsString(path);
	src += "\n loadScene(scene)";
	src = footer + src;
	std::cout << src << "\n";

	LuaEngine::instance().executeScriptString(src.c_str());
	_Scene = nullptr;
}

void Scene::setTable(const LuaTable& table)
{
	m_table = table;
}

std::vector<LuaTable> sip::Scene::getEntityTables()
{
	std::vector<LuaTable> instances;
	auto& instanceTable = m_table.getTable("instances");
	int i = 1;
	while (instanceTable.isTable(i))
	{
		instances.push_back(instanceTable.getTable(i));
		++i;
	}
	return instances;
}

std::string& Scene::getPath()
{
	return m_path;
}

void Scene::setMetaTable(const LuaTable& table)
{
	m_metaTable = table;
}

LuaTable Scene::getMetaTable()
{
	return m_metaTable;
}
