#include <functional>

#include "Inspector.h"
#include "Director.h"
#include "Context.h"
#include "nanogui/nanogui.h"
#include "Transform.h"
#include "Entity.h"
#include "UITypes.h"
#include "Storage.h"
using namespace nanogui;
using namespace sip;
using namespace sip::editor;

namespace
{
	Handle<Entity> ptrEntity;
	Window* ptrWindow;

	VScrollPanel* ptrPanel;
	Widget* ptrCanvas;

	//floatWidget* transformX;
	//floatWidget* transformY;
	//floatWidget* transformZ;

	FloatBox<float>* pTransformXBox;
	FloatBox<float>* ptransformYBox;
	FloatBox<float>* ptransformZBox;

	TextBox* pNameBox;

	float x, y, z = 0.0f;
	Widget* pTransformCanvas;

}


Inspector::Inspector()
{
	ptrEntity = Handle<Entity>();

	ptrWindow = new nanogui::Window(Context::Instance().getScreen(), "Inspector");
	ptrWindow->setLayout(new GroupLayout());

	ptrPanel = new nanogui::VScrollPanel(ptrWindow);
	ptrPanel->setLayout(new GroupLayout());
	//ptrPanel->setFixedWidth(400);

	ptrCanvas = new nanogui::Widget(ptrPanel);

	ptrCanvas->setLayout(new BoxLayout(Orientation::Vertical, Alignment::Minimum));
	auto canvasLayout = dynamic_cast<BoxLayout*>(ptrCanvas->layout());
	canvasLayout->setMargin(2);
	canvasLayout->setSpacing(2);

	{
		auto hGroup = new Widget(ptrCanvas);
		hGroup->setLayout(new GridLayout(Orientation::Horizontal, 4, Alignment::Minimum));

		auto l = new Label(hGroup, "Name");
		l->setFontSize(20);
		l->setFixedWidth(100);

		pNameBox = new TextBox(hGroup, "<nothing selected>");
		pNameBox->setEditable(false);
		pNameBox->setFixedWidth(200);
		pNameBox->setAlignment(TextBox::Alignment::Left);

		//new Label(ptrCanvas, "----------------------------------------------------------------------------------------------------------------");
	}


	{
		pTransformCanvas = new Widget(ptrCanvas);

		pTransformCanvas->setLayout(new BoxLayout(Orientation::Horizontal, Alignment::Fill));
		auto layout = dynamic_cast<BoxLayout*>(pTransformCanvas->layout());
		layout->setSpacing(2);

		auto l = new Label(pTransformCanvas, "Transform");
		l->setFontSize(20);
		l->setFixedWidth(100);

		new Label(pTransformCanvas, "x");
		pTransformXBox = new FloatBox<float>(pTransformCanvas);
		pTransformXBox->setEditable(true);

		new Label(pTransformCanvas, "y");
		ptransformYBox = new FloatBox<float>(pTransformCanvas);
		ptransformYBox->setEditable(true);

		new Label(pTransformCanvas, "z");
		ptransformZBox = new FloatBox<float>(pTransformCanvas);
		ptransformZBox->setEditable(true);
	}
	onSelected(Handle<Entity>());
}

Inspector::~Inspector()
{
	delete ptrPanel;
	//delete ptrWindow;
}

bool Inspector::isVisible()
{
	return ptrWindow->visible();
}

void Inspector::toggleVisible()
{
	ptrWindow->setVisible(!ptrWindow->visible());
}

void Inspector::setVisible(bool visible)
{
	ptrWindow->setVisible(visible);
}

void Inspector::onSelected(Handle<Entity> hEntity)
{
	ptrEntity = hEntity;
	if (ptrEntity.isDeleted())
	{
		pNameBox->setValue(" < Nothing Selected > ");
		pTransformCanvas->setVisible(false);

		return;
	}
	auto entity = entityPtr(hEntity);

	pTransformCanvas->setVisible(true);
	if(entity->getTransform().isDeleted()) return;

	auto transform = Storage<Transform>::convert(entity->getTransform());


	pTransformXBox->setValue(transform->getLocalPosition().x);
	ptransformYBox->setValue(transform->getLocalPosition().y);
	ptransformZBox->setValue(transform->getLocalPosition().z);

	pNameBox->setValue(entity->getName());

	pTransformXBox->setCallback([=](float f) 
	{
		transform->getLocalTransform()[3][0] = f;
	});

	pTransformXBox->setCallback([=](float f)
	{
		transform->getLocalTransform()[3][1] = f;
	});

	pTransformXBox->setCallback([=](float f)
	{
		transform->getLocalTransform()[3][2] = f;
	});

	Context::Instance().getScreen()->performLayout();
}

Handle<Entity> Inspector::getSelection()
{
	return ptrEntity;
}

