#pragma once
#include "Handle.h"

namespace sip
{

	class Entity;

	namespace editor
	{

		class Command
		{
		public:
			virtual void execute() = 0;
			virtual void executeUndo() = 0;
			virtual bool append(Command* other) = 0;
		};

		class DeleteEntityCommand : public Command
		{
		private:
			Handle<Entity> m_entity;

		public:

			DeleteEntityCommand(Handle<Entity> entity);
			void execute() override;
			void executeUndo() override;
			bool append(Command* other) { return false;}
		};

		class CreateEntityCommand : public Command 
		{
			Handle<Entity> m_entity;
			std::string m_pathToPrefab;
		public:

			CreateEntityCommand(const std::string& pathToPrefab);
			void execute() override;
			void executeUndo () override;
			bool append(Command* other) { return false; }
			Handle<Entity> getCreatedEntity() { return m_entity; }
		};

		class RotateCommand : public Command 
		{
			glm::fquat m_rotateFrom;
			glm::fquat m_rotateBy;
			Handle<Entity> m_entity;
		public:
			RotateCommand(Handle<Entity> entity, const glm::quat& rotateBy);

			void execute() override;
			void executeUndo() override;
			bool append(Command* other) override;
		};

		class ScaleCommand : public Command
		{
			glm::vec3 m_scaleFrom;
			glm::vec3 m_scaleBy;
			Handle<Entity> m_entity;
		public:
			ScaleCommand(Handle<Entity> entity, glm::vec3 scaleBy);

			void execute() override;
			void executeUndo() override;
			bool append(Command* other) override;
		};

		class MoveCommand : public Command
		{
			glm::vec3 m_moveBy;
			glm::vec3 m_moveFrom;
			Handle<Entity> m_entity;

		public:
			MoveCommand( Handle<Entity> entity, const glm::vec3& moveTo);
			void execute() override;
			void executeUndo() override;
			bool append(Command* other) override;
		};

	}
}
