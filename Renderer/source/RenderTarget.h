#pragma  once
namespace sip
{

	class RenderTarget
	{
		friend class TextureManager;
	public:
		RenderTarget() : m_fboHandle(0), m_texHandle(0), m_rboHandle(0), m_hasTexHandle(false), m_hasRBOandle(false) {};
		unsigned int m_fboHandle;
		unsigned int m_texHandle;
		unsigned int m_rboHandle;
		bool m_hasTexHandle;
		bool m_hasRBOandle;

		void bind();
	};
}