#pragma once

#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"

#include <tuple>
#include "LuaStateHolder.h"
#include "LuaFunctionInvoke.h"

namespace sip
{
	class LuaEngine 
	{
	private:

		LuaEngine();
		~LuaEngine();

		LuaEngine(const LuaEngine& other);
		void operator=(const LuaEngine& other);

	public:
		static LuaEngine& instance();
		inline lua_State* getState() { return LuaStateHolder::Instance().getState(); }
		void addSearchPath(const char* path);
		void excecuteScriptFile(const char* fullPath);
		void executeScriptString(const char* script);

		/*
		*
		*  Function calls to lua with variadic arguments list. Usage:
		*  float value = LuaEngine::instance().CallGlobalFunction <float> ("Add", 1, 2.0);
		*															  V		   V
		*														  return type  function name and args
		**/
		template <typename ReturnValueType, typename... Args>
		ReturnValueType CallGlobalFunction(const char* funcName, Args...args)
		{

			lua_getglobal(LuaEngine::instance().getState(), funcName);
			const int argsCount = sizeof ...(Args);

			std::tuple <Args...> tuple = std::make_tuple(args...);

			PushValue<argsCount - 1, typename Types<argsCount - 1, Args...>::type >::push(tuple);

			// function parameters are in backwards order in the lua stack, it needs to be reversed.
			LuaStackReverse <argsCount>::reverse();

			if (lua_pcall(LuaEngine::instance().getState(), argsCount, 1, 0) != 0) {
				Debug::log("ERROR! %s", lua_tostring(LuaEngine::instance().getState(), -1));
			}

			ReturnValueType retVal = ReturnValue <ReturnValueType>::get();
			lua_pop(LuaEngine::instance().getState(), 1);
			return retVal;
		}

		template < typename...Args>
		void CallGlobalFunction(const char* funcName, Args...args)
		{

			lua_getglobal(LuaEngine::instance().getState(), funcName);
			const int argsCount = sizeof ...(Args);

			std::tuple <Args...> tuple = std::make_tuple(args...);

			PushValue<argsCount - 1, typename Types<argsCount - 1, Args...>::type >::push(tuple);

			// function parameters are in backwards order in the lua stack, it needs to be reversed.
			LuaStackReverse <argsCount>::reverse();

			if (lua_pcall(LuaEngine::instance().getState(), argsCount, 1, 0) != 0) {
				Debug::log("ERROR! %s", lua_tostring(LuaEngine::instance().getState(), -1));
			}
		}


		/*
		*   no-args function call with template return type
		*/
		template <typename ReturnValueType>
		ReturnValueType CallGlobalFunction(const char* funcName)
		{
			lua_getglobal(LuaEngine::instance().getState(), funcName);

			if (lua_pcall(LuaEngine::instance().getState(), 0, 1, 0) != 0) {
				Debug::log("ERROR! %s", lua_tostring(LuaEngine::instance().getState(), -1));
			}

			ReturnValueType retVal = ReturnValue <ReturnValueType>::get();
			lua_pop(LuaEngine::instance().getState(), 1);
			return retVal;
		}



	};


	/*
	 *   no-args function call with void return type
	 */
	template <>
	void LuaEngine::CallGlobalFunction <void>(const char* funcName);
}
