#pragma once

#include <string>
#include <sstream>
#include <iostream>
#include "Util.h"
#include "glm/vec3.hpp"
#include <windows.h>

namespace sip
{
	class Debug
	{
	public:

		static void checkGLError(const std::string& message)
		{
			Util::checkGLError(message);
		}

		static void loghline()
		{
			std::cout << "--------------------------" << std::endl;
		}

		static void log(const glm::vec3& v)
		{
			std::cout << v.x << " " << v.y << " " << v.z << std::endl;
		}



		template<typename F, typename...Args>
		static void log(F f, Args...args)
		{
			std::cout << f << " ";
			log(args...);
		}

		template <typename F>
		static void log(F f)
		{
			std::cout << f << std::endl;
		}
		/*
		static void logFail(const std::string& str)
		{
			std::cout << str.c_str() << std::endl;
			MessageBoxA(nullptr, str.c_str(), NULL, MB_ICONERROR | MB_OK);
			assert(false);
		}
		*/
		
		template<typename F, typename...Args>
		static void logFail(F f, Args...args)
		{
			if (f) return;
			_logFail(args...);
		}


	private:
		template<typename F, typename...Args>
		static void _logFail(F f, Args...args)
		{
			std::cout << f << " ";
			_logFail(args...);
		}
		template <typename F>
		static void _logFail(F f)
		{
			std::stringstream ss;
			ss << f;
			MessageBoxA(nullptr, ss.str().c_str(), NULL, MB_ICONERROR | MB_OK);
			assert(false);
		}


	};

	typedef Debug debug;
}
