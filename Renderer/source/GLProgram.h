#pragma once

#include "GL\glew.h"
#include <vector>
#include "Shader.h"

namespace sip
{
	class Texture;
	class Material;
	class RenderTarget;
	class CubeMap;

	class GLProgram
	{
		Shader m_shader;
		std::string m_name;
		unsigned int m_id;
	public:

		GLProgram();
		GLProgram(std::string);

		inline GLuint getHandle() { return m_shader.Handle; }
		inline void use() { glUseProgram(m_shader.Handle); }
		void use(const Material * const material);
		std::string getName() { return m_name; }
		const Shader& getShader() const { return m_shader; }

		void reloadShader();
		void setUniform(const char* uniform, const glm::mat4& mat);
		void setUniform(const char* uniform, const glm::vec3& vec);
		void setUniform(const char* uniform, const glm::vec2& vec);
		void setUniform(const char* uniform, float value);
		void setUniform(const char* uniform, int value);
		void setUniform(const char* uniform, std::vector<glm::mat4x4>& matrices);

		void useTexture(int index, const Texture * const texture);
		void useNormalMap(int index, const  Texture * const texture);
		void useDepthTexture(const RenderTarget* target);

		void useCubeMap(const CubeMap* cubeMap);

		unsigned int getId() const { return m_id; }

		GLint getTextureLocation(int index) ;
		GLint getNormalMapLocation(int index) ;

		void deleteProgram();
		~GLProgram();
	};
}

