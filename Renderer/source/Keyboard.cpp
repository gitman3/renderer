
#include "Keyboard.h"
#include <windows.h>
#include "EditorGUI.h"

std::map<int, short> Keyboard::sm_keyStates;

namespace 
{
	int KeyHoldCounter = 0;

	short getState(Keyboard::KEY key)
	{
		return ((unsigned short)GetAsyncKeyState(key)) >> 15;
	}
}

bool Keyboard::isKeyDown(Keyboard::KEY key)
{

	if (!sip::editor::EditorGUI::WindowHasFocus) return false;
	//SHORT prevState = sm_keyStates[key];
	//sm_keyStates[key] = state;
	return sm_keyStates[key] == 1;
	//return prevState != 1 && state == 1;
}

bool Keyboard::isKeyHold(Keyboard::KEY key)
{
	if (!sip::editor::EditorGUI::WindowHasFocus) return false;
	return sm_keyStates[key] == 2;
	/*
	SHORT state = ((unsigned short)GetAsyncKeyState(key)) >> 15;
	SHORT prevState = sm_keyStates[key];
	sm_keyStates[key] = state;

	return prevState == 1 && state == 1;
	*/
}

void Keyboard::update()
{
	for (auto& kp : sm_keyStates)
	{
		auto prevState = sm_keyStates[kp.first];
		auto curState = getState(kp.first);
		if (prevState == 0 && curState == 1) sm_keyStates[kp.first] = 1;
		else if (prevState == 1 && curState == 1) sm_keyStates[kp.first] = 2;
		else if (prevState == 2 && curState == 1) sm_keyStates[kp.first] = 2;
		else if (curState == 0) sm_keyStates[kp.first] = 0;

	}
}

bool Keyboard::isAnyKeyHoldOrDown()
{
	for (auto& kp : sm_keyStates)
	{
		if (isKeyDown(kp.first)) return true;
		if (isKeyHold(kp.first)) return true;
	}
	return false;
}

