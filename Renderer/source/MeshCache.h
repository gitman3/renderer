#pragma once
#include <map>
#include <memory>

namespace sip
{
	class Mesh;

	class MeshCache
	{
		std::map<std::string, std::unique_ptr<Mesh>> m_cache;

	public:
		static MeshCache& Instance()
		{
			static MeshCache Instance;
			return Instance;
		}

		Mesh* getMesh(const std::string& pathToModel);
	};
};