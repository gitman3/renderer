#ifndef LUA_FUNCTION_INVOKE
#define LUA_FUNCTION_INVOKE

#include "LuaStateHolder.h"
#include <GL\glew.h>

namespace sip
{


	/////// Lua stack reverse ///////
	template <int I>
	struct LuaStackReverse
	{
		static void reverse()
		{
			lua_insert(LuaStateHolder::Instance().getState(), -I);
			LuaStackReverse <I - 1>::reverse();
		}
	};

	template <>
	struct LuaStackReverse<0>
	{
		static void reverse() {}
	};


	//// Return Value Type /////
	template <typename ReturnValueType>
	struct ReturnValue
	{
		static ReturnValueType get() {}
	};

	template <>
	struct ReturnValue <float>
	{
		static float get()
		{
			return static_cast<float>(lua_tonumber(LuaStateHolder::Instance().getState(), -1));
		}
	};

	template <>
	struct ReturnValue <const char*>
	{
		static const char* get()
		{
			return lua_tostring(LuaStateHolder::Instance().getState(), -1);
		}
	};

	//////// Types ////// 
	template <int index, typename T, typename...args>
	struct Types
	{
		typedef typename Types <index - 1, args...>::type type;
	};

	template <typename T, typename...Args>
	struct Types <0, T, Args...>
	{
		typedef T type;
	};

	//////// Push Value Functions ////// 


	// Generic
	template <int index, typename T>
	struct PushValue
	{
		template<typename...Args>
		static void push(std::tuple<Args...> tuple)
		{}
	};

	// Float values
	template <int index>
	struct PushValue <index, float>
	{
		template<typename...Args>
		static void push(std::tuple<Args...> tuple)
		{
			lua_pushnumber(LuaStateHolder::Instance().getState(), std::get<index>(tuple));
			PushValue <index - 1, typename Types<index - 1, Args...>::type >::push(tuple);
		}
	};

	template <>
	struct PushValue <0, float>
	{
		template<typename...Args>
		static void push(std::tuple<Args...> tuple)
		{
			lua_pushnumber(LuaStateHolder::Instance().getState(), std::get<0>(tuple));
		}
	};

	// Double values
	template <int index>
	struct PushValue <index, double>
	{
		template<typename...Args>
		static void push(std::tuple<Args...> tuple)
		{
			lua_pushnumber(LuaStateHolder::Instance().getState(), std::get<index>(tuple));
			PushValue <index - 1, typename Types<index - 1, Args...>::type >::push(tuple);
		}
	};



	template <>
	struct PushValue <0, double>
	{
		template<typename...Args>
		static void push(std::tuple<Args...> tuple)
		{
			lua_pushnumber(LuaStateHolder::Instance().getState(), std::get<0>(tuple));
		}
	};


	// int values
	template <int index>
	struct PushValue <index, int>
	{
		template<typename...Args>
		static void push(std::tuple<Args...> tuple)
		{
			lua_pushinteger(LuaStateHolder::Instance().getState(), std::get<index>(tuple));
			PushValue <index - 1, typename Types<index - 1, Args...>::type >::push(tuple);
		}
	};



	template <>
	struct PushValue <0, int>
	{
		template<typename...Args>
		static void push(std::tuple<Args...> tuple)
		{
			lua_pushinteger(LuaStateHolder::Instance().getState(), std::get<0>(tuple));
		}
	};


	// GLubyte (unsigned char) values
	template <int index>
	struct PushValue <index, GLubyte>
	{
		template<typename...Args>
		static void push(std::tuple<Args...> tuple)
		{
			lua_pushinteger(LuaStateHolder::Instance().getState(), (int)std::get<index>(tuple));
			PushValue <index - 1, typename Types<index - 1, Args...>::type >::push(tuple);
		}
	};


	template <>
	struct PushValue <0, GLubyte>
	{
		template<typename...Args>
		static void push(std::tuple<Args...> tuple)
		{
			lua_pushinteger(LuaStateHolder::Instance().getState(), (int)std::get<0>(tuple));
		}
	};

	// Const Char values
	template <int index>
	struct PushValue <index, const char*>
	{
		template<typename...Args>
		static void push(std::tuple<Args...> tuple)
		{
			lua_pushstring(LuaStateHolder::Instance().getState(), std::get<index>(tuple));
			PushValue <index - 1, typename Types<index - 1, Args...>::type >::push(tuple);
		}
	};

	template <>
	struct PushValue <0, const char*>
	{
		template<typename...Args>
		static void push(std::tuple<Args...> tuple)
		{
			lua_pushstring(LuaStateHolder::Instance().getState(), std::get<0>(tuple));
		}
	};

}

#endif