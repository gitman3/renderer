#include "Camera.h"
#include "Entity.h"
#include "Debug.h"
#include "Context.h"
#include "MeshEntry.h"
#include "Director.h"
#include "EditorGUI.h"

using namespace sip;

sip::Camera::Camera() :
	m_fov(3.14159f * 0.25f),
	m_near(1.0f),
	m_far(2000.0f),
	m_aspect(1.0f),
	m_cameraMatrix(1.0f),
	m_worldPosition(0.0f, 0.0f, 0.0f),
	m_target(glm::vec3(0.0f, 0.0f, -1.0f)),
	m_lookaroundx(0.0f),
	m_lookaroundy(0.0f)
{
	auto sz = Director::Instance().getContext()->getScreenSize();
	m_aspect = (float)sz.x / sz.y;
	m_projectionMatrix = glm::perspective(m_fov, m_aspect, m_near, m_far);
}

const glm::mat4& Camera::getViewProjectionMatrix()
{
	m_viewProjectionMatrix = m_projectionMatrix * m_cameraMatrix;
	return m_viewProjectionMatrix;
}


RayTracing::Ray Camera::screenToWorldRay(const glm::vec2& screenCoord)
{
	glm::vec4 m = glm::vec4((float)screenCoord.x, (float)screenCoord.y, 0.0f, 1.0f);
	m.x /= (float)Context::Instance().getScreenSize().x;
	m.y /= (float)Context::Instance().getScreenSize().y;
	m += glm::vec4(-0.5f, -0.5f, 0.0f, 0.0f);
	m.x *= 2.0f; m.y *= -2.0f;

	
	m = glm::inverse(m_projectionMatrix) * m;
	m.z = -1.0f; m.w = 0.0f;

	m = glm::inverse(m_cameraMatrix) * m;

	RayTracing::Ray r;
	r.origin = getPosition();
	r.direction = glm::normalize(glm::vec3(m.x, m.y, m.z));
	return r;
}

bool Camera::cull(MeshEntry* e, const glm::mat4& model, const glm::vec3& scale)
{
	float fov2 = m_fov * 0.5f;
	float pi = 3.14159f;

	// fov / 2 + pi, because we are calculating the left plane and 0 radians points to right
	float rx = cos(fov2 + pi);
	float rz = sin(fov2 + pi);

	float lx = -rx;
	float lz = rz;

	// scale direction vectors for different aspect ratios
	glm::vec4 rpdir((1.0f / m_aspect) * rx, 0.0f, rz, 0.0f);
	glm::vec4 lpdir((1.0f / m_aspect) * lx, 0.0f, lz, 0.0f);

	///////////// 


	glm::vec4 tpdir(0.0f, rx, rz, 0.0f);
	glm::vec4 bpdir(0.0f, lx, lz, 0.0f);
	
	glm::vec4 mvpos = m_cameraMatrix * model * glm::vec4(e->m_origin.x, e->m_origin.y, e->m_origin.z, 1.0f);

	float rd = glm::dot(glm::vec3(rpdir), glm::vec3(-mvpos));
	float ld = glm::dot(glm::vec3(lpdir), glm::vec3(-mvpos));

	float td = glm::dot(glm::vec3(tpdir), glm::vec3(-mvpos));
	float bd = glm::dot(glm::vec3(bpdir), glm::vec3(-mvpos));

	float scl = std::max(scale.x, scale.y);
	scl = std::max(scale.z, scl);
	float rad = scl * e->m_boundingSphereRadius;

	bool clipFar = -mvpos.z > m_far + rad;
	bool clipNear = -mvpos.z < m_near - rad;

	bool cull = rd > rad || ld > rad || td > rad || bd > rad || clipFar || clipNear;
	return cull;
}

void Camera::lookAt(const glm::vec3& from, const glm::vec3& to)
{
	m_cameraMatrix = glm::lookAt(from, to, glm::vec3(0.0f, 1.0f, 0.0f));
	m_target = to;
}

void Camera::translateGlobal(const glm::vec3& vec)
{
	m_worldPosition += vec;
	lookAt(m_worldPosition, m_target + vec);
}

void Camera::translateLocal(const glm::vec3& vec)
{
	glm::mat3 rot;
	rot[0] = glm::vec3(m_cameraMatrix[0]);
	rot[1] = glm::vec3(m_cameraMatrix[1]);
	rot[2] = glm::vec3(m_cameraMatrix[2]);

	glm::vec3 offset = glm::inverse(rot) * vec;
	m_worldPosition += offset;
	lookAt(m_worldPosition, m_target + offset);
}

void Camera::lookAround(float x, float y)
{

	m_lookaroundx -= x;
	m_lookaroundy -= y;

	glm::fquat r;
	r = glm::rotate(r, m_lookaroundx, glm::vec3(0.0f, 1.0f, 0.0f));
	r = glm::rotate(r, m_lookaroundy, glm::vec3(1.0f, 0.0f, 0.0f));

	m_target = r * (glm::vec3(0.0f, 0.0f, -1.0f));
	lookAt(m_worldPosition, m_worldPosition + m_target);

}

