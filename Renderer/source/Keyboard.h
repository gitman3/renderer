#pragma once

#include <vector>
#include <map>

class Keyboard
{
public:
	typedef char KEY;

	enum VKEY : char
	{
		LBUTTON = 0x01,
		RBUTTON = 0x02,
		RETURN = 0x0D,
		TAB = 0x9,
		SHIFT = 0x10,
		CONTROL = 0x11,
		LEFT = 0x25,
		UP = 0x26,
		RIFHT = 0x27,
		DOWN = 0x28,
		DEL = 0x2E,
	};


	static bool isKeyDown	(KEY key);
	static bool isKeyHold	(KEY key);
	static void update();
	static bool isAnyKeyHoldOrDown();

private:

	static std::map<int, short> sm_keyStates;
};


