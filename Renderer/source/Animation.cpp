#include "Animation.h"
#include "Debug.h"
#include "assimp/matrix4x4.h"
#include "Mesh.h"
#include "MeshEntry.h"
#include "assimp/anim.h"
using namespace sip;

namespace
{
	template<typename T>
	T getAlpha(T begin, T end, T t)
	{
		return (t - begin) / (end - begin);
	}
}

glm::mat4 Animation::getOffsetMatrix()
{
	return m_matrix * m_bindPoseMatrixInverse;
	//return glm::mat4();
}

void Animation::initBindPoseMatrix()
{
	// find the bone with default pose
	debug::loghline();
	debug::log("looking for ", m_aiAnim->mNodeName.C_Str());


	auto& entries = m_mesh->getMeshEntries();
	for (auto entry : entries)
	{
		for (unsigned int i = 0; i < entry->m_aiMesh->mNumBones; ++i)
		{
			debug::log(": ", entry->m_aiMesh->mBones[i]->mName.C_Str());
			if (strcmp(entry->m_aiMesh->mBones[i]->mName.C_Str(), m_aiAnim->mNodeName.C_Str()) == 0)
			{
				
				aiMatrix4x4 mat = entry->m_aiMesh->mBones[i]->mOffsetMatrix; // TODO: should transpose or not?
				//mat = mat.Transpose();
				m_bindPoseMatrixInverse[0] = glm::vec4(mat.a1, mat.a2, mat.a3, mat.a4);
				m_bindPoseMatrixInverse[1] = glm::vec4(mat.b1, mat.b2, mat.b3, mat.b4);
				m_bindPoseMatrixInverse[2] = glm::vec4(mat.c1, mat.c2, mat.c3, mat.c4);
				m_bindPoseMatrixInverse[3] = glm::vec4(mat.d1, mat.d2, mat.d3, mat.d4);
				glm::inverse(m_bindPoseMatrixInverse);
				return;
			}


		}
	}



	//EngineDebug::logFail("Could not find bind pose matrix for animation");
}

Animation::Animation(aiNodeAnim* anim, Mesh* mesh) :
	m_elapsed(0.0f),
	m_aiAnim(anim),
	m_mesh(mesh),
	m_running(false)
{
	initBindPoseMatrix();
}

void Animation::update(float dt)
{
	//if (!m_running) return;
	m_elapsed += dt;
	if (m_elapsed > 2.0f) m_elapsed = 0.0f;
	//auto boneName = m_aiAnim->mNodeName.C_Str();
	m_matrix = glm::mat4(); // identity

	// Scales
	for (unsigned int i = 1; i < m_aiAnim->mNumScalingKeys; ++i)
	{
		auto scaleKeyStart = m_aiAnim->mScalingKeys[i - 1];
		auto scaleKeyEnd = m_aiAnim->mScalingKeys[i];
		if (m_elapsed >= scaleKeyStart.mTime && m_elapsed < scaleKeyEnd.mTime)
		{
			glm::vec3 start(scaleKeyStart.mValue.x, scaleKeyStart.mValue.y, scaleKeyStart.mValue.z);
			glm::vec3 end(scaleKeyEnd.mValue.x, scaleKeyEnd.mValue.y, scaleKeyEnd.mValue.z);
			auto result = glm::mix(start, end, getAlpha<double>(scaleKeyStart.mTime, scaleKeyEnd.mTime, m_elapsed));
			m_matrix[0][0] = result.x;
			m_matrix[1][1] = result.y;
			m_matrix[2][2] = result.z;
			break;
		}
	}

	// Rotations
	for (unsigned int i = 1; i < m_aiAnim->mNumRotationKeys; ++i)
	{
		auto rotKeyStart = m_aiAnim->mRotationKeys[i - 1];
		auto rotKeyEnd = m_aiAnim->mRotationKeys[i];

		if (m_elapsed >= rotKeyStart.mTime && m_elapsed < rotKeyEnd.mTime)
		{
			auto qStart = rotKeyStart.mValue;
			auto qEnd = rotKeyEnd.mValue;
			glm::quat qs(qStart.x, qStart.y, qStart.z, qStart.w);
			glm::quat qe(qEnd.x, qEnd.y, qEnd.z, qEnd.w);
			float a = (float)getAlpha<double>(rotKeyStart.mTime, rotKeyEnd.mTime, m_elapsed);
			auto result = glm::mat4_cast(glm::slerp(qs, qe, (float)getAlpha<double>(rotKeyStart.mTime, rotKeyEnd.mTime, m_elapsed)));
			m_matrix = m_matrix * result;
			break;
		}
	}

	// Positions
	for (unsigned int i = 1; i < m_aiAnim->mNumPositionKeys; ++i)
	{
		auto posKeyStart = m_aiAnim->mPositionKeys[i - 1];
		auto posKeyEnd = m_aiAnim->mPositionKeys[i];

		if (m_elapsed >= posKeyStart.mTime && m_elapsed < posKeyEnd.mTime)
		{
			glm::vec3 startPos(posKeyStart.mValue.x, posKeyStart.mValue.y, posKeyStart.mValue.z);
			glm::vec3 endPos(posKeyEnd.mValue.x, posKeyEnd.mValue.y, posKeyEnd.mValue.z);
			auto result = glm::mix(startPos, endPos, getAlpha<double>(posKeyStart.mTime, posKeyEnd.mTime, m_elapsed));
			m_matrix[3][0] = result.x;
			m_matrix[3][1] = result.y;
			m_matrix[3][2] = result.z;
			break;
		}
	}
}