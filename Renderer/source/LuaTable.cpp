#include "LuaTable.h"
#include "LuaUtils.h"

#include <assert.h>
#include <string>
#include "Debug.h"

using namespace sip;

/// Lua Stack Restore /// 
LuaStackRestore::LuaStackRestore() {}

LuaStackRestore::LuaStackRestore(lua_State* state) : m_stackPointer(0), m_state(state) {
	init(state);
}

LuaStackRestore::~LuaStackRestore() {
	lua_settop(m_state, m_stackPointer);
};

void LuaStackRestore::init(lua_State* state) {
	m_state = state;
	m_stackPointer = lua_gettop(state);
};


//// 

void LuaTable::pushStr(const char* value) const
{
	lua_rawgeti(m_state, LUA_REGISTRYINDEX, m_ref);
	assert(lua_istable(m_state, -1));
	lua_pushstring(m_state, value);
	lua_gettable(m_state, -2);
}

void LuaTable::pushNumeric(int index) const
{
	lua_rawgeti(m_state, LUA_REGISTRYINDEX, m_ref);
	assert(lua_istable(m_state, -1));
	lua_pushnumber(m_state, index);
	lua_gettable(m_state, -2);
}


bool LuaTable::isTable(const char* value) const {
	LuaStackRestore lsr(m_state);
	pushStr(value);
	return lua_istable(m_state, -1);
}

bool LuaTable::isBoolean(const char* value) const {
	LuaStackRestore lsr(m_state);
	pushStr(value);
	return lua_isboolean(m_state, -1);
}

bool LuaTable::isString(const char* value) const {
	LuaStackRestore lsr(m_state);
	pushStr(value);
	return lua_isstring(m_state, -1);
}

bool LuaTable::isFloat(const char* value) const{
	LuaStackRestore lsr(m_state);
	pushStr(value);
	return lua_isnumber(m_state, -1);
}



bool LuaTable::isTable(int index) const {
	LuaStackRestore lsr(m_state);
	pushNumeric(index);	
	return lua_istable(m_state, -1);
}

bool LuaTable::isString(int index) const {
	LuaStackRestore lsr(m_state);
	pushNumeric(index);
	return lua_isstring(m_state, -1);
}

bool LuaTable::isFloat(int index) const{
	LuaStackRestore lsr(m_state);
	pushNumeric(index);
	return lua_isnumber(m_state, -1);
}

bool LuaTable::isBoolean(int index) const{
	LuaStackRestore lsr(m_state);
	pushNumeric(index);
	return lua_isboolean(m_state, -1);
}

LuaTable LuaTable::getTable(const char* name) const 
{
	LuaStackRestore lsr(m_state);
	pushStr(name);
	assert(lua_istable(m_state, -1));
	LuaTable t(m_state, true);
	return t;
}

LuaTable LuaTable::getTable(int index) const{
	LuaStackRestore lsr(m_state);
	pushNumeric(index);
	assert(lua_istable(m_state, -1));
	LuaTable t(m_state, true);
	return t;
}

std::string LuaTable::getString(const char* value) const
{
	LuaStackRestore lsr(m_state);
	pushStr(value);
	assert(lua_isstring(m_state, -1));
	return std::string(lua_tostring(m_state, -1));
}

bool LuaTable::getBoolean(const char* value) const{
	if (!isBoolean(value)) return false;
	
	LuaStackRestore lsr(m_state);
	pushStr(value);
	assert(lua_isboolean(m_state, -1));
	return lua_toboolean(m_state, -1);
}

bool LuaTable::getBoolean(int index) const{
	if (!isBoolean(index)) return false;
	LuaStackRestore lsr(m_state);
	pushNumeric(index);
	assert(lua_isboolean(m_state, -1));
	return lua_toboolean(m_state, -1);
}

std::string LuaTable::getString(int index) const{
	LuaStackRestore lsr(m_state);
	pushNumeric(index);
	assert(lua_isstring(m_state, -1));
	return lua_tostring(m_state, -1);
}

float LuaTable::getFloat(const char* value, float defaultValue) const {

	if (!isFloat(value) && defaultValue != FLT_MAX) 
	{
		return defaultValue;
	}

	LuaStackRestore lsr(m_state);
	pushStr(value);
	assert(lua_isnumber(m_state, -1));
	return static_cast<float>(lua_tonumber(m_state, -1));
}

float LuaTable::getFloat(int index) const
{
	if (!isFloat(index)) return FLT_MAX;
	LuaStackRestore lsr(m_state);
	pushNumeric(index);
	assert(lua_isnumber(m_state, -1));
	return static_cast<float>(lua_tonumber(m_state, -1));
}



void LuaTable::setFloat(const char* name, int value)
{
	setFloat(name, (float)value);
}

void LuaTable::setFloat(const char* name, float value) 
{
 	LuaStackRestore lsr(m_state);

	lua_rawgeti(m_state, LUA_REGISTRYINDEX, m_ref);
	lua_pushstring(m_state, name);
	lua_pushnumber(m_state, value);
	lua_settable(m_state, -3);
}

void LuaTable::setString(const char* name, const char* value)
{
	LuaStackRestore lsr(m_state);

	lua_rawgeti(m_state, LUA_REGISTRYINDEX, m_ref);
	lua_pushstring(m_state, name);
	lua_pushstring(m_state, value);
	lua_settable(m_state, -3);
}

void LuaTable::setBool(const char* name, const bool value)
{
	LuaStackRestore lsr(m_state);

	lua_rawgeti(m_state, LUA_REGISTRYINDEX, m_ref);
	lua_pushstring(m_state, name);
	lua_pushboolean(m_state, value);
	lua_settable(m_state, -3);
}

// In Lua 5.0 reference manual is a table traversal example at page 29.
void LuaTable::printTable() const
{
//	LuaUtils::stackdump(m_state);
	
	//lua_gettable(m_state, m_stackIndex);

    lua_pushnil(m_state);
	
    while(lua_next(m_state, -2) != 0)
    {
		if (lua_isstring(m_state, -1))
		{
			Debug::log( lua_tostring(m_state, -2), " = ", lua_tostring(m_state, -1));
		}
		else if (lua_isnumber(m_state, -1))
		{
			Debug::log( lua_tostring(m_state, -2), " = ", lua_tonumber(m_state, -1));
		}
		else if (lua_istable(m_state, -1))
		{
			printTable();
		}
        lua_pop(m_state, 1);
    }
	
}
