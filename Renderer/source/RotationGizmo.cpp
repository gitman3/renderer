#include "RotationGizmo.h"
#include "MeshCache.h"
#include "Storage.h"
#include "Director.h"
#include "World.h"
#include "Mesh.h"
#include "MeshEntry.h"
#include "Entity.h"
#include "Material.h"
#include "ShaderCache.h"
#include "World.h"
#include "Mouse.h"
#include "Util.h"
#include "Debug.h"
#include "Command.h"
#include "Editor.h"
#include "Camera.h"

using namespace sip;
using namespace sip::editor;

RotationGizmo::RotationGizmo() : 
	m_rotateAxis(NONE),
	m_prevHitPos()
{
	m_entity = Director::Instance().getWorld()->addEntity();
	auto pRotGizmo = entityPtr(m_entity);
	pRotGizmo->setMesh(MeshCache::Instance().getMesh("../Renderer/resources/rotation.dae"));
	auto mat = pRotGizmo->getMesh()->getDefaultMaterial();
	mat->setProgram(ShaderCache::Instance()->addShaderAtDefaultPath("rotationgizmo"));
	pRotGizmo->setInspectorSelectable(false);
}

void RotationGizmo::onSelected(Handle<Entity> selection)
{
	auto gizmoPtr = entityPtr(m_entity);

	if (!selection.isDeleted() && m_selection.isDeleted())
	{
		m_selection = selection;
	}

	if (m_selection.isDeleted())
	{
		m_rotateAxis = NONE;
		gizmoPtr->setVisible(false);
		m_prevHitPos == glm::vec3();
		return;
	}
		
	gizmoPtr->setVisible(true);
	auto selectionEPtr = entityPtr(m_selection);
	auto selectionTPtr = transformPtr(selectionEPtr->getTransform());

	auto gizmoTptr = transformPtr(gizmoPtr->getTransform());
	const auto& localT = selectionTPtr->getLocalTransform();
	gizmoTptr->setLocalPosition(selectionTPtr->getLocalPosition());

	auto tr = selectionTPtr->getRotation();
	gizmoTptr->setRotation(selectionTPtr->getRotation());
	gizmoTptr->setScale(selectionEPtr->getBoundingSphereRadius());

	auto prog = gizmoPtr->getMesh()->getDefaultMaterial()->getProgram();
	prog->use();

	if (!Mouse::isButtonHold(Mouse::Button::Left))
	{
		prog->setUniform("u_selectedHandle", 0.0f);
		m_selection = Handle<Entity>();
		m_rotateAxis = NONE;
		m_prevHitPos == glm::vec3();
		return;
	}

	auto world = Director::Instance().getWorld();
	RayTracing::Ray ray = world->mouseToWorldRay();
	auto info = RayTracing::traceSphere(ray, gizmoTptr->getLocalPosition(), gizmoTptr->getScale().x);

	if (!info.collided() && m_selection.isDeleted()) return;

	if (m_prevHitPos == glm::vec3())
	{
		m_prevHitPos = info.pos;
		return;
	}

	glm::vec3 localPos = info.pos - selectionTPtr->getLocalPosition();
	glm::vec3 prevLocalPos = m_prevHitPos - selectionTPtr->getLocalPosition();

	auto rMat = glm::mat3_cast(gizmoTptr->getRotation());

	const float HANDLE_SIZE = 0.15f;
	float x = glm::smoothstep(HANDLE_SIZE, 0.0f, abs(glm::dot(localPos, rMat[2]))); // xz plane
	float y = glm::smoothstep(HANDLE_SIZE, 0.0f, abs(glm::dot(localPos, rMat[0]))); // yz plane
	float z = glm::smoothstep(HANDLE_SIZE, 0.0f, abs(glm::dot(localPos, rMat[1]))); // yx plane


	const float speed = 0.05f;
	auto camera = Director::Instance().getWorld()->getActiveCamera();
	auto rotm = glm::mat3_cast(selectionTPtr->getRotation());

	prog->setUniform("u_normalMatrix", glm::transpose(glm::inverse(selectionTPtr->getLocalTransform() * camera->getViewMatrix())));
	
	/*
	glm::mat3 prevRM;
	prevRM[2] = glm::normalize(prevLocalPos);
	prevRM[0] = -glm::normalize(glm::cross(prevRM[2], glm::vec3(0.0, 1.0, 0.0)));
	prevRM[1] = glm::normalize(glm::cross(prevRM[2], prevRM[0]));
	glm::quat prevRQ = glm::quat_cast(prevRM);

	glm::mat3 RM;
	prevRM[2] = glm::normalize(localPos);
	prevRM[0] = -glm::normalize(glm::cross(RM[2], glm::vec3(0.0, 1.0, 0.0)));
	prevRM[1] = glm::normalize(glm::cross(RM[2], RM[0]));
	glm::quat RQ = glm::quat_cast(RM);

	*/
	switch (m_rotateAxis)
	{
	case X: // red
	{
		prog->setUniform("u_selectedHandle", 3.0f);
		auto qt = glm::angleAxis((float)Mouse::getDeltaX() * speed, glm::vec3(0.0f, 0.0f, 1.0f));
		editor::Editor::Instance().rotateLocal(m_selection, qt);
	}
	break;
	
	case Y: // green
	{
		auto qt = glm::angleAxis((float)Mouse::getDeltaY() * speed, glm::vec3(1.0f, 0.0f, 0.0f));
		prog->setUniform("u_selectedHandle", 2.0f);
		editor::Editor::Instance().rotateLocal(m_selection, qt);
	}
	break;
	
	case Z: // blue
	{
		auto qt = glm::angleAxis((float)Mouse::getDeltaY() * speed, glm::vec3(0.0f ,1.0f, 0.0f));
		prog->setUniform("u_selectedHandle", 1.0f);
		editor::Editor::Instance().rotateLocal(m_selection, qt);
	}
	break;

	case NONE:
		prog->setUniform("u_selectedHandle", 0.0f);
		if ( x + y + z > 0.f)
		{
			if (x > y && x > z) m_rotateAxis = X;
			if (y > x && y > z) m_rotateAxis = Y;
			if (z > x && z > y) m_rotateAxis = Z;
		}
		break;
			
	}
	m_prevHitPos = info.pos;

	Util::checkGLError("");
}

