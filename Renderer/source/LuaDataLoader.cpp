#include "LuaDataLoader.h"
#include "Util.h"
#include "Debug.h"
#include "LuaEngine.h"

#include "lauxlib.h"
#include "lualib.h"
#include "lua.h"
#include "LuaTable.h"
#include "LuaUtils.h"
#include "Material.h"
#include "MaterialCache.h"
#include "ShaderCache.h"
#include "TextureManager.h"
#include "Scene.h"
#include "CubeMap.h"
using namespace sip;

namespace 
{

	std::shared_ptr<CubeMap> TargetCubeMap;
	static int callCreateCubeMapFunc(lua_State* state)
	{
		LuaTable tbl(state, true);
		TargetCubeMap.reset(new CubeMap(
			tbl.getString("pos_x"), 
			tbl.getString("neg_x"), 
			tbl.getString("pos_y"), 
			tbl.getString("neg_y"), 
			tbl.getString("pos_z"), 
			tbl.getString("neg_z")
		));

		return 0;
	}


	Scene* TargetScene;
	static int callCreateSceneMetaFunc(lua_State* state)
	{
		LuaTable tbl(state, true);
		TargetScene->setMetaTable(tbl);
		return 0;
	}


	std::shared_ptr<Material> CreatedMaterial;
	static int callCreateMaterialFunc(lua_State* state)
	{
		LuaTable tbl(state, true);
		CreatedMaterial.reset(new Material( { tbl.getString("name"), 0} ));
		CreatedMaterial->setProgram(ShaderCache::Instance()->getShader(tbl.getString("shaderPath")));

		auto textureTable = tbl.getTable("texture");
		int i = 0;
		while (textureTable.isString(++i))
		{
			std::string texName = textureTable.getString(i);
			auto texture = TextureManager::Instance().addTextureIfMissing(textureTable.getString(i));
			CreatedMaterial->addTexture(*texture);
		}
		MaterialCache::Instance().cacheMaterial({ tbl.getString("name"), 0 }, CreatedMaterial);
		return 0;
	}


	Prefab CreatedPrefab;

	// called by script when loading the file
	static int callCreatePrefabFunc(lua_State* state)
	{
		LuaTable tbl(state, true);
		CreatedPrefab = Prefab();
		CreatedPrefab.Name = tbl.getString("name");
		
		//CreatedPrefab.Material = tbl.getString("material");

		auto materialsTable = tbl.getTable("materials");
		int i = 0;
		while (materialsTable.isTable(++i))
		{
			auto entryName = materialsTable.getTable(i).getString("meshEntryName");
			auto materialName = materialsTable.getTable(i).getString("materialName");
			CreatedPrefab.MaterialMap[entryName] = materialName;
		}

		auto meshTable = tbl.getTable("mesh");
		CreatedPrefab.MeshName = meshTable.getString("name");
		CreatedPrefab.MeshFile = meshTable.getString("file");

		auto transformTable = tbl.getTable("transform");
		// positions should not be in prefabs at all
		CreatedPrefab.Position.x = 0;
		CreatedPrefab.Position.y = 0;
		CreatedPrefab.Position.z = 0;

		auto rotationTable = transformTable.getTable("rotation");
		CreatedPrefab.Rotation.x = rotationTable.getFloat("x");
		CreatedPrefab.Rotation.y = rotationTable.getFloat("y");
		CreatedPrefab.Rotation.z = rotationTable.getFloat("z");

		auto scaleTable = transformTable.getTable("scale");
		CreatedPrefab.Scale.x = scaleTable.getFloat("x");
		CreatedPrefab.Scale.y = scaleTable.getFloat("y");
		CreatedPrefab.Scale.z = scaleTable.getFloat("z");

		if (tbl.isTable("flags"))
		{
			auto flagsTable = tbl.getTable("flags");
			int i = 0;
			while (flagsTable.isString(++i))
			{
				auto flag = flagsTable.getString(i);
				if (flag == "AO_TARGET") CreatedPrefab.IsAOTarget = true;
				if (flag == "AO_OCCLUDER") CreatedPrefab.isAOOccluder = true;
			}
		}

		return 0;
	}
}

Prefab LuaDataLoader::loadPrefab(const std::string& pathToFile)
{
	auto suffix = Util::parseFilenameSuffix(pathToFile);
	debug::logFail(suffix == "prefab", pathToFile, "must end .prefab");
	std::string prefabFile = Util::fileAsString(pathToFile);
	prefabFile += "\n";
	prefabFile += "callCreatePrefabFunc(object)";
	LuaEngine::instance().executeScriptString(prefabFile.c_str());

	// the prefab is now created, grab it
	CreatedPrefab.fileName = Util::fileFromPath(pathToFile);
	CreatedPrefab.filePath = pathToFile;
	return CreatedPrefab;
}

LuaDataLoader::LuaDataLoader()
{
	lua_register(LuaEngine::instance().getState(), "callCreatePrefabFunc", callCreatePrefabFunc);
	lua_register(LuaEngine::instance().getState(), "callCreateMaterialFunc", callCreateMaterialFunc);
	lua_register(LuaEngine::instance().getState(), "callCreateCubeMapFunc", callCreateCubeMapFunc);
	lua_register(LuaEngine::instance().getState(), "callCreateSceneMetaFunc", callCreateSceneMetaFunc);
}

Material* LuaDataLoader::loadMaterial(const std::string& pathToFile)
{

	std::string prefix = "material = {}\n";
	prefix += "material.texture = {}\n";

	auto suffix = Util::parseFilenameSuffix(pathToFile);

	std::string materialFile = Util::fileAsString(pathToFile);
	materialFile += "\n";
	materialFile += "callCreateMaterialFunc(material)";

	auto fullSrc = prefix + materialFile;
	LuaEngine::instance().executeScriptString(fullSrc.c_str());
	return CreatedMaterial.get();

}

void sip::LuaDataLoader::loadSceneMeta(const std::string& pathToFile, Scene& scene)
{
	std::string prefix = "scene = {}\n";
	auto suffix = Util::parseFilenameSuffix(pathToFile);
	std::string file = Util::fileAsString(pathToFile);
	file += "\n";
	file += "callCreateSceneMetaFunc(scene)";
	auto fullSrc = prefix + file;

	TargetScene = &scene;
	LuaEngine::instance().executeScriptString(fullSrc.c_str());
	TargetScene = nullptr;
}

std::shared_ptr<CubeMap> LuaDataLoader::loadCubeMap(const std::string& pathToFile)
{
	std::string prefix = "map = {}\n";
	auto suffix = Util::parseFilenameSuffix(pathToFile);
	std::string file = Util::fileAsString(pathToFile);
	file += "\n";
	file += "callCreateCubeMapFunc(map)";
	auto fullSrc = prefix + file;

	TargetCubeMap = nullptr;
	LuaEngine::instance().executeScriptString(fullSrc.c_str());
	return TargetCubeMap;

}
