#pragma once

#include <map>
#include <string>
#include <GL\glew.h>
#include "GLProgram.h"
#include "Passkey.h"

namespace sip
{
	class World;

	class ShaderCache
	{

		std::map<std::string, GLProgram> m_shaderCache;
	public:

		static const char * const SHADERS_PATH;

		static const char * const SNAME_DEPTH;
		static const char * const SNAME_POS_COL;
		static const char * const SNAME_CUBEMAP;
		static const char * const SNAME_POS_COL_LIGHT_PROJ_NOTEXTURES;
		static const char * const SNAME_POS_COL_LIGHT_PROJ_TEXTURED;
		static const char * const SNAME_POS_COL_LIGHT_PROJ_TEXTURED_OPAQUE;
		static const char * const SNAME_COMPUTE_RAY_MESH_COLLISION;
		static const char * const SNAME_EDITOR_WIREFRAME;

		void loadDefaultShaders();

		static ShaderCache* Instance()
		{
			static ShaderCache instance;
			return &instance;
		}

		GLProgram* getShader(const std::string& path);
		GLProgram* getShaderAtDefaultPath(const std::string& path);

		void addShader(const char* path, const GLProgram& shader);
		void reloadShader(const char* path);
		GLProgram* addShaderAtDefaultPath(const char* name);

		/** Calling this alone without refreshing material cache afterwards will cause problems */
		void reloadAll(PassKey<World>);
	};
}

