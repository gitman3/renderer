#include <memory>
#include <unordered_set>

#include "Entity.h"
#include "GLProgram.h"
#include "World.h"
#include "Debug.h"
#include "Material.h"
#include "Director.h"
#include "ShaderCache.h"
#include "Camera.h"
#include "Animation.h"
#include "TextureManager.h"
#include "Mesh.h"
#include "assimp/Importer.hpp"
#include "Transform.h"
#include "Component.h"
#include "Time.h"
#include "MeshEntry.h"
#include "Prefab.h"
#include "EditorGUI.h"
#include "Handle.h"
#include "Storage.h"
#include "Transform.h"
#include "IDGenerator.h"

using namespace sip;



Entity::Entity() :
	m_mesh(nullptr),
	m_transform(),
	m_name("Entity(unnamed)"),
	m_prefab(nullptr),
	m_inspectorSelectable(true),
	m_id(getNewID<Entity>()),
	m_visible(true),
	m_material(nullptr)
{
	m_transform = Storage<Transform>::newObject();
}

void Entity::setPrefab(const Prefab& prefab)
{
	m_prefab.reset(new Prefab(prefab));
	m_name = "Instanceof(" + m_prefab->Name + ")";
}


float Entity::getBoundingSphereRadius() const
{
	auto bounds = getAABB();
	if (m_transform.isDeleted()) return 0.0f;

	auto transform = transformPtr(m_transform);
	return std::max(std::max(std::max(abs(bounds.x), abs(bounds.y)), abs(bounds.z)), abs(bounds.w)) * transform->getScale().x;
}

void Entity::setMaterial(Material* material)
{
	m_material = material;
}

glm::vec4 sip::Entity::getAABB() const
{
	glm::vec4 res = glm::vec4(0.0f);
	if (m_mesh != nullptr)
	{
		float xmin = FLT_MAX;
		float xmax = FLT_MIN;
		float ymin = FLT_MAX;
		float ymax = FLT_MIN;

		for (auto entry : m_mesh->getMeshEntries())
		{
			auto rad = entry->getBoundingSphereRadius();
			auto pos = entry->getOrigin();
			xmax = std::max(pos.x + rad, xmax);
			xmin = std::min(pos.x - rad, xmin);

			ymax = std::max(pos.y + rad, ymax);
			ymin = std::min(pos.y - rad, ymin);
		}
		res.x = xmax;
		res.y = ymax;
		res.z = xmin;
		res.w = ymin;
	}
	return res;
}

sip::Transform* sip::Entity::getTransformPtr()
{
	return transformPtr(m_transform);
}

void Entity::setVisible(bool visible)
{
	m_visible = visible;
}

bool Entity::isVisible() const
{
	return m_visible;
}

void Entity::refreshShaders()
{
	if (m_mesh == nullptr)
	{
		return;
	}


	Director::Instance().reloadMaterialConfig();

	for (auto entry : m_mesh->m_meshEntries)
	{
		auto mat = entry->getMaterial();
		mat->getProgram()->reloadShader();
		Director::Instance().refreshMaterial(mat);
	}
}



void Entity::setMaterialConf(const std::string& conf)
{
	Debug::logFail(m_mesh != nullptr, "[Entity] Mesh null! ");
	/**/
	for (auto entry : m_mesh->m_meshEntries)
	{
		entry->getMaterial()->setMaterialConfig(conf);
	}

}



void Entity::update()
{
	// should be updated by manager of some sort.

	/*
	float dt = Time::deltaTime();
	if (m_mesh != nullptr)
	{
		auto& animations = m_mesh->m_animations;
		for (unsigned int i = 0; i < animations.size(); ++i)
		{
			animations[i]->update(dt);
		}
	}

	for (auto& t : m_transform->getChildren())
	{
		t->getEntity()->update();
	}
	*/
}



void Entity::setMesh(Mesh* mesh)
{
	m_mesh = mesh;
}


Mesh* Entity::getMesh()
{
	return m_mesh;
}

Handle<Transform> Entity::getTransform()
{
	return m_transform;
}

