#pragma once
#include "Types.h"
#include "Handle.h"
#include <algorithm>
namespace sip
{
	template <typename T>
	class Memory
	{
		struct MemorySlot
		{
			T m_object;
			bool m_deleted;

			MemorySlot(const T& object) : 
				m_deleted(false), 
				m_object(object)
			{}
		};

		const uint MAX_DELETED_CACHE = 5;
		const uint MAX_OBJECTS = 4096;
		std::vector<MemorySlot> m_buffer;
		std::vector<int> m_deletedIndices;

		template <typename T> friend class MemoryManager;
		
		// memory keeps track of the deleted object positions in the memory
		// and starts replacing them when there are more dead indices than MAX_DELETED_CACHE
		void markObjectDeleted(Handle<T>& handle)
		{
			assert(handle.isDeleted());
			m_buffer[handle.getMemoryLocation()].m_deleted = true;
			m_deletedIndices.push_back(handle.getMemoryLocation());
			refreshIterators();
		}

		void rescurrectObject(Handle<T>& handle)
		{
			int location = findMemoryLocationByOjectId(handle.getId());
			handle.setDeleted(false);
			m_buffer[location].m_deleted = false;
			m_deletedIndices.erase(std::remove(m_deletedIndices.begin(), m_deletedIndices.end(), location), m_deletedIndices.end());
			handle.setMemoryLocation(location);
			refreshIterators();
		}
		
		int findMemoryLocationByOjectId(uint id)
		{
			for (uint i = 0; i < m_buffer.size(); ++i)
			{
				if (m_buffer[i].m_object._mem_id() == id)
				{
					return i;
				}
			}
			return -1;
		}
		
		int allocate()
		{
			assert(m_buffer.size() < MAX_OBJECTS);

			// if amount of deleted objects in the memory is more than MAX_DELETED_CACHE,
			// start cleaning those up by inserting new objects in their place.
			// this might mean that there are dead handles that cannot be resurrected anymore
			// as their objects are now gone.
			if (m_deletedIndices.size() > MAX_DELETED_CACHE)
			{
				int index = m_deletedIndices.back();
				m_deletedIndices.erase(m_deletedIndices.end() - 1);
				m_buffer.emplace(m_buffer.begin() + index, T());
				m_buffer.at(index) = T();
				refreshIterators();
				return index;
			}

			m_buffer.emplace_back(T());
			refreshIterators();
			return m_buffer.size() - 1;
		}

		int getMemId(const Handle<T>& handle)
		{
			return m_buffer[handle.getMemoryLocation()].m_object._mem_id();
		}


	public:
		//////////////////////////////////////////////
		/////////////////////// Iterator /////////////
		//////////////////////////////////////////////
		class Iterator
		{
			// for constructor
			template<typename T> friend class Memory; 

			typename std::vector<MemorySlot>::iterator m_iter;
			std::vector<MemorySlot>* m_buffer;

			Iterator(typename const std::vector<MemorySlot>::iterator& iter, std::vector<MemorySlot>& buffer) : m_buffer(&buffer), m_iter(iter)
			{
			}

		public:

			Iterator& operator++()
			{
				do
				{
					++m_iter;
				} while (m_iter != m_buffer->end() && m_iter->m_deleted);

				return *this;
			}

			T& get()
			{
				return m_iter->m_object;
			}

			bool operator ==(const Iterator& other)
			{
				return other.m_iter == m_iter;
			}

			bool operator !=(const Iterator& other)
			{
				return other.m_iter != m_iter;
			}
		};

		/////////////////////////////////////////////////////
		/////////////////////// End of iterator /////////////
		/////////////////////////////////////////////////////


		Iterator begin()
		{
			return m_begin;
		}

		Iterator end()
		{
			return m_end;
		}

		Memory() : 
			m_buffer(), 
			m_deletedIndices(),
			m_begin (Iterator(m_buffer.begin(), m_buffer)),
			m_end (Iterator(m_buffer.end(), m_buffer))
		{
			m_buffer.reserve(MAX_OBJECTS);
			m_deletedIndices.reserve(MAX_OBJECTS); // yes it's possible that all objects are deleted.
		}

	private:
		Iterator m_begin;
		Iterator m_end;

		void refreshIterators()
		{
			m_end.m_iter = m_buffer.end(); //  
			m_begin.m_iter = std::find_if(m_buffer.begin(), m_buffer.end(), [](const MemorySlot& slot)
			{
				return !slot.m_deleted;
			});
		}

	};
}
