#pragma once
#include "Handle.h"

namespace sip
{

	class Transform;
	class Material;
	class Entity;
	class Camera;

	class Batcher
	{
	public:
		static Batcher& Instance()
		{
			static Batcher Instance;
			return Instance;
		}

		void buildBatches();
		void render();
		void issueCustomRenderCommand(Handle<Entity> entity, Material& material);
		void issueCustomRenderCommand(Handle<Entity> entity, Material& material, Handle<Transform> transform);

	private:
		Batcher();
		void createRenderCommand(Handle<Entity> entity, Transform& t, Camera* camera, Material* material);
	};
}

/*
worldMat, batchId, entry->vao, entry->m_elementCount
*/