#include "MaterialCache.h"
#include "Material.h"
#include "Debug.h"
#include "Util.h"
#include "ShaderCache.h"
#include "TextureManager.h"
#include "EditorGUI.h"
using namespace sip;
//// MAT CACHE

std::string sip::MaterialCache::CUSTOM_MATERIALS_KEY = "CUSTOM_MATERIALS";


sip::Material* sip::MaterialCache::getCustomMaterialWithKey(CustomKeys key)
{
	return getMaterial(getCustomMaterialKey(key));
}

sip::Material* sip::MaterialCache::getWithUniqueName(const std::string& key)
{
	auto mat =  getMaterial({ key, 0 });
	assert(mat != nullptr);
	return mat;

}

Material* MaterialCache::createMaterial(const aiMaterial* aiMat, const std::string& pathToModel, int indexInScene)
{

	std::pair<std::string, int> key(pathToModel, indexInScene);
	auto m = m_materials.find(key);

	if (m != m_materials.end())
	{
		Debug::log("[MaterialCache] Material already exists ", pathToModel, " ", indexInScene);
		return m->second.get();
	}


	Material* mat = new Material({pathToModel, indexInScene});

	aiString name;
	aiMat->Get(AI_MATKEY_NAME, name);
	std::string s(name.C_Str());
	Debug::loghline();
	Debug::log(std::string("[MaterialCache] new material : " + s));

	mat->setPathToFolder({}, Util::trimPathFrom(pathToModel));


	int difTexCount = aiMat->GetTextureCount(aiTextureType_DIFFUSE);
	{
		Debug::log("[MaterialCache] Diffuse Textures: ", difTexCount);
	}

	mat->setProgram( ShaderCache::Instance()->getShader(ShaderCache::SNAME_POS_COL_LIGHT_PROJ_NOTEXTURES));

	for (int j = 0; j < difTexCount; ++j)
	{
		aiString path;
		aiMat->GetTexture(aiTextureType_DIFFUSE, j, &path);
		std::string p(path.C_Str());

		std::stringstream ss;
		ss << mat->getPathToFolder() << "/" << path.C_Str();
		auto pathToTexture = ss.str();

		Texture* texture = TextureManager::Instance().addTextureIfMissing(pathToTexture);

		if (texture != nullptr)
		{

			mat->getTextures().push_back(*texture);
			mat->setProgram(ShaderCache::Instance()->getShader(ShaderCache::SNAME_POS_COL_LIGHT_PROJ_TEXTURED));
			auto pathToNormalMap = pathToTexture.replace(pathToTexture.end() - 4, pathToTexture.end(), "_NORM.png");

			Texture* normalMap = TextureManager::Instance().addTextureIfMissing(pathToNormalMap);
			if (normalMap != nullptr)
			{
				mat->getNormalMaps().push_back(*normalMap);// TODO
				//mat->setProgram(ShaderCache::Instance()->getShader(ShaderCache::SNAME_POS_COL_LIGHT_PROJ));
			}
		}

		Debug::log("[MaterialCache] Path : " + p);

	}


	{
		aiColor3D col;
		aiMat->Get(AI_MATKEY_COLOR_DIFFUSE, col);
		mat->DiffuseColor = glm::vec3(col.r, col.g, col.b);

		Debug::log(mat->DiffuseColor);
	}

	{
		aiColor3D col;
		aiMat->Get(AI_MATKEY_COLOR_SPECULAR, col);
		mat->SpecularColor = glm::vec3(col.r, col.g, col.b);
	}

	{
		float out;
		aiGetMaterialFloat(aiMat, AI_MATKEY_SHININESS, &out);
		mat->Specular = out;
	}
	{
		float out;
		aiGetMaterialFloat(aiMat, AI_MATKEY_SHININESS_STRENGTH, &out);
	}
	
	debug::log("[", pathToModel, "] Material created with shader: [", mat->getProgram()->getName(), "]");

	m_materials[key] = std::shared_ptr<Material>(mat);
	return m_materials.find(key)->second.get();

}

void MaterialCache::cacheMaterial(MaterialKey key, std::shared_ptr<Material> mat)
{
	auto m = m_materials.find(key);

	if (m != m_materials.end())
	{
		Debug::log("[MaterialCache] Material already exists ", key.first, " ", key.second);
	}
	m_materials[key] = mat;
}

Material* MaterialCache::getMaterial(MaterialKey key)
{
	auto m = m_materials.find(key);

	if (m != m_materials.end())
	{
		return m->second.get();
	}

	//debug::logFail(false, "Material does not exist");
	return nullptr;
}

void MaterialCache::refreshMaterials()
{
	for (auto& iter = m_materials.begin(); iter != m_materials.end(); ++iter)
	{
		auto prog = ShaderCache::Instance()->getShader(iter->second->getProgramName());
		iter->second->setProgram(prog);
	}
}



MaterialKey MaterialCache::getCustomMaterialKey(CustomKeys key)
{
	return MaterialKey(CUSTOM_MATERIALS_KEY, key);
}
