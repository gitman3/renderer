#pragma once

//typedef unsigned long long uint64;
//typedef unsigned int uint;

namespace sip
{

	typedef unsigned int ID;
	//typedef unsigned long long BatchId;
	typedef std::pair<std::string, int> MaterialKey;
	typedef unsigned int ObjectID;

	/** The set flags determine also the drawing order in batcher.*/
	enum ShaderFlags : unsigned int
	{
		DEPTH_WRITE_ENABLED = 1u,
		DEPTH_TEST_ENABLED  = 1u << 1,
		CULLING_DISABLED = 1u << 2,
		BLEND_ENABLED  = 1u << 30 
	};

	typedef unsigned int uint;
	typedef unsigned long long uint64;
	const unsigned long long INDEX_MASK =    0xFFFF000000000000; // 65535 max (FFFF << 48)
	const unsigned long long ID_MASK =       0x0000FFFF00000000; // 65535 max (FFFF << 32)
	const unsigned long long REFCOUNT_MASK = 0x000000000000FFFF; // 65535 max (FFFF)
																 // const unsigned long long TYPE_MASK =      0x00000000001F0000; // 31 max types of components (1F << 16)
	const unsigned long long DELETED_MASK = 0x0000000080000000; // single bit (1 << 31)

}

