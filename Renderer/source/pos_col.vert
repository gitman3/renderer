#version 430

layout(location = 0) in vec4 a_vertexColor;
layout(location = 1) in vec4 a_vertexPosition;

out vec4 Color;

void main()
{
	Color = a_vertexColor;
	gl_Position = a_vertexPosition;
}