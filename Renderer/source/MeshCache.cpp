#include "MeshCache.h"
#include "Mesh.h"


using namespace sip;

Mesh* MeshCache::getMesh(const std::string& pathToModel)
{
	auto kp = m_cache.find(pathToModel);

	if (kp != m_cache.end())
	{
		return kp->second.get();
	}

	std::unique_ptr<Mesh> ptr(new Mesh(pathToModel.c_str())); // moved!

	m_cache.insert(std::pair<std::string, std::unique_ptr<Mesh>>(pathToModel, std::move(ptr)));
	return m_cache.find(pathToModel)->second.get();
}

