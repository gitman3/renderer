#include "LuaEngine.h"
#include "Debug.h"
#include <fstream>

using namespace sip;

LuaEngine::LuaEngine()  {}

LuaEngine::~LuaEngine() {}

LuaEngine& LuaEngine::instance() {
	static LuaEngine instance;
	return instance;
}

void LuaEngine::excecuteScriptFile(const char* fullpath) 
{
	std::ifstream f(fullpath);
	if (f.good()) 
	{
		f.close();
	}
	else
	{
		Debug::logFail(false, "[LuaEngine] file does not exist : ", fullpath);
	}
	int nRet = luaL_dofile(getState(), fullpath);
	if (nRet != 0)
	{
		Debug::log("[LUA ERROR] %s", lua_tostring(getState(), -1));
		lua_pop(getState(), 1);
	}
}

void LuaEngine::executeScriptString(const char* script) {
	int nRet = luaL_dostring(getState(), script);
	if (nRet != 0)
	{
		debug::logFail(false, lua_tostring(getState(), -1));
		//Debug::log("[LUA ERROR] %s", lua_tostring(getState(), -1));
		lua_pop(getState(), 1);
	}

}

template <>
void LuaEngine::CallGlobalFunction <void> (const char* funcName)
{
    lua_getglobal(LuaEngine::instance().getState(), funcName);
    if (lua_pcall(LuaEngine::instance().getState(), 0, 0, 0) != 0)
    {
		Debug::log("ERROR! %s", lua_tostring(LuaEngine::instance().getState(), -1));
    }
}


void LuaEngine::addSearchPath (const char* path){
	lua_getglobal(getState(), "package");
	lua_getfield(getState(), -1, "path");
	const char* cur_path = lua_tostring(getState(), -1);
	lua_pop(getState(), 1);
	lua_pushfstring(getState(), "%s;%s/?.lua", cur_path, path);
	lua_setfield(getState(), -2, "path");
	lua_pop(getState(), 1);
}

