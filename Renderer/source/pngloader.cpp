#include "Pngloader.h"
#include "png.h"
#include "Debug.h"

#include <fstream>
#include <gl\glew.h>
#include <vector>
#define PNGSIGSIZE 4
using namespace sip;

void PNGLoader::userReadData(png_structp pngPtr, png_bytep data, png_size_t length)
{
	//Here we get our IO pointer back from the read struct.
	//This is the parameter we passed to the png_set_read_fn() function.
	//Our std::istream pointer.
	png_voidp a = png_get_io_ptr(pngPtr);
	//Cast the pointer to std::istream* and read 'length' bytes into 'data'
	((std::istream*)a)->read((char*)data, length);
}

bool PNGLoader::validate(std::istream& source)
{

	//Allocate a buffer of 8 bytes, where we can put the file signature.
	png_byte pngsig[PNGSIGSIZE];
	int is_png = 0;

	//Read the 8 bytes from the stream into the sig buffer.
	source.read((char*)pngsig, PNGSIGSIZE);


	if (source.bad())
	{
		Debug::log("[PNGLoader] source bad");
	}

	if (source.fail())
	{
		Debug::log("[PNGLoader] source fail");
	}


	if (source.eof())
	{
		Debug::log("[PNGLoader] source EOF");
	}

	//Check if the read worked...
	if (!source.good()) {
		return false;
	}
	//Let LibPNG check the sig. If this function returns 0, everything is OK.
	is_png = png_sig_cmp(pngsig, 0, PNGSIGSIZE);
	return (is_png == 0);
}

std::vector<GLubyte> PNGLoader::load(const char* path, GLint& width, GLint& height)
{

	std::ifstream source(path, std::ifstream::binary);
	if (!source.good())
	{
		Debug::logFail(false, "[PNGLoader] file does not exist ", path);
		return std::vector<GLubyte>();
	}
	
	bool v = validate(source);
	
	if (!v)
	{
		Debug::logFail(false, "[PNGLoader] could not load file ", path);
		return std::vector<GLubyte>();
	}

	//Here we create the png read struct. The 3 NULL's at the end can be used
	//for your own custom error handling functions, but we'll just use the default.
	//if the function fails, NULL is returned. Always check the return values!
	png_structp pngPtr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!pngPtr) {
		Debug::logFail(false, "[PNGLoader] Couldn't initialize png read struct ", path);
		return std::vector<GLubyte>();
	}

	//Here we create the png info struct.
	//Note that this time, if this function fails, we have to clean up the read struct!
	png_infop infoPtr = png_create_info_struct(pngPtr);
	if (!infoPtr) {
		png_destroy_read_struct(&pngPtr, (png_infopp)0, (png_infopp)0);
		Debug::logFail(false, "[PNGLoader] ERROR: Couldn't initialize png info struct ", path);
		return std::vector<GLubyte>();
	}

	png_bytep* rowPtrs = NULL;
	char* data = NULL;

	if (setjmp(png_jmpbuf(pngPtr))) {
		//An error occured, so clean up what we have allocated so far...
		png_destroy_read_struct(&pngPtr, &infoPtr, (png_infopp)0);
		if (rowPtrs != NULL) delete[] rowPtrs;
		if (data != NULL) delete[] data;

		Debug::logFail("[PNGLoader] ERROR: An error occured while reading the PNG file ", path);
		//std::cout << "ERROR: An error occured while reading the PNG file";

			//Make sure you return here. libPNG will jump to here if something
			//goes wrong, and if you continue with your normal code, you might
			//End up with an infinite loop.
		Debug::logFail(false, "FAILED TO LOAD PNG");
		return std::vector<GLubyte>();
	}

	png_set_read_fn(pngPtr, (png_voidp)&source, userReadData);

	//Set the amount signature bytes we've already read:
	//We've defined PNGSIGSIZE as 8;
	png_set_sig_bytes(pngPtr, PNGSIGSIZE);

	//Now call png_read_info with our pngPtr as image handle, and infoPtr to receive the file info.
	png_read_info(pngPtr, infoPtr);


	png_uint_32 imgWidth = png_get_image_width(pngPtr, infoPtr);
	png_uint_32 imgHeight = png_get_image_height(pngPtr, infoPtr);

	width = imgWidth;
	height = imgHeight;

	//bits per CHANNEL! note: not per pixel!
	png_uint_32 bitdepth = png_get_bit_depth(pngPtr, infoPtr);
	//Number of channels
	png_uint_32 channels = png_get_channels(pngPtr, infoPtr);
	//Color type. (RGB, RGBA, Luminance, luminance alpha... palette... etc)
	png_uint_32 color_type = png_get_color_type(pngPtr, infoPtr);

	switch (color_type) {

	case PNG_COLOR_TYPE_RGBA:
//		EngineDebug::logFail("[PNGLoader] unsupported format");

		break;

	case PNG_COLOR_TYPE_PALETTE:
		png_set_palette_to_rgb(pngPtr);
		//Don't forget to update the channel info (thanks Tom!)
		//It's used later to know how big a buffer we need for the image
		channels = 3;
		break;
	case PNG_COLOR_TYPE_GRAY:
		if (bitdepth < 8)
			png_set_expand_gray_1_2_4_to_8(pngPtr);
		//And the bitdepth info
		bitdepth = 8;
		break;
	default:
		png_set_palette_to_rgb(pngPtr);
		channels = 3;
		Debug::log("[PNGLoader] default..");
		break;
	}

	/*if the image has a transperancy set.. convert it to a full Alpha channel..*/
	if (png_get_valid(pngPtr, infoPtr, PNG_INFO_tRNS)) {
		png_set_tRNS_to_alpha(pngPtr);
		channels += 1;
	}

	//Here's one of the pointers we've defined in the error handler section:
	//Array of row pointers. One for every row.
	rowPtrs = new png_bytep[imgHeight];

	//This pointer was also defined in the error handling section, so we can clean it up on error.
	data = new char[imgWidth * imgHeight * bitdepth * channels / 8];
	//This is the length in bytes, of one row.
	const unsigned int stride = imgWidth * bitdepth * channels / 8;

	//A little for-loop here to set all the row pointers to the starting
	//Adresses for every row in the buffer

	for (size_t i = 0; i < imgHeight; i++) {
		//Set the pointer to the data pointer + i times the row stride.
		//Notice that the row order is reversed with q.
		//This is how at least OpenGL expects it,
		//and how many other image loaders present the data.
		//png_uint_32 q = (imgHeight - i - 1) * stride;
		png_uint_32 q = i * stride;
		rowPtrs[i] = (png_bytep)data + q;

		//rowPtrs[i] = (png_bytep)data;


	}

	//And here it is! The actuall reading of the image!
	//Read the imagedata and write it to the adresses pointed to
	//by rowptrs (in other words: our image databuffer)
	png_read_image(pngPtr, rowPtrs);
	

	std::vector<GLubyte> glData;
	glData.reserve(imgHeight * stride);

	for (int i = imgHeight - 1; i >= 0; --i)
	{
		png_bytep row = rowPtrs[i];
		for (unsigned int s = 0; s < stride; ++s)
		{
			GLubyte r = (GLubyte)(*row);
			row++;
			glData.push_back(r);
		}

	}

	//Delete the row pointers array....
	delete[](png_bytep)rowPtrs;
	//And don't forget to clean up the read and info structs !
	png_destroy_read_struct(&pngPtr, &infoPtr, (png_infopp)0);

	return std::move(glData);

}

