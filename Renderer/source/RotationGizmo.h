#pragma once
#include "Handle.h"
#include <glm/vec3.hpp>
namespace sip
{
	class Entity;

	namespace editor
	{
		class RotationGizmo
		{
			enum Axis
			{
				X, 
				Y, 
				Z,
				NONE
			};
		public:
			RotationGizmo();
			void onSelected(Handle<Entity> entity);

		private:
			Handle<Entity> m_entity;
			Axis m_rotateAxis;
			Handle<Entity> m_selection;
			glm::vec3 m_prevHitPos;
		};
	}
}