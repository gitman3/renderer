#pragma once
namespace sip
{
	class Component
	{
	public:
		virtual ~Component() = 0;
	};
}
