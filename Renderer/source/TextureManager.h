#pragma once

#include <map>
#include "RenderTarget.h"
#include "Texture.h"

namespace sip
{

	class CubeMap;

	class TextureManager
	{
		std::map<std::string, Texture> m_textureCache;
		std::map<std::string, std::shared_ptr<CubeMap>> m_cubeMapCache;

		Texture* m_noiseTexture;
	public:
		TextureManager();
		void loadDefaultTextures();

		Texture* getNoiseTexture() { return m_noiseTexture; }

		RenderTarget getRenderTarget(int w, int h, unsigned int format = GL_RGBA);
		RenderTarget getDepthRenderTarget(int w, int h);
		Texture* addTextureIfMissing(const std::string& path);
		CubeMap* addCubeMapIfMissing(std::string name, std::shared_ptr<CubeMap> cubeMap);
		CubeMap* getCubeMap(const std::string& name);
		static TextureManager& Instance()
		{
			static TextureManager tm;
			return tm;
		}
	};
}