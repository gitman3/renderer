#pragma once

namespace sip
{
	class CubeMap
	{


		std::string m_pathPosX;
		std::string m_pathNegX;
		std::string m_pathPosY;
		std::string m_pathNegY;
		std::string m_pathPosZ;
		std::string m_pathNegZ;

		GLuint m_texture;
	public:

		GLuint getTexture() const { return m_texture; }

		CubeMap(const std::string& pathPosX,
			const std::string& pathNegX,
			const std::string& pathPosY,
			const std::string& pathNegY,
			const std::string& pathPosZ,
			const std::string& pathNegZ);

	

	};
}