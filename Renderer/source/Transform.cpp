#include "Transform.h"
#include "Entity.h"
#include "World.h"
#include "Storage.h"
#include "IDGenerator.h"

using namespace sip;



sip::Transform::Transform() :
	m_worldTransform(1.0f),
	m_rotation(),
	m_scale(1.0f),
	m_parent(),
	m_entity(),
	m_worldTransformDirty(true),
	m_ID(getNewID<Transform>()),
	m_localMatrixDirty(false),
	m_position(0.0f, 0.0f, 0.0f)
{
}

Transform::~Transform()
{
}

void sip::Transform::setRotation(const glm::fquat& rotation)
{
	m_rotation = rotation;
	m_localMatrixDirty = true;
	markWorldTransformDirty();
}

void Transform::rotateLocal(const glm::vec3& axis, float radians)
{
	m_rotation = glm::rotate(m_rotation, radians, axis);
	m_localMatrixDirty = true;
	markWorldTransformDirty();
}

glm::mat4 sip::Transform::getLocalTransform()
{
	 //TODO: if not dirty (m_localMatrixDirty), return cached version
	glm::mat4 trans(1.0f);
	trans = glm::translate(trans, m_position);
	return trans *= glm::mat4_cast(m_rotation) * glm::scale(glm::vec3(m_scale));
}



void Transform::translateLocal(const glm::vec3& by)
{
	m_position += by;
	m_localMatrixDirty = true;
	markWorldTransformDirty();
}



void Transform::addChild(Handle<Transform> child)
{
	assert(m_parent != child);
	auto childObj = Storage<Transform>::convert(child);

	if (!childObj->m_parent.isDeleted())
	{
		Storage<Transform>::convert(childObj->m_parent)->detachChild(child);
	}

	childObj->m_parent = this_handle;
	m_children.push_back(child);
}

void Transform::setLocalPosition(const glm::vec3& pos)
{
	m_position = pos;
	markWorldTransformDirty();
}

void Transform::markWorldTransformDirty()
{
	//if (m_worldTransformDirty) return;

	m_worldTransformDirty = true;
	for (auto child : m_children)
	{
		Storage<Transform>::convert(child)->markWorldTransformDirty();
	}
}

glm::mat4 Transform::getWorldTransform(PassKey<Batcher>)
{
	if (m_parent.isDeleted())
	{
		return getLocalTransform();
	}

	if (!m_worldTransformDirty)
	{
		return m_worldTransform;
	}


	Transform* parent = Storage<Transform>::convert(m_parent);
	while (parent && parent->m_worldTransformDirty)
	{
		if (parent->m_parent.isDeleted()) break;
		parent = Storage<Transform>::convert(parent->m_parent);
	}
	parent->rebuildWorldTransforms();
	return m_worldTransform;
}

void Transform::rebuildWorldTransforms()
{
	if (m_worldTransformDirty)
	{
		auto localT = getLocalTransform();
		m_worldTransform = m_parent.isDeleted() ? localT : Storage<Transform>::convert(m_parent)->m_worldTransform * localT;
		m_worldTransformDirty = false;
	}


	for (auto child : m_children)
	{
		if (child.isDeleted()) continue;
		Storage<Transform>::convert(child)->rebuildWorldTransforms();
	}
}

void Transform::setScale(float scale)
{
	m_scale = scale;
	markWorldTransformDirty();
}

void Transform::detachChild(Handle<Transform> t)
{
	// TODO: for non-editor build
	//m_children.erase(std::remove(m_children.begin(), m_children.end(), t), m_children.end());
}

