#include "Mouse.h"
#include "Context.h"
#include "GLFW/glfw3.h"
using namespace sip;


namespace 
{

	int getMask(Mouse::Button button, Mouse::MouseButtonState state)
	{
		switch (button)
		{
		
		case Mouse::Left:
			return  state;
			break;
		
		case Mouse::Middle:
			return  state << 8;
			break;

		case Mouse::Right:
			return state << 16;
			break;
		}
		return 0;
	}


}

void Mouse::updateState(Button button, MouseButtonState state)
{
	auto& mouse = Instance();
	auto prevState = mouse.m_mouseButtonStates & button; // grab old state
	mouse.m_mouseButtonStates &= (~button); // clear current state

	auto pressStateBits = getMask(button, PRESS);
	auto releaseStateBits = getMask(button, RELEASE);
	auto holdStateBits = getMask(button, HOLD);

	if (state == PRESS)
	{
		//debug::log("Press");
		mouse.m_mouseButtonStates |= pressStateBits;
	}
	else if (state == RELEASE)
	{
		//debug::log("Release");
		mouse.m_mouseButtonStates |= releaseStateBits;
	}
	else if (state == HOLD)
	{
		if (prevState == pressStateBits)
		{
			//debug::log("Hold");
			mouse.m_mouseButtonStates |= holdStateBits;
		}
	}
}

void Mouse::MouseButtonFunc(int button, int action, int mods)
{
	auto& mouse = Instance();
	const auto state = action == GLFW_PRESS ? PRESS : RELEASE;
	
	if (button == GLFW_MOUSE_BUTTON_LEFT)
	{
		mouse.updateState(Button::Left, state);
	}
	else if (button == GLFW_MOUSE_BUTTON_RIGHT)
	{
		mouse.updateState(Button::Right, state);
	}
	else if (button == GLFW_MOUSE_BUTTON_MIDDLE)
	{
		mouse.updateState(Button::Middle, state);
	}

}

void Mouse::MouseMovedFunc(int x, int y)
{
	Instance().m_x = x;
	Instance().m_y = y;
}

void Mouse::MouseScrollFunc(double x, double y)
{
	Instance().m_scrollX = x;
	Instance().m_scrollY = y;
}

void Mouse::bind(Context* context)
{
	context->setMouseButtonFunc(Mouse::MouseButtonFunc);
	context->setMouseFunc(Mouse::MouseMovedFunc);
	context->setMouseScrollFunc(Mouse::MouseScrollFunc);
}

bool Mouse::isButtonDown(Button button) 
{
	return getMask(button, MouseButtonState::PRESS) & Instance().m_mouseButtonStates;
}

bool Mouse::isButtonHold(Button button)
{
	return getMask(button, MouseButtonState::HOLD) & Instance().m_mouseButtonStates;
}

bool Mouse::isButtonReleased(Button button)
{
	return getMask(button, MouseButtonState::RELEASE) & Instance().m_mouseButtonStates;
}

void Mouse::Update()
{
	auto& mouse = Instance();

	static const std::vector<Button> buttons { Button::Left, Button::Middle, Button::Right };

	for (auto button : buttons)
	{
		if (mouse.m_mouseButtonStates & getMask(button, MouseButtonState::PRESS) )
		{
			mouse.updateState(button, MouseButtonState::HOLD);
		}
		else if (mouse.m_mouseButtonStates & getMask(button, MouseButtonState::RELEASE))
		{
			mouse.updateState(button, MouseButtonState::NONE);
		}
	}
	Instance().m_prevX = Instance().m_x;
	Instance().m_prevY = Instance().m_y;
	Instance().m_scrollX = 0.0;
	Instance().m_scrollY = 0.0;
}


