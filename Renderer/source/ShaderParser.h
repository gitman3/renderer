#pragma once
#include "Shader.h"


namespace sip
{
	struct ShaderParserOutput
	{
		std::string vertexShaderSrc;
		std::string fragmentShaderSrc;
		Shader shader;
	};


	class ShaderParser
	{
		static ShaderParserOutput fromCustomSrc(const std::string& filename);
		static void replaceIncludes(std::string& src);
	public:
		static ShaderParserOutput parseShader(const std::string& path);
	};

}