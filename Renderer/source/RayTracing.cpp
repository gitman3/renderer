#include "RayTracing.h"
#include "Camera.h"
#include "Director.h"
#include "World.h"

using namespace sip;

RayTracing::Ray RayTracing::getRay(int screenX, int screenY)
{
	auto world = Director::Instance().getWorld();
	return world->getActiveCamera()->screenToWorldRay(glm::vec2(screenX, screenY));
}

RayTracing::RayCollisionInfo RayTracing::traceSphere(const Ray& ray, const glm::vec3& spherePos, float radius)
{
	RayCollisionInfo info;

	float d = glm::dot(ray.direction, spherePos - ray.origin);
	if (d < 0.0) return info;

	glm::vec3 x = (ray.origin + ray.direction * d) - spherePos;
	float disc = glm::length(x);
	if (disc >= radius) return info;

	float b = sqrt(radius*radius - disc*disc);
	info.t = glm::length(ray.direction * d) - b;
	info.t2 = glm::length(ray.direction * d) + b;
	info.pos = ray.origin + ray.direction * info.t;
	info.pos2 = ray.origin + ray.direction * info.t2;

	info.ray = ray;
	return info;
}

RayTracing::RayCollisionInfo RayTracing::tracePlane(const Ray& ray, const glm::vec3& planePos, const glm::vec3 planeNormal)
{
	RayCollisionInfo info;
	float rd_pn = glm::dot(ray.direction, planeNormal);
	if (rd_pn > 0.0f) return info;

	float t =  (glm::dot(planeNormal, ray.origin) - glm::dot(planePos, planeNormal));
	t /= -rd_pn;

	info.pos = ray.origin + ray.direction * t;
	info.ray = ray;
	info.t = t;
	return info;
}
