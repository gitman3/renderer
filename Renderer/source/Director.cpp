#include "Debug.h"
#include "Director.h"
#include "LuaEngine.h"
#include "LuaTable.h"
#include "LuaUtils.h"
#include "Material.h"
#include "World.h"
#include "Context.h"
#include "nanogui/nanogui.h"
#include "Keyboard.h"
#include "EditorGUI.h"
#include "Editor.h"
using namespace sip;

bool Director::sm_initialized = false;
Director::Director() : 
	m_cullCount(0), 
	m_drawCount(0), 
	m_world(nullptr), 
	m_context(nullptr)
{
	
}

Director::~Director()
{
	delete m_context;
	m_context = nullptr;
}

void Director::onRenderStart()
{
	m_cullCount = 0;
	m_drawCount = 0;
}



void Director::onRenderEnd()
{
	//EngineDebug::log("[EngineDirector] drawn : ", m_drawCount, " culled : ", m_cullCount);
}

void Director::refreshWorldConfig(World* w)
{
	Debug::log("[EngineDirector] refreshWorldConfig");
	LuaEngine::instance().excecuteScriptFile(w->m_pathToConfig.c_str());

	const LuaTable& worldConf = LuaTable(LuaEngine::instance().getState(), "g_WorldConfig");

	const LuaTable& lightDir = worldConf.getTable("globalLightDirection");
	w->m_globalLightDirection.x = lightDir.getFloat("x");
	w->m_globalLightDirection.y = lightDir.getFloat("y");
	w->m_globalLightDirection.z = lightDir.getFloat("z");

	const LuaTable& bgcol = worldConf.getTable("bgColor");
	float r = bgcol.getFloat("r");
	float g = bgcol.getFloat("g");
	float b = bgcol.getFloat("b");

	Debug::log("[EngineDirector] bgColor refreshed");
	w->m_bgColor = glm::vec3(r, g, b);

	const LuaTable& ambientColor = worldConf.getTable("ambientColor");
	r = ambientColor.getFloat("r");
	g = ambientColor.getFloat("g");
	b = ambientColor.getFloat("b");
	w->m_ambientLightColor = glm::vec3(r, g, b);

}

void Director::reloadMaterialConfig()
{
	if (!sm_initialized)
	{
		return;
	}
	loadMaterialConfigFile(m_pathToMatConf.c_str());
}

void Director::loadMaterialConfigFile(const char* pathTo)
{
	sm_initialized = true;
	Debug::log("[EngineDirector] loadMaterialConfigFile");
	m_pathToMatConf = pathTo;
	LuaEngine::instance().excecuteScriptFile(pathTo);

}


void Director::refreshMaterial(Material* mat)
{
	if (mat->getMaterialConfigName().empty()) return;

	setMaterialConfig(mat, mat->getMaterialConfigName());
}

void Director::setMaterialConfig(Material* material, const std::string& materialName)
{

	assert(material != nullptr);

	Debug::logFail(sm_initialized, "Director material file is not set! set with EngineDirector::setMaterialConfig()");

	material->setMaterialConfigName({}, materialName);

	const LuaTable& matConf = LuaTable(LuaEngine::instance().getState(), "g_materials");
	const LuaTable& mat = matConf.getTable(materialName.c_str());
	float spec = mat.getFloat("specular");
	float ra = mat.getFloat("rim_amount");
	float rp = mat.getFloat("rim_power");
	float bp = mat.getFloat("bump_power");
	float ba = mat.getFloat("bump_amount");

	material->Specular = spec;
	material->RimAmount = ra;
	material->RimPower = rp;
	material->BumpPower = bp;
	material->BumpAmount = ba;
}



void Director::createContext(const char* pathToConfig, int w, int h)
{
	
	m_context = new Context(w , h);

	m_world = new World("../Renderer/config/config.lua");
	editor::EditorGUI::Instance().InitGUI();
	editor::Editor::Instance().init();
}

void Director::update()
{
	editor::Editor::Instance().update();
	editor::EditorGUI::Instance().update();
}

Context* Director::getContext()
{
	return m_context;
}

World* Director::getWorld()
{
	return m_world;
}

